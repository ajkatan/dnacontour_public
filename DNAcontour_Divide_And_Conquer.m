function [DNAregions,SK,SubIms]=DNAcontour_Divide_And_Conquer(Analog_im,DNA_bin,PadSize,skelmeta,doplots)
%divides the image into regions, one region per DNA molecule
%get the skeleton for each region
if nargin<3|| isempty(PadSize)
    PadSize=2;
end
if nargin<4||isempty(skelmeta)
    skelmeta.method='thin';%Select method for skeletonization. 'thin' works better than 'skel' up to now
    skelmeta.clean=0; %nr of pixels to prune off the skeleton
end

if nargin<5
    doplots=0;
end

%analyze DNA regions
DNA_binstruct=bwconncomp(DNA_bin,8); %makes a structure of connected regions. Points in these regions are found in field 'PixelIdxList'%minimal area in pixels
DNAregions=regionprops(DNA_binstruct,'Centroid','Area','BoundingBox','PixelList','Image','Perimeter');
NDNA=length(DNAregions);
%preallocation
sksingle=struct('PixelList',[],'Xoff',[],'Yoff',[],'PixelIdxList',[],... %fields added here
    'ImSize',[],'EndPoints',[],'BranchPoints',[],'BranchIdx',[],'BranchDist',[],... %fields added in MapBranch
    'LoopN',[],'LoopPixels',[],'LoopBranches',[],'LoopEPBP',[],... %fields added in FindLoop
    'Length',[],'LengthNoLoop',[],'Path',[],'Angle',[],'Curv',[],'Dist',[],...
    'AdjMat',[],'CostMat',[],'ParAdjMat',[],'ParCostMat',[],'costs',[],'paths',[] ...%Fields added in getpath
    );

SK=repmat(sksingle,NDNA,1);
SubIms=repmat(struct('pic',[],'bin_im',[],'sk_im',[]),NDNA,1);
for ii=1:length(DNAregions)
    BB=DNAregions(ii).BoundingBox;
    xoff=floor(BB(2));xext=BB(4);
    yoff=floor(BB(1));yext=BB(3);
    subpic=Analog_im(xoff+(1:xext),yoff+(1:yext));
    subpic=padarray(subpic,[PadSize,PadSize]); %padding is done to avoid problems later with pixels not being found in the subimages
    SubIms(ii).pic=subpic;
    DNAregions(ii).Xoff=xoff-PadSize; %add or subtract this number to get translation small-large image
    DNAregions(ii).Yoff=yoff-PadSize;
    %we do the skeletonization in this loop too to make sure the subarrays are
    %the same
    SK(ii).ImSize=size(subpic);
    SubIms(ii).bin_im=padarray(DNAregions(ii).Image,[PadSize,PadSize]);%subarray of binary image
    if isfield(skelmeta,'fill') && skelmeta.fill==1;
        SubIms(ii).bin_im=bwmorph(SubIms(ii).bin_im,'fill');
    end
    SubIms(ii).sk_im=bwmorph(SubIms(ii).bin_im,skelmeta.method,'Inf');
    if isfield(skelmeta,'clean')&&skelmeta.clean~=0
        SubIms(ii).sk_rawim=SubIms(ii).sk_im;
        SubIms(ii).sk_im=bwmorph(SubIms(ii).sk_im,'spur',skelmeta.clean);
    end
    SK(ii).Xoff=DNAregions(ii).Xoff;SK(ii).Yoff=DNAregions(ii).Yoff;
    SK(ii).PixelIdxList=find(SubIms(ii).sk_im);
    [x,y]=ind2subFast2D(SK(ii).ImSize(1),SK(ii).PixelIdxList);
    SK(ii).PixelList=[x,y];
end


%plot the result including labels of the DNA

if doplots
    DNAcentroids=reshape([DNAregions.Centroid],2,DNA_binstruct.NumObjects)'; %from N struct array of 2x1 to Nx2
    DNAcentroids=[DNAcentroids(:,2) DNAcentroids(:,1)]; % the order is reversed in the builtin function
    DNAcontour_plotim(DNA_bin);title('DNA masked');hold all;
    for ii=1:size(DNAcentroids,1)
        text(DNAcentroids(ii,1),DNAcentroids(ii,2),num2str(ii),'Color','w');
    end
    hold off
end

fprintf('found %i DNA molecules\n',NDNA);

