function [ClickPoints,validmols] = DNAcontour_Click_Points(SubIms,istart,npoints,clickfig,Clim)
%DNAcontour_Click_Points
%Allows you to select special points on the molecules by manual clicking
%These are stored in a structure 'ClickPoints', which contains the points'
%coordinates in the subimage, as well as their distances to each other
if nargin<2||isempty(istart)
    istart=1;
end
if nargin<3||isempty(npoints)
    npoints=4;
end
if nargin<4||isempty(clickfig)
    clickfig=figure;
end
if nargin<5||isempty(Clim)
    Clim=[-0.1e-9 4e-9];
end
load('colormaps.mat')
ClickPoints=repmat(struct('Points',zeros(1,npoints),'Distances',[]),length(SubIms),1);
validmols=[];
but=1;
for ii=istart:length(SubIms)
    if but==2*pi
        fprintf(1,'loop exited before image nr %i \n',ii)
        break
    else
        figure(clickfig);clf; DNAcontour_plotim(SubIms(ii).pic);colormap(gold1);
        hold('all');set(gca,'Clim',Clim)
        but=1;ClickedX=[];ClickedY=[];
    end
    nclicks=0;
    while but<=3 && numel(ClickedX)<=npoints
        try
            [cx,cy,but]=ginput(1);
        catch %if you close the figure this gives an error, but we just want it to return to sender
            but=2*pi;
            break
        end
        nclicks=nclicks+1;
        if but==1
            ClickedX=[ClickedX cx];
            ClickedY=[ClickedY cy];
            Dots=plot(ClickedX,ClickedY,'ro');
        elseif but==2 %middle click resets
            nclicks=0;
            figure(clickfig);hold('off');clf; DNAcontour_plotim(SubIms(ii).pic);colormap(gold1);
            hold('all');set(gca,'Clim',Clim)
            ClickedX=[];ClickedY=[];
        elseif but==3 %middle click resets
            %nothing yet assigned to right click
        else
            but=pi; % using the enter button gives an empty matrix, which gives an error, so we use pi as a placeholder
        end
    end
    hold ('off')
    if numel(ClickedX)==npoints
        validmols=[validmols ii];if mod(numel(validmols),20)==0,fprintf(1,'%i\n',ii);else fprintf(1,'%i ',ii);end
        CP_wrapped=[ClickedX' ClickedY';[ClickedX(1) ClickedY(1)]];
        ClickPoints(ii).Points=CP_wrapped(1:nclicks,:);
        ClickPoints(ii).Distances=sqrt(sum((diff(CP_wrapped,1)).^2,2));
    else
        ClickPoints(ii).Points=[];
        ClickPoints(ii).Distances=[];
    end

end
if ishghandle(clickfig), close(clickfig);end
if ii==numel(SubIms),fprintf(1,'Finished. Last image: nr%i\n',ii); end
end