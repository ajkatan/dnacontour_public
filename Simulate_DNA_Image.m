function [simim,simim0,DNAxp,DNAyp,protpos,DNAx,DNAy]=Simulate_DNA_Image(im_nm,imsize,nDNA,lp,DNAbp,tipR,kinkbp,kinkang,doplots);

if nargin<1 ||isempty(im_nm)
im_nm=2000;% image size nm
end
if nargin<2 ||isempty(imsize)
    imsize=1000; %image size pixels
end
pixsize=im_nm/imsize; %nm/pixel
%posvec=linspace(0,im_nm,imsize);
if nargin<3 ||isempty(nDNA)
nDNA=10;% %nr of DNA molecules
end
if nargin<4
    lp=50;
end
bpl=0.34; %basepair length in mn
if nargin<5
   DNAbp=1000; 
end
if nargin<6
tipR=0;% Tip radius nm
end
if nargin<7
kinkbp=0; %you can add kinks in the DNA on purpose. stochastic, mean every kinkbp basepairs. 0 = no kinks
end
if nargin<8
kinkang=0; %sharpness of kinks. random between -kinkang and kinkang (in rad)
end
if nargin<9
    doplots=1;
end
DNAh=0.8; %apparent height nm
DNAw=2;  %DNA width nm
nPROT=0; %nr of proteins
onDNA=0.3; %fraction of proteins on DNA
PROTh=3; %apparent height nm
hnoise=0.05; %vertical measurement noise
horstripes=4; % nr of horizontal stripes
stripeh=2;

if nDNA==1
    DNAstartposX=im_nm/2*ones(DNAbp,1);DNAstartposY=im_nm/2*ones(DNAbp,1); %start in center
else
    DNAstartposX=rand(1,nDNA)*im_nm; %set of random start x positions in real space
    DNAstartposX=repmat(DNAstartposX,DNAbp,1);
    DNAstartposY=rand(1,nDNA)*im_nm; %set of random start y positions in real space
    DNAstartposY=repmat(DNAstartposY,DNAbp,1);
end
[DNAx,DNAy]=DNAcontour_WLC_simulation(lp,DNAbp,nDNA,kinkbp,kinkang,bpl); %simulate the chains
DNAx=DNAx+DNAstartposX;DNAy=DNAy+DNAstartposY; %Place them on the image
DNAxp=round(DNAx/pixsize);InBoundsX=(DNAxp>=1)&(DNAxp<=imsize); %to pixels
DNAyp=round(DNAy/pixsize);InBoundsY=(DNAyp>=1)&(DNAyp<=imsize);
DNAxp=DNAxp(InBoundsX&InBoundsY);
DNAyp=DNAyp(InBoundsY&InBoundsX);
simim0=zeros(imsize,imsize);
DNAlin=sub2ind([imsize,imsize],DNAxp,DNAyp);
simim0(DNAlin)=DNAh;
bwd=bwdist(simim0);also_on=(bwd*pixsize)<DNAw/2;
simim0(also_on)=DNAh;

if nPROT>0
np_on=round(onDNA*nPROT);np_off=nPROT-np_on;
npix_DNA=length(DNAlin);
indprot_on=ceil(npix_DNA*rand(np_on,1));
indprot_on=DNAlin(indprot_on);
emptyspace=find(simim0==0);
npix_none=numel(emptyspace);
indprot_off=ceil(npix_none*rand(np_off,1));
indprot_off=emptyspace(indprot_off);
protpos=[indprot_on;indprot_off];
%simim(protpos)=simim(protpos)+PROTh;
[simim0,protim]=makeprotein(simim0,protpos,PROTh,pixsize);
else 
    protpos=0;
end
if tipR>0
    simim1=tipconvolution(simim0,tipR,pixsize);
else
    simim1=simim0;
end
%add noise
noiseRMS=0.2;
simim2=simim1+hnoise*randn(size(simim1));

% make horizontal stripes on top
simim3=simim2;
stripelines=ceil(imsize*rand(horstripes,1));
stripelength=ceil(imsize*rand(horstripes,1));
stripestart=ceil(imsize*rand(horstripes,1));
stripeend=min(imsize,stripelength+stripestart);
for ii=1:length(stripelines)
    ss=stripestart(ii);ee=stripeend(ii);
    stripeheight=max(simim3(ss:ee,stripelines(ii)),stripeh);
    simim3(ss:ee,stripelines(ii))=stripeheight;
end
   
simim=simim3;

if doplots
subplot(2,2,1);plot(DNAx,DNAy);title('DNA molecules');
subplot(2,2,2);DNAcontour_plotim(simim0); title('Base image');
subplot(2,2,3);DNAcontour_plotim(simim1); title('Tip convoluted');
subplot(2,2,4);DNAcontour_plotim(simim3); title('With noise and stripes');
end

end

function [outp_im,protim]=makeprotein(inp_im,p_pos,p_hi,ps)
%makes  a hemispherical protein image and adds it to the input image
[x,y]=ind2sub(size(inp_im),p_pos);
p_p=ceil(p_hi/ps); %protein radius in pixels
R=2*ps*p_p*radialgrid(2*p_p+1);
xs=x-p_p;
xe=x+p_p;
ys=y-p_p;
ye=y+p_p;
protim=p_hi.^2-R.^2;
protim=sqrt(protim.*(protim>0));
imsiz=size(inp_im);
outp_im=zeros(imsiz+2*[p_p,p_p]); %pad the output
outp_im((p_p+1):(p_p+imsiz(1)),(p_p+1):(p_p+imsiz(2)))=inp_im;
for ii=1:length(p_pos)
%     if xs(ii)<1||ys(ii)<1
%         disp('?')
%     end
    rx=p_p+(xs(ii):xe(ii));
    ry=p_p+(ys(ii):ye(ii));
    outp_im(rx,ry)=outp_im(rx,ry)+protim;
end
outp_im=outp_im((p_p+1):(p_p+imsiz(1)),(p_p+1):(p_p+imsiz(2))); %unpad
end



