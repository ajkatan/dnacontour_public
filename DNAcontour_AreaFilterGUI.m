function [a_t,BW]=DNAcontour_AreaFilterGUI(BWin,dogui,a_t);
%UNTITLED Filters a thresholded image for area with visual feedback
%   Detailed explanation goes here
if nargin<1
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    tim2=getfield(load(fullfile(thisfolder,'Test data','testims.mat')),'tim2');
    pic=tim2.data;
    BWin=pic>6e-10;
end
if nargin<2
    dogui=1; %default use gui
end

BWstruct= bwconncomp(BWin,8);
area = cellfun(@numel, BWstruct.PixelIdxList);
if dogui || nargin<3
    a_t=round([median(area)/2,max(area)]); %first try
end

stop_it=0;
AFfig=figure;
subplot(3,1,1);nbins=max(1,round(length(area)/3));hist(area,nbins);
Woptions.WindowStyle='normal';
CurrentZoomX=[0 size(BWin,1)];CurrentZoomY=[0 size(BWin,2)];
while ~stop_it
    idx1=(area >=a_t(1)) &(area <=a_t(2));
    idxToKeep = BWstruct.PixelIdxList(idx1);
    idxToKeep = vertcat(idxToKeep{:});
    BW = false(size(BWin));
    BW(idxToKeep) = true;
    if dogui
        figure(AFfig);subplot(3,1,2:3);
        DNAcontour_plotim(BWin+BW);ImAx=gca;
        colormap(hot);set(ImAx,'Clim',[0 2]);title('included=white, excluded=orange');
        set(ImAx,'xlim',CurrentZoomX);set(ImAx,'ylim',CurrentZoomY);
        answer = inputdlg_tt({'Minimal area (pixels)','Maximual area (pixels)'},'Set Area Threshold',1,{num2str(a_t(1)),num2str(a_t(2))},Woptions);
        if isempty(answer) % The 'done' button, aka 'cancel', gives an empty answer
            stop_it=1;
        else
            a_t=[sscanf(answer{1},'%i'),sscanf(answer{2},'%i')];
        end
        CurrentZoomX=get(ImAx,'xlim');CurrentZoomY=get(ImAx,'ylim');
    else
        stop_it=1; %skip gui means you only run once
    end
end
if exist('AFfig','var') && ishghandle(AFfig)
        close(AFfig);
end

