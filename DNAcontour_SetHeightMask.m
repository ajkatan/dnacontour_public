function masked=DNAcontour_SetHeightMask(pic,hbottom,htop,htoo,gracepixels)
%thresholding function sets a height mask on an AFM image using two
%thresholds masked pixels will be higher than the lower threshold hbottom,
%as in simple thresholding To allow inclusion of boundaries without taking
%in too much noise, the regions are filtered. For this, all regions with a
%maximum value below htop are excluded. This means values between hbottom
%and htop are only included if the region also has areas above htop.

%The htoo threshold prevents perimeters of too large objects (junk) being counted.
%Any regions that have a maximum value above htoo are excluded.
%A mechanism is built in to allow areas with only few pixels (gracepixels)
%higher than the high threshold to not be excluded.

if nargin==0
    Test_Mode=1;
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    tim1=getfield(load(fullfile(thisfolder,'Test data','testims.mat')),'tim1');
    pic=tim1.data;
    hbottom=2e-10;
    htop=0.7e-9;
    htoo=4e-9;
    gracepixels=0;
else
    if nargin<3 || isempty(htop)
    htop=hbottom; %if only bottom is given we do not use top
    end
    if nargin<4 || isempty(htoo)
        htoo=Inf; %if no max is given we do not use it
    end
    if nargin<5
        gracepixels=0; %no mercy unless you ask for it
    end
    Test_Mode=0;
end


notlow=pic>hbottom;
notlowstruct=bwconncomp(notlow);
maxes=regionprops(notlowstruct,pic,'MaxIntensity'); %N_notlowregions x 1 struct array of max values
toohighinds = find([maxes.MaxIntensity] > htoo); %label indices of regions that would be discarded on the basis of too high pixels
toolowinds =  find([maxes.MaxIntensity] < htop);
graceflag=false(size(toohighinds)); %preallocate in case toohighinds is empty

if gracepixels>0 %find regions with only a few pixels too high
    toohigh=pic>htoo;
    toohighstruct=bwconncomp(toohigh);
    areas=regionprops(toohighstruct,'Area'); %N_highregions x 1 struct array of areas
    graceinds=find([areas.Area]<gracepixels); %label indices of regions that are too small to discard
    grace=ismember(labelmatrix(toohighstruct),graceinds); %boolean matrix size of image, 1 for small regions in 'too high' boolean
    gracelin=find(grace); %linear indices of points to include
    for ii=1:length(toohighinds)
        inds=notlowstruct.PixelIdxList{toohighinds(ii)}; %all the linear indices of pixels in the regions to be tested
        graceflag(ii)=any(ismember(gracelin,inds)); %sets
    end
end

belongstoohigh = ismember(labelmatrix(notlowstruct), toohighinds(~graceflag));
belongstoolow =  ismember(labelmatrix(notlowstruct), toolowinds);
masked=notlow & ~belongstoohigh & ~belongstoolow;

if Test_Mode
    figure(333)
    subplot(2,3,1);DNAcontour_plotim(pic);title('original');caxis([hbottom-htop htoo]);
    subplot(2,3,2);DNAcontour_plotim(notlow);title('initial mask');
    subplot(2,3,3);DNAcontour_plotim(belongstoohigh+0);title('too high');
    subplot(2,3,4);DNAcontour_plotim(belongstoolow+0);title('not high enough');
    subplot(2,3,5);DNAcontour_plotim(masked+0);shading flat;title('final mask');
    disp('end of test')
end