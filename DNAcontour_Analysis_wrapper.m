function [DNAregions,SK,SubIms,SPS,sk_im,loop_im,path_im,meta]=DNAcontour_Analysis_wrapper(pic,DNA_bin,meta,doplots)
%wrapper around the analysis programs. First cuts up the image into
%regions, which saves a ton of memory
if nargin<5
    doplots=0;
end
if ~ isfield(meta,'BranchMethod')
   % meta.BranchMethod='AK_homemade';
    meta.BranchMethod='AK_boundary';
end

[DNAregions,SK,SubIms]=DNAcontour_Divide_And_Conquer(pic,DNA_bin,meta.PadSize,meta.skel,doplots);
sk_im=false(size(DNA_bin));
loop_im=false(size(DNA_bin));
path_im=false(size(DNA_bin));
AllSkels=[];AllPaths=[];AllLoops=[];

%To avoid errors, the functions used later on should not try to assign
%values to fields that are not yet in the 
%the structures .
%Therefore we need to initialize some of them.

SPS=struct(...
    'Path',[],'Angle',[],'Curv',[],'D',[],'SnakeX',[]...
    ,'SnakeY',[],'SnakeAng',[],'SnakeCurv',[],'SnakeD',[],'SnakeLength',[]...
    ); 
SPS=repmat(SPS,size(DNAregions));%that should cover the subpixel part
%we use one run to find the field names generated in the boundary method
SK0=DNAcontour_trace_boundary(SubIms(1),SK(1),0);
SK0=DNacontour_MapBoundaryBranches(SK0,[],0,0);%just make all the fields for both methods
FL=fieldnames(SK0);
for kk=1:length(FL) %this should automatically generate the fields created above for SK0;
    if~isfield(SK,FL{kk})
        SK(1).(FL{kk})=[];
    end
end


for ss=1:length(DNAregions)
    Debug=false;%ss==4 ;% set to ss==n if you want visual feedback on that nr
    skippath=0;
    echonr=1; % nr of ones t skip when reporting progress
    fprintf(1, 'molecule nr %i: ', ss)
    switch meta.BranchMethod
        case 'AK_homemade'
            [SK(ss),SubIms(ss).sk_im]=DNAcontour_MapBranchDistance(SK(ss),SubIms(ss).sk_im,ss,Debug);
            [SK(ss),SubIms(ss).loop_im]=DNAcontour_FindLoops(SK(ss),SubIms(ss).sk_im);
            if ~skippath
                [SK(ss),SubIms(ss).path_im]=DNAcontour_GetPaths(SK(ss),ss);
                if round(ss/echonr)==ss/echonr, fprintf('%i of %i: ',[ss,length(DNAregions)]);end;
                SPS(ss)=SubPixDNA(SK(ss),SubIms(ss).pic.*SubIms(ss).bin_im,meta,round(ss/echonr)==ss/echonr);
            end
        case('AK_boundary')
            SK(ss)=DNAcontour_trace_boundary(SubIms(ss),SK(ss),0); %for now we ignore the minimum length thing
            SK(ss)=DNacontour_MapBoundaryBranches(SK(ss),[],0,0);
            %--- The following elements are assigned in GetPaths but not in
            %MapBoundaryBranches
            SK(ss).Path=SK(ss).bdy_lin;
            [x,y]=ind2subFast2D(SK(ss).ImSize(1),SK(ss).Path);
            [SK(ss).Angle,SK(ss).Curv,SK(ss).Dist]=AngAndCurv(x,y);
            SK(ss).Length=SK(ss).Dist(end);
            %---
            SPS(ss)=SubPixDNA(SK(ss),SubIms(ss).pic.*SubIms(ss).bin_im,meta,round(ss/echonr)==ss/echonr);
    end
    SK(ss).Length=numel(SK(ss).Path);
end

%To make full images
for ss=1:length(DNAregions)
    if~isempty(SK(ss).LoopPixels)
    theseloops=vertcat(SK(ss).LoopPixels{:});
    if ~isempty(theseloops)
        temploop=DNAcontour_sub2fullmap(theseloops,SK(ss).Xoff,SK(ss).Yoff,SK(ss).ImSize(1),size(DNA_bin,1));
        AllLoops=vertcat(AllLoops,temploop);
    end
    end
    tempskel=DNAcontour_sub2fullmap(SK(ss).PixelIdxList,SK(ss).Xoff,SK(ss).Yoff,SK(ss).ImSize(1),size(DNA_bin,1));
    AllSkels=vertcat(AllSkels,tempskel);
    [temppath]=DNAcontour_sub2fullmap(SK(ss).Path,SK(ss).Xoff,SK(ss).Yoff,SK(ss).ImSize(1),size(DNA_bin,1));
    AllPaths=vertcat(AllPaths,temppath);
end


%make full images
sk_im(AllSkels)=true;
path_im(AllPaths)=true;
loop_im(AllLoops)=true;