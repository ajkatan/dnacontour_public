function [DNAregions,SK,SubIms,SPS,sk_im,loop_im,path_im]=DNAcontour_Analysis_wrapper_Bound(pic,DNA_bin,meta,doplots)
%wrapper around the analysis programs. First cuts up the image into
%regions, which saves a ton of memory
if nargin<5
    doplots=0;
end
[DNAregions,SK,SubIms]=DNAcontour_Divide_And_Conquer_bound(pic,DNA_bin,meta.PadSize,meta.skel,doplots);
sk_im=false(size(DNA_bin));
loop_im=false(size(DNA_bin));
path_im=false(size(DNA_bin));
AllSkels=[];AllPaths=[];AllLoops=[];
SPS=struct('Path',[],'Angle',[],'Curv',[],'D',[],'SnakeX',[],'SnakeY',[],'SnakeAng',[],'SnakeCurv',[],'SnakeD',[]);
SPS=repmat(SPS,size(DNAregions));
some more initialization before parallel operations
SK(1) = DNAcontour_trace_boundary(SubIms(1),SK(1));
SK(1)=DNacontour_MapBoundaryBranches(SK(1),[],0,0);
for 
% for jj=1:length(DNAregions)
% SK(jj).BranchIdx=[];SK(jj).BranchDist=[];SK(jj).EndPoints=[];SK(jj).BranchPoints=[];SK(jj).LoopN=[];SK(jj).LoopPixels=[];SK(jj).LoopBranches=[];SK(jj).LoopEPBP=[];SK(jj).LengthNoLoop=[];
% SK(jj).AdjMat=[];SK(jj).ParAdjMat=[];SK(jj).ParCostMat=[];SK(jj).CostMat=[];SK(jj).costs=[];SK(jj).paths=[];SK(jj).Path=[];SK(jj).Angle=[];SK(jj).Curv=[];SK(jj).Dist=[];SK(jj).Length=[];
% SubIms(jj).loop_im=[];SubIms(jj).path_im=[];
% end


for ss=1:length(DNAregions)
    Debug=0; %ss==1 ;% set to ss==n if you want visual feedback on that nr
    SK(ss)=DNAcontour_MapBranchDistance(SK(ss),SubIms(ss).sk_im,ss,Debug);
    [SK(ss),SubIms(ss).loop_im]=DNAcontour_FindLoops(SK(ss),SubIms(ss).sk_im);
    skippath=0;
    if ~skippath
        [SK(ss),SubIms(ss).path_im]=DNAcontour_GetPaths(SK(ss),ss);
        echonr=1;if round(ss/echonr)==ss/echonr, fprintf('%i of %i: ',[ss,length(DNAregions)]);end;
        SPS(ss)=SubPixDNA(SK(ss),SubIms(ss).pic.*SubIms(ss).bin_im,meta,round(ss/echonr)==ss/echonr);
    end
end

%To make full images
for ss=1:length(DNAregions)
    theseloops=vertcat(SK(ss).LoopPixels{:});
    if ~isempty(theseloops)
        temploop=DNAcontour_sub2fullmap(theseloops,SK(ss).Xoff,SK(ss).Yoff,SK(ss).ImSize(1),size(DNA_bin,1));
        AllLoops=vertcat(AllLoops,temploop);
    end
    tempskel=DNAcontour_sub2fullmap(SK(ss).PixelIdxList,SK(ss).Xoff,SK(ss).Yoff,SK(ss).ImSize(1),size(DNA_bin,1));
    AllSkels=vertcat(AllSkels,tempskel);
    [temppath]=DNAcontour_sub2fullmap(SK(ss).Path,SK(ss).Xoff,SK(ss).Yoff,SK(ss).ImSize(1),size(DNA_bin,1));
    AllPaths=vertcat(AllPaths,temppath);
end


%make full images
sk_im(AllSkels)=true;
path_im(AllPaths)=true;
loop_im(AllLoops)=true;