function [SKstr] = DNAcontour_trace_boundary(SubImStr,SKstr,minlength)
%DNAcontour_trace_boundary Gets an order out of a skeleton by using
%boundary tracing
% uses bwtraceboundary for the outer boundary
% also stores other boundaries, traced with the different 'bwboundaries'' function but optionally throws out any that are
% smaller than minlength


if nargin==0 %test mode
    [SKstr,sk_im,SubImStr]=MakeTestSkel;
end
if nargin<3 ||isempty(minlength)
    minlength=0; % default: keep all
end

if~isfield(SKstr,'EndPoints')
    SKstr.EndPoints=find(bwmorph(SubImStr.sk_im,'endpoints'));
end

if isstruct(SubImStr) %this line allows for just an image to be input to the function
   sk_im=SubImStr.sk_im;
end

[B,~,N,~]=bwboundaries(SubImStr.sk_im,8);

if N~=1
    warning('Found %i objects during boundary trace of skeleton. It should be exactly 1, please review',N)
end
if ~isempty(SKstr.EndPoints)
    [Px,Py]=ind2subFast2D(size(sk_im,1),SKstr.EndPoints(1));
else
    [Px,Py]=ind2subFast2D(size(sk_im,1),find(sk_im==1,1));
end
B1 = bwtraceboundary(sk_im,[Px,Py],'NW');
B{1}=B1;
[blength,~]=cellfun(@size,B); %size of each boundary
keepvector=blength>=minlength;
B=B(keepvector);
SKstr.TrB=B;


if nargin==0 %test mode plotting
    [B4]=bwboundaries(SubImStr.sk_im,4); %obsolete, for comparison with 4-connected boundary
    im2plot=0*sk_im;im2plot4=im2plot;
    B1i=sub2indFast2D(size(sk_im,1),B1(:,1),B1(:,2));
    B4i=sub2indFast2D(size(sk_im,1),B4{1}(:,1),B4{1}(:,2));
    for ii=1:length(B1i)
        im2plot(B1i(ii))=im2plot(B1i(ii))+1;
    end
    for ii=1:length(B4i)
        im2plot4(B4i(ii))=im2plot4(B4i(ii))+1;
    end
    figure(2);DNAcontour_plotim(im2plot)
    figure(4);DNAcontour_plotim(im2plot4)
end
end %main function

function [SK,sk_im,SI]=MakeTestSkel
SK=struct;
sk_im=false(99); %make a fake image
sk_im(50,2:50)=true;
sk_im(10:50,50)=true;
sk_im(10,50:70)=true;
sk_im(10:50,70)=true;
sk_im(50,70:80)=true;
sk_im(40:50,80)=true;
sk_im(40,80:90)=true;
sk_im(40:50,90)=true;
sk_im(30:50,25)=true;
sk_im=sk_im|flipud(sk_im);
sk_im(2:50,2)=true;
sk_im(40:50,40)=true;
sk_im(sub2ind(size(sk_im),20:30,15:25))=true;
sk_im(sub2ind(size(sk_im),45:55,35:45))=true;
sk_im(24,20)=true;
sk_im(sub2ind(size(sk_im),45:55,10:20))=true;
sk_im=rot90(bwmorph(sk_im,'skel','Inf'),3);
SI.sk_im=sk_im;
figure(1);DNAcontour_plotim(sk_im)
end

