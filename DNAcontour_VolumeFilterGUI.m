function [BW,V_min,V_max]=DNAcontour_VolumeFilterGUI(BWin,pic,skipplot,V_min,V_max);
%DNAcontour_VolumeFilterGUI Filters a thresholded image to keep a volume range of regions, with visual feedback
%   Detailed explanation goes here
if nargin<1 %test mode, assumes there is a testimage in this folder
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    tim2=getfield(load(fullfile(thisfolder,'Test Data','testims.mat')),'tim2');
    pic=tim2.data;
    BWin=pic>6e-10;
end
if nargin<3
    skipplot=0;
end

BWstruct= bwconncomp(BWin,8);
RP=regionprops(BWstruct,pic,'Area','MeanIntensity');
RVol=[RP.MeanIntensity].*[RP.Area];

if~skipplot
VFfig=figure;
subplot(4,1,1);nbins=max(1,round(length(RVol)/3));hist(RVol,nbins);
V_min=0.5*median(RVol); %first try
V_max=2*median(RVol); %first try
end
stop_it=0;
Woptions.WindowStyle='normal';
CurrentZoomX=[0 size(BWin,1)];CurrentZoomY=[0 size(BWin,2)];
while ~stop_it
    idxToKeep = find((RVol>V_min)&RVol<V_max);
    BW =ismember(labelmatrix(BWstruct),idxToKeep);
    if skipplot
       stop_it=1;
       break
    end
    figure(VFfig);subplot(4,1,2:4);    
    DNAcontour_plotim(BWin+BW);ImAx=gca;
    colormap(hot);set(ImAx,'Clim',[0 2]);title('included=white, excluded=orange');
    set(ImAx,'xlim',CurrentZoomX);set(ImAx,'ylim',CurrentZoomY);
    answer = inputdlg_tt({'Min Volume','Max Volume'},'Set Volume filter',1,{num2str(V_min),num2str(V_max)},Woptions);
    if isempty(answer) % The 'done' button, aka 'cancel', gives an empty answer
        stop_it=1;
    else
        V_min=sscanf(answer{1},'%g');
        V_max=sscanf(answer{2},'%g');
    end
    CurrentZoomX=get(ImAx,'xlim');CurrentZoomY=get(ImAx,'ylim');
end
if ishghandle(VFfig)
close(VFfig);
end
end

