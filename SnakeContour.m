function [NPx,NPy,SnakeVals]=SnakeContour(Px,Py,ImData,a,b,N_its,DoReverse,DoEcho)
%Active contour method for refining a filament trace. Corrects the trace
%based on image height and trace curvature

%Input data: x and y values of the initial trace, and height (intensity) data
%Calculates a normalized height gradient for the entire picture. On each iteration,
%the coordinates of the trace are corrected towards upward gradient (higher
%height/intensity) values, and low (xy) curvature of the trace.
%Parameters:
%a:         determines weight ratio height/curvature (1 is equal, larger is more height)
%b:         determines total step size per iteration
%N_its:     the number of iterations
%DoReverse: boolean that determines whether iteration direction is reversed
%           for each iteration (matters for curvature). Default = 1,
%           reverse
%Do Echo:   throw iteration info to the command window

%Output: x and y values of corrected trace
%all x and y in pixels.

%to do: a good criterion to stop the iterations automatically


if nargin<6;
    N_its=5;
end
if nargin<7
    DoReverse=1;
end
if nargin<8
    DoEcho=1;
end
test_mode=0;
if nargin==0;
    test_mode=1;mw=200;mh=25;perio=50;sm=mh/10;
    Im0=zeros(mw+20,2*mh+20);
    noiselevel=0.2;
    Px0=(1:mw)+10+noiselevel*randn(1,mw);Px=round(Px0);
    Py0=10+mh*(1+sin(2*pi*(Px/perio)))+noiselevel*randn(1,mw); Py=round(Py0);
    for ii=1:size(Px0,2)
        Im0(Px(ii),Py(ii))=1;
    end
    ImData=100*noiselevel*DNAcontour_GaussSmoothImage(Im0,sm,2*ceil(sm)+1,1)+noiselevel*randn(size(Im0));
    a=4;b=0.1;
    N_its=200;DoReverse=1;
    DoEcho=1;
end
if (isvector(Px) && isvector(Py)) && (size(Px,1)<size(Px,2))&& all(size(Py)==size(Px)) %if row vectors of equal size
    Px=Px'; Py=Py'; %make columns
elseif ~(isvector(Px) && isvector(Py))||any(size(Px)~=size(Py)) %if not vectors of equal size
    error('Input size is wrong')
end

npoints=size(Px,1);
HE=ImData;%height=energy. Other functions are possible too.
[HGy,HGx]=gradient(HE); %Gradients. y and x are reversed in definition
HGt=sqrt(HGy.^2+HGx.^2); %total gradient
try
    HG1=HGt(sub2ind(size(HGt),round(Px),round(Py))); %gradient values on path
catch %not a catch but an error reporting
    size(HGt)
    max(Px),max(Py)
end
HGmedian=median(HG1(:)); HGx=(HGx)/HGmedian; HGy=(HGy)/HGmedian; %normalize by values on path
HFx=griddedInterpolant(HGx);HFy=griddedInterpolant(HGy); %interpolation function that is used later on
NPx=Px;NPy=Py;
abnorm=sqrt(a^2+1); % this is the normalization that takes into account the relative weights for height (a) and curvature (1)
stopcrit=0;rd_old=0;
itcount=0;
%bsteps=[1,[5 2 1 1 1 1 1 1 1 1 1]*b]; %veriable stepping might help?
while ~stopcrit && itcount<N_its  
    itcount=itcount+1;
    %b=bsteps(1+mod(itcount,length(bsteps)));
    if DoReverse && mod(itcount,2)==0
        NPx=flipud(NPx);NPy=flipud(NPy);
        Px=flipud(Px);Py=flipud(Py);
    end
    NPxtemp=NPx;NPytemp=NPy;
    
    %  for pp=(2):(npoints-1)%first we correct for height
    %heigth energy gradient is NN averaged.
    %         LocalEnvX=round(NPx(pp)+grid1);LocalEnvY=round(NPy(pp)+grid1);
    %         HGx_loc=HGx(LocalEnvX,LocalEnvY); %local gradient, positve if increasing x increases height
    %         HGy_loc=HGy(LocalEnvX,LocalEnvY);
    %         HGXdif=sum(Xweight(:).*HGx_loc(:))/9; %sum is faster than mean
    %         HGYdif=sum(Yweight(:).*HGy_loc(:))/9;
    HGXdif=HFx(NPx(2:(end-1)),NPy(2:(end-1)));
    HGYdif=HFy(NPx(2:(end-1)),NPy(2:(end-1)));
    xdif=(a*HGXdif)./abnorm;
    ydif=(a*HGYdif)./abnorm;
    NPx(2:(end-1))=NPx(2:(end-1))+b*xdif;NPy(2:(end-1))=NPy(2:(end-1))+b*ydif;
    %   end
    
    %only going by height will introduce too much lateral scatter, so we
    %also correct for curvature.
    
    dx=NPx(2:end)-NPx(1:end-1);dy=NPy(2:end)-NPy(1:end-1);   %differentials
    ddx=dx(2:end)-dx(1:end-1);ddy=dy(2:end)-dy(1:end-1);     %curvatures
    NPx(2:(end-1))=NPx(2:(end-1))+b*ddx/abnorm; NPy(2:(end-1))=NPy(2:(end-1))+b*ddy/abnorm;
    %2023-08-08: there was no step size in the curvature so I corrected
    %that
    
    % resample to get equal spacing
    try
        distan=[0;cumsum(sqrt(dx.^2+dy.^2))]; %distance
        ldistan=(linspace(0,distan(end),npoints))'; %linearly increasing distance
        NPx=interp1(distan,NPx,ldistan,'linear'); 
        NPy=interp1(distan,NPy,ldistan,'linear'); %resampled
    catch ME
        [distan2,UnInd]=unique(distan,'stable');
        if length(distan2)~= length(distan)
            warning ('non-unique distance found, reducing length')
            ldistan=(linspace(0,distan(end),npoints))';
            NPx=interp1(distan2,NPx(UnInd),ldistan,'linear'); 
            NPy=interp1(distan2,NPy(UnInd),ldistan,'linear'); %resampled
        else 
            rethrow(ME)
        end
    end


    Tdif=sum(sqrt((Px-NPx).^2+(Py-NPy).^2))/npoints; %Total difference with start situation, RMS in pixels
    onestepdif=sqrt((NPxtemp-NPx).^2+(NPytemp-NPy).^2); %change in the difference, RMS(n)/RMS(n-1)
    maxdif=max(onestepdif); %maximum difference
    reldif=sum(onestepdif)./(npoints*Tdif); %relative contribution of this step
    diftrend=rd_old-reldif; %trend in the relative contribution, >0 if this step is smaller
    stopcrit=diftrend<1e-2 && reldif<b/10 &&maxdif<0.050;
    rd_old=reldif;
    if test_mode,  plotlatest;
        %waitforbuttonpress;
        %pause(0.5)
        fprintf('%i iterations: RMS shift %g, max shift %g, trend %g\n',itcount,Tdif,maxdif,diftrend)
    end
end
NPx(1)=2*NPx(2)-NPx(3);%endpoints we simply extrapolate
NPy(1)=2*NPy(2)-NPy(3);%reason to do like this: doing this inside the loop causes the endpoints to contract slowly
NPx(end)=2*NPx(end-1)-NPx(end-2);
NPy(end)=2*NPy(end-1)-NPy(end-2);
ImF=griddedInterpolant(ImData);
SnakeVals=ImF(NPx,NPy);
if DoEcho
    fprintf('%i iterations: RMS shift %g, max shift %g, trend %g\n',itcount,Tdif,maxdif,diftrend)
end
%plotlatest
if test_mode, figure(671);hold off;end
if DoReverse && mod(N_its,2)==0
    NPx=flipud(NPx);NPy=flipud(NPy); %works only if Px,Py is column!
end
    function plotlatest
        figure(671);clf;DNAcontour_plotim(ImData);caxis([0 2]);hold all
        if test_mode
            plot(Px0+0.5,Py0+0.5,'-g','LineWidth',2)
        end
        plot(0.5+Px,0.5+Py,'.k','MarkerSize',12);
        plot(0.5+NPx,0.5+NPy,'-w','LineWidth',2);
        
    end
end
