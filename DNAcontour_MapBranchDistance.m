function [SK,sk_im]=DNAcontour_MapBranchDistance(SK,sk_im,ss,Debug);
%Gets a distance map from thresholded DNA AFM images Makes a skeleton and
%finds endpoints and branchpoints. Starting from each endpoint, nearest
%neighbours are found on the skeleton, stopping at endpoints or
%branchpoints.


%% input parsing
if nargin<4
    Debug=false;
end
if nargin<3||isempty(ss)
    ss=1; %region index, used for error messages
end
if Debug
    firsttime=1;circplot=[];squareplot=[]; %for debug
end
%% finding endpoints, branchpoints

bpim=bwmorph(sk_im,'branchpoints'); %image with junctions =1, rest=0
BranchPoints=find(bpim>0); %list of branchpoints
%an issue is branchpoints that are actually superfluous skeleton points. We recognize these by
%seeing that they have 3 4-connected neighbours
for dd=1:length(BranchPoints)
    [bnn,~]=GetNearestNeighbours(sk_im,BranchPoints(dd)); %whether or not any of the NN are on the skeleton
    bn4=sum(bnn(1:4)>0); %nr of 4 connected neighbours
    zapit= bn4==3 ; %exactly 3 nearest neighbours 4-connected
    if zapit
        sk_im(BranchPoints(dd))=0;
    end
end
%redefine some things on the improved skeleton
SK.PixelIdxList=find(sk_im);
epim=bwmorph(sk_im,'endpoints'); %image with endpoints =1, rest=0
EndPoints=find(epim>0); %list of endpoints
bpim=bwmorph(sk_im,'branchpoints'); %image with junctions =1, rest=0
BranchPoints=find(bpim>0); %list of branchpoints

%there is one more issue that pops op, with points that are connected to
%two branch points and one other point. They are not counted as branch
%points but in this context they should
%20230904: added multiple run of this

f = @(x) sum(x(:));lut = makelut(f,3); %this lut creates an image that has the connectivity nr as it's value
bpt=bwlookup(bpim,lut);bpt(bpim|~sk_im|epim)=0;bpt2=(bpt==2); %this creates an image that is true where there is a non-branch/endpoint with exactly two branchpoints connected on the skeleton
nrun_addbp=0;
while any(bpt2(:)) && nrun_addbp<4 %limit this in case it runs out of hand
    bpim=bpim|bpt2;
    BranchPoints=find(bpim>0); %list of branchpoints
    bpt=bwlookup(bpim,lut);bpt(bpim|~sk_im|epim)=0;bpt2=(bpt==2);
    nrun_addbp=nrun_addbp+1;
    if nrun_addbp==4
        warning('4 runs of adding branchpoints, this is weird')
    end
end

%contrary to what you would expect, sometimes something is called an
%endpoint but is actually sandwiched between points. That seems to be a bug in the builtin code. I clean up those
%points here
threeinarow=[1 5 8;4 7 8;3 6 7;2 5 6];
for cc=1:length(EndPoints)
    [enn,ennind]=GetNearestNeighbours(sk_im,EndPoints(cc));
    zapit= numel(ennind(enn==1))>3 || ... %more than 3 nearest neighbours
        (numel(ennind(enn==1))==3 && ~ismember(find(enn)',threeinarow,'rows')) ; %or three that are not in a row
    if zapit
        epim(EndPoints(cc))=0;
    end
end
EndPoints=find(epim>0);

%% prepare mapping branches
con_im=zeros(SK.ImSize,'uint16'); %image that contains the number of times each point has been traced
if(length(EndPoints)==2 &&isempty(BranchPoints))
    NrEpBranch=1; %if there are only two endpoints and no branches (simple line), there is only one branch.
else
    NrEpBranch=length(EndPoints); %if not there is one branch for each endpoint.
end
BranchIdx=cell([1,NrEpBranch]);%initialization
BranchDist=BranchIdx; %initialization
skipmap=0; %debug variable to skip the mapping
if~skipmap

    ThisSkel=SK.PixelIdxList; %List of points of this skeleton.
    np=numel(ThisSkel); %nr of points
    %% first we do the endpoints
    if ~isempty(EndPoints)
        for ee=1:NrEpBranch %for each endpoint
            ThisEndPoint=EndPoints(ee); %select one endpoint
            ep_sk_id=find(ThisEndPoint==ThisSkel,1); %index in the pixellist that corresponds to the endpoint
            %initialization;
            thispoint=ThisEndPoint;
            IndOrder=zeros(np,1);
            IndOrder(1)=ep_sk_id;
            d=-1*ones(np,1);d(1)=0;
            con_im(thispoint)=con_im(thispoint)+1;
            for jj=2:np
                visual_feedback
                if  any(BranchPoints==thispoint) %if this is a branchpoint
                    break %then quit
                elseif jj>2 &&any (EndPoints==thispoint) %if endpoint
                    break %then quit
                else
                    ThisSkel(IndOrder(jj-1))=0; %by deleting the previous point we avoid backtracking
                    [~,NNindsL] = GetNearestNeighbours(sk_im,thispoint,[],'linear'); %linear indices (wrt image) of the neighbours of the point we are investigating
                    [onskel,skelind]=ismember_A(NNindsL,ThisSkel); %1 on the neigbouring points that are on the skeleton
                    if any (onskel)
                        whichneighbour=find(onskel,1,'first'); % this should find 4-connected points first due to the order in 'neighbours'
                        dtemp=1+((sqrt(2)-1)*(whichneighbour>4)); %take the distance, 1 for straight, sqrt 2 for diagonal
                        d(jj)=d(jj-1)+dtemp; %add to previous distance and store
                        IndOrder(jj)=skelind(whichneighbour); %store the order of the points
                        thispoint=NNindsL(whichneighbour); %set the new base point for the next loop
                        con_im(thispoint)=con_im(thispoint)+1;
                    else
                        warning('no adjacent points and no stop criterion in endpoint branch (region %i, branch %i)! There must be something wrong',ss,ee)
                        break
                    end
                end
            end
            BranchIdx{ee}=SK.PixelIdxList(IndOrder(IndOrder>0));
            BranchDist{ee}=d(d>=0);
        end
    else     %If there are no endpoints for this region
        %that happens in case of a braid or 'perfect circle'
        ee=0;
    end

    %% now we tackle branchpoints not connected to an endpoint

    ThisSkel=SK.PixelIdxList;%select the skeleton we are working in, needs to be reloaded here
    UnchartedRegions=~con_im(ThisSkel); %all the places on the skeleton we have not counted yet
    BranchCounter=ee;
    %% If there are no endpoints or branchpoints for this region, that happens in case of a 'perfect circle'
    if isempty(EndPoints) && isempty(BranchPoints)
        %we then run the length calculation over the whole skeleton without
        %checking for endpoints etc
        thispoint=ThisSkel(1);
        IndOrder=zeros(np,1);  IndOrder(1)=1; d=-1*ones(np,1);d(1)=0; %initialization
        con_im(thispoint)=con_im(thispoint)+1;
        for jj=2:np
            ThisSkel(IndOrder(jj-1))=0; %by deleting the previous point we avoid backtracking
            [~,NNindsL] = GetNearestNeighbours(sk_im,thispoint,[],'linear'); %linear indices (wrt image) of the neighbours of the point we are investigating
            [onskel,skelind]=ismember_A(NNindsL,ThisSkel); %1 on the neigbouring points that are on the skeleton
            if any (onskel)
                whichneighbour=find(onskel,1,'first'); % this should find 4-connected points first due to the order in 'neighbours'
                dtemp=1+((sqrt(2)-1)*(whichneighbour>4)); %take the distance, 1 for straight, sqrt 2 for diagonal
                d(jj)=d(jj-1)+dtemp; %add to previous distance and store
                IndOrder(jj)=skelind(whichneighbour); %store the order of the points
                thispoint=NNindsL(whichneighbour); %set the new base point for the next loop
                con_im(thispoint)=con_im(thispoint)+1;
            else
                warning('no adjacent points in single connected region (%i)! There must be something wrong',ss)
                break
            end
        end
        BranchIdx{1}=SK.PixelIdxList(IndOrder(IndOrder>0));
        BranchDist{1}=d(d>=0);

    elseif ~isempty(BranchPoints)
        %% if there are branchpoints.
        for ff=1:size(BranchPoints,1) %for each branchpoint
            if any(UnchartedRegions)
                ThisBranchPoint=BranchPoints(ff); %select one branchpoint
                OBP=BranchPoints(BranchPoints~=ThisBranchPoint);%the other branchpoints
                [BP_unc,BPNNinds] = GetNearestNeighbours(con_im,ThisBranchPoint,[],'linear'); % BP_unc has all the neighbours of the branchpoint and contains 1 where there is no connections
                BP_unc(isnan(BP_unc))=1;
                BP_unc=~BP_unc; % invert to get the unconnected points. This is 10x faster than inverting con_im in the line above
                BP_unc=BP_unc&ismember_A(BPNNinds,ThisSkel); %weed out the points off the skeleton
                bp_sk_id=find(ThisBranchPoint==ThisSkel,1); %index in the pixellist that corresponds to the endpoint
                %first we handle the case of adjacent branchpoints
                [BP_Conn,ConnBP]=ismember_A(BPNNinds,OBP); %BP_Conn is 1 for each neighbour that is a branchpoint, ConnBP tells you which branchpoint it is
                for cc=1:8
                    if BP_Conn(cc) && ConnBP(cc)>(ff-1) %This means two adjacent branchpoints. Second rule is to avoid double counting
                        BranchCounter=BranchCounter+1;
                        BranchDist{BranchCounter}=[0; 1+(cc>4)*sqrt(2)]; %naturally, the branch has only 2 points
                        BranchIdx{BranchCounter}=[ThisBranchPoint; OBP(ConnBP(cc))];
                        con_im([ThisBranchPoint OBP(ConnBP(cc))])=con_im([ThisBranchPoint OBP(ConnBP(cc))])+uint16([1 1]);
                        visual_feedback
                    end
                end
                %now we map each branch going out from a branchpoint
                lc=0;%lc is a loop counter for the while loop running over the nearest neighbours of the branchpoint
                doublecheck=zeros(4,1); %this is needed below
                while any(BP_unc)&&lc<8 %lc<8 avoids getting stuck in this loop in case of an error
                    %initialization;
                    thispoint=ThisBranchPoint;
                    lc=lc+1; %check how many times we go through this loop
                    BranchCounter=BranchCounter+1;
                    con_im(thispoint)=con_im(thispoint)+1; %keep track of which points we've covered
                    IndOrder=zeros(np,1);%This records the positions in the branch
                    try
                    IndOrder(1)=bp_sk_id;
                    catch
                        blah=1;
                        %because the error stopping doesn't work
                    end
                    d=-1*ones(np,1);d(1)=0; %distance
                    for jj=2:np
                        visual_feedback
                        if  jj>2 && any(BranchPoints==thispoint) %if this is a branchpoint
                            break %then quit
                        elseif  any (EndPoints==thispoint) %if endpoint
                            break %then quit
                        else
                            [TP_unc,NNindsL] = GetNearestNeighbours(con_im,thispoint,[],'linear'); % TP_unc has all the neighbours of the current point and contains 0 where there is no connections
                            TP_unc(isnan(TP_unc))=1; %On the edges of the image, we get NaNs.
                            TP_unc=~TP_unc; % invert to get the unconnected points
                            [onskel,skelind]=ismember_A(NNindsL,ThisSkel); %1 on the neigbouring points that are on the skeleton
                            TP_unc(~onskel)=0; %weed out the points off the skeleton
                            %In principle we only step to unconnected
                            %points, but we need to add some exceptions for
                            %branchpoints that were already connected earlier
                            OBP_NN=ismember_A(NNindsL,OBP); %has ones for neighours that are branchpoints, but not the startpoint of this branch
                            BP_NN=find(BPNNinds==thispoint,1);% Has a value if this is a neighbour of the startpoint of this branch
                            if ~isempty(BP_NN) && BP_NN<5 %if this is a 4-connected neighbour of the startpoint of this branch
                                ONBP=ismember_A(NNindsL,BPNNinds(1:4)); %Other Neighbours of this point that are also 4-connected neighbours of this BranchPoint
                                TP_unc(ONBP)=0; %Those points are considered to be a different branch
                            end
                            if any(OBP_NN(1:4))&&~any(TP_unc(1:4)) %if another branchpoint is a 4-connected neighour of this point, and no other points are 4-connected
                                if thispoint==ThisBranchPoint %branchpoint to branchpoint branches are special
                                    dc=NNindsL(1:4)~=doublecheck; %have we not counted this before?
                                    TP_unc(1:4)=(OBP_NN(1:4)&dc); %then we will step there
                                    whichneighbour=find(TP_unc(1:4),1,'first'); %that will be the one we step to
                                    doublecheck(whichneighbour)=NNindsL(whichneighbour);%but only this time
                                else
                                    TP_unc=TP_unc|OBP_NN; %we don't have to worry about double counting for longer branches
                                end
                            elseif any(OBP_NN) && ~any(TP_unc) % or if a branchpoint is the only neighbour to connect to
                                TP_unc=TP_unc|OBP_NN;
                            end
                            if any (TP_unc)
                                whichneighbour=find(TP_unc,1,'first'); % this should find 4-connected points first due to the order in 'neighbours'
                                dtemp=1+((sqrt(2)-1)*(whichneighbour>4)); %take the distance, 1 for straight, sqrt 2 for diagonal
                                d(jj)=d(jj-1)+dtemp; %add to previous distance and store
                                IndOrder(jj)=skelind(whichneighbour); %store the order of the points
                                thispoint=NNindsL(whichneighbour); %set the new base point for the next loop
                                con_im(thispoint)=con_im(thispoint)+1;
                            else
                                [~,circlecheckinds] = GetNearestNeighbours(sk_im,thispoint,[],'linear');
                                if any (circlecheckinds==ThisBranchPoint)
                                    %we're back where we started, which
                                    %should mean we're in a circular
                                    %branch, and all is fine
                                    %fprintf('circle! %i - %i',ss,BranchCounter)
                                    IndOrder(jj)=IndOrder(1);
                                    con_im(ThisBranchPoint)=con_im(ThisBranchPoint)+1;
                                else
                                    warning('no adjacent points and no stop criterion in a branchpoint branch. There must be something wrong. Region %i, Branch %i',ss,BranchCounter)
                                end
                                break
                            end
                        end
                    end
                    BranchIdx{BranchCounter}=SK.PixelIdxList(IndOrder(IndOrder>0));
                    BranchDist{BranchCounter}=d(d>=0);
                    [BP_unc,BPNNinds] = GetNearestNeighbours(con_im,ThisBranchPoint,[],'linear'); % BP_unc has all the neighbours of the branchpoint and contains 0 (1 after inversion below) where there is no connections
                    BP_unc(isnan(BP_unc))=1;
                    BP_unc=~BP_unc; % invert to get the unconnected points.
                    BP_unc=BP_unc&ismember_A(BPNNinds,ThisSkel); %weed out the points off the skeleton
                end
            end
        end
    end
    %There are cases when the code above fails to find connection between two
    %adjacent points, if one of those points is a branch/endpoint. We correct that here.
    EPBP=[EndPoints;BranchPoints];
    if length(EPBP)>=2
        for ii=1:(length(EPBP)-1)
            for jj=(ii+1):length(EPBP)
                a=EPBP(ii);b=EPBP(jj); %shorthand
                [~,TheseNeighbours] = GetNearestNeighbours(sk_im,a,[],'linear');  %get the neighbours
                if any (TheseNeighbours==b) %if this other point is a neighbour
                    IsCovered=false;
                    for kk=1:length(BranchIdx)
                        IsCovered= numel(BranchIdx{kk})==2 && ~or(all(BranchIdx{kk}==[a;b]),all(BranchIdx{kk}==[b;a])); % a complex line to check if these two points form a complete branch already
                        if IsCovered
                            break
                        end
                    end
                    if ~IsCovered
                            BranchIdx{kk+1}=[a;b]; % if not, we add this branch to the list
                            BranchDist{kk+1}=1+(sqrt(2)-1)*(find(TheseNeighbours==b,1,'first')>4); % 1 if 4-connected, sqrt(2) if 8 connected
                            warning('Creating two point branch between %i and %i',ii,jj)
                    end
                end
            end
        end
    end

    %any hanging branches (for some reason adjacent to , but not including end or branchpoint) are corrected here
    for kk=1:length(BranchIdx)
        BS=BranchIdx{kk}(1);BE=BranchIdx{kk}(end); %first and last points of the branch
        if ~any(BS==EPBP) %if the start point is not an end or branchpoint
            [~,TheseNeighbours] = GetNearestNeighbours(sk_im,BS,[],'linear'); %get the neighbours of the start point
            PointToAdd=find(ismember_A(TheseNeighbours,EPBP),1,'first'); %find the first one that is an end or branchpoint
            if ~isempty (PointToAdd)
                BranchIdx{kk}=[TheseNeighbours(PointToAdd);BranchIdx{kk}]; %add it to the start of the branch
            else
                warning('Start of branch %i is not connected to any branch or endpoint even after correction!',kk)
            end
        elseif ~any(BE==EPBP) %mutatis mutandis for the end point
            [~,TheseNeighbours] = GetNearestNeighbours(sk_im,BE,[],'linear'); %get the neighbours of the end point
            PointToAdd=find(ismember_A(TheseNeighbours,EPBP),1,'first'); %find the first one that is an end or branchpoint
            if ~isempty (PointToAdd)
                BranchIdx{kk}=[BranchIdx{kk};TheseNeighbours(PointToAdd)]; %add it to the end of the branch
            else
                warning('End of branch %i is not connected to any branch or endpoint even after correction!',kk)
            end
        end
    end
end
SK.BranchIdx=BranchIdx;
SK.BranchDist=BranchDist;
SK.EndPoints=EndPoints;
SK.BranchPoints=BranchPoints;



    function visual_feedback
        if Debug
            if firsttime
                figure(1);clf;DNAcontour_plotim(uint16(sk_im)+con_im); Dbax=gca;
                hold all;
                [x,y]=ind2subFast2D(size(sk_im,1),BranchPoints);
                plot(x,y,'ys','MarkerSize',15);
                [x,y]=ind2subFast2D(size(sk_im,1),EndPoints);
                plot(x,y,'yx','MarkerSize',15);
                if exist('ThisBranchPoint','var'), workingpoint=ThisBranchPoint; else workingpoint=ThisEndPoint; end
                set(groot,'CurrentFigure',1);
                [x,y]=ind2subFast2D(size(sk_im,1),workingpoint);
                squareplot=plot(x,y,'gs','MarkerSize',15);
                [x,y]=ind2subFast2D(size(sk_im,1),thispoint);
                circplot=plot(x,y,'go','MarkerSize',15);
                firsttime=0;
            end
            if exist('ThisBranchPoint','var'), workingpoint=ThisBranchPoint; else workingpoint=ThisEndPoint; end
            set(groot,'CurrentFigure',1);
            [squareplot.XData,squareplot.YData]=ind2subFast2D(size(sk_im,1),workingpoint);
            [circplot.XData,circplot.YData]=ind2subFast2D(size(sk_im,1),thispoint);
            refresh
            pause(0.05);
        end
    end
if Debug
    br_im=zeros(size(sk_im));
    for kk=1:length(BranchIdx)
        br_im(BranchIdx{kk})=br_im(BranchIdx{kk})+1;
    end
    figure(1);clf;
    DNAcontour_plotim(br_im)
    hold off
end
end


