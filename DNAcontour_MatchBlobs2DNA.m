function [BlobRegions] = DNAcontour_MatchBlobs2DNA(BlobRegions,SK,pathim,sk_im,on_DNA_thresh,doplots)
%Matches blobs to their respective DNA positons
%
[pathdist,pathID]=bwdist(pathim);
[skdist]=bwdist(sk_im);
%make a sort of 'labelmatrix' function for SK structure. Can't uses labelmatrix
%because it is not guaranteed the order will be the same
Lmat=zeros(size(sk_im),'uint16');
for ss=1:length(SK)
    xsub=SK(ss).PixelList(:,1);ysub=SK(ss).PixelList(:,2);
    x=xsub+SK(ss).Xoff;y=ysub+SK(ss).Yoff;
    lind=sub2indFast2D(size(Lmat,1),x,y);
    Lmat(lind)=ss;
end

if doplots
    DNAcontour_plotim(pathdist.*(pathdist<on_DNA_thresh));hold all
    title('path distances and colocalization')
end

for ii=1:length(BlobRegions)
    blobx=BlobRegions(ii).WeightedCentroid(2);
    bloby=BlobRegions(ii).WeightedCentroid(1);
    [blobxp,blobyp]=round_to_index(size(pathim),blobx,bloby);
    BlobRegions(ii).onPath=pathdist(blobxp,blobyp)<on_DNA_thresh;
    BlobRegions(ii).onDNA=skdist(blobxp,blobyp)<on_DNA_thresh;
    if BlobRegions(ii).onDNA
        if doplots
            plot(blobx,bloby,'og','MarkerSize',12)
        end
        if BlobRegions(ii).onPath
            p_idx=pathID(blobxp,blobyp);[p_x,p_y]=ind2sub(size(pathID),p_idx);
            ss=Lmat(p_idx);BlobRegions(ii).blob_path=ss;
            p_lind=sub2ind(SK(ss).ImSize,p_x-SK(ss).Xoff,p_y-SK(ss).Yoff); %linear index in the subimage
            pp=find(SK(ss).Path==p_lind);
            BlobRegions(ii).blob_pos=pp;
            BlobRegions(ii).blob_dist=SK(ss).Dist(pp);
        else
            BlobRegions(ii).blob_path=0;
            BlobRegions(ii).blob_pos=0;
            BlobRegions(ii).blob_dist=-1;
        end
    end
end
hold off

end

