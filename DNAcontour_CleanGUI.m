function [picclean,p]=DNAcontour_CleanGUI(pic);
%DNAcontour_CleanGUI Cleans lines from an image with visual feedback and user input
%   Detailed explanation goes here
if nargin<1
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    tim1=getfield(load(fullfile(thisfolder,'testims.mat')),'tim1');
    pic=tim1.data;
end
p.filtsize=7;
p.linethreshold=1;
p.minlinelength=20;
p.direction='x'; %that doesn't usually have to be adjusted
 

Cfig=figure;
stop_it=0;
Woptions.WindowStyle='normal';
CurrentZoomX=[0 size(pic,1)];CurrentZoomY=[0 size(pic,2)];
while ~stop_it
    pars.cleaner=p;
    picclean=DNAcontour_unscar_hough(pic,pars,0);
    picclean=DNAcontour_bgsub(picclean,{'LBLO',},1.1,0,1); %redo line sub
    figure(Cfig);
    subplot(1,2,2);DNAcontour_plotim(pic); ImAx1=gca;title('Original');
    subplot(1,2,1);DNAcontour_plotim(picclean); ImAx2=gca;title('Cleaned');CmapLims=get(ImAx2,'Clim');
    linkaxes([ImAx1,ImAx2],'xy');set(ImAx1,'xlim',CurrentZoomX);set(ImAx1,'ylim',CurrentZoomY);set(ImAx1,'clim',CmapLims);
    answer = inputdlg_tt({'Filter height (nr of lines)','Line threshold (SD''s)','Minimum line length (pixels)'},'Set parameters',1,{num2str(p.filtsize),num2str(p.linethreshold),num2str(p.minlinelength)},Woptions);
    if isempty(answer) % The 'done' button, aka 'cancel', gives an empty answer
        stop_it=1;
    else
         p.filtsize=ceil(sscanf(answer{1},'%i'));p.linethreshold=sscanf(answer{2},'%g');p.minlinelength=sscanf(answer{3},'%i');
    end
    if ishghandle(Cfig)
        CurrentZoomX=get(ImAx1,'xlim');CurrentZoomY=get(ImAx1,'ylim');
    end
end
pars.cleaner=p;
picclean=DNAcontour_unscar_hough(pic,pars,0); %repeat to be sure we use the latest values. note there's no bgsub included in this step
if ishghandle(Cfig)
    close(Cfig);
end
end

