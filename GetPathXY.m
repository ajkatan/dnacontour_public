function [PathX,PathY,Psub]=GetPathXY(SKsub)

[PathX,PathY]=ind2sub(SKsub.ImSize,SKsub.Path); 
PathX=PathX-floor(SKsub.BoundingBox(2)); %boundingbox is transposed!
PathY=PathY-floor(SKsub.BoundingBox(1));
Psub=false(SKsub.BoundingBox(4),SKsub.BoundingBox(3));
Psub(sub2ind(size(Psub),PathX,PathY))=true;

