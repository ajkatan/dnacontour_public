function [ImpPath,ImpSK,fign,ClickSet]=DNAcontour_ClickAndImprove2(SK,SubIm,nr,fign,c_range,colmap,extrapoints,preloadpoints)
%manually click branchpoints to set an order and improve the path finding
%completely re-made function compared to an earlier version
%This one is based on a subfunction 'GetClickedPath' which takes care of
%most of the path finding
%Shows an image with the auto-found path, the branchpoints and endpoints,
%then uses ginput to get information from the user.
%First click is the startpoint. Second click is the endpoint. For the next
%clicks, left click adds a waypoint before the endpoint. Right click adds a
%waypoint after the startpoint. If it all goes wrong you middle click and
%start again. Enter means you're done, you can press it anytime, if no editing of the auto path needs
%to be done, you can skip.
%you can supply extra points (previously clicked points of interest for
%example, to help you find the way. These should be in a vector of nx2 as
%xy pairs.
%you can even say that someof these points should be waypoints. In that
%case, provide the indices into 'extrapoints' in 'preloadpoints', where the
%order in preloadpoints should be ['start', 'end', intermediate 1, ....
%intermediate n]
%the
if nargin <8
    preloadpoints=[]; %isempty is handled below
end
if nargin <7
    extrapoints=[]; %isempty is handled below
end
if nargin <6 || isempty(colmap)
    colmap=hot;
end
if nargin <5 || isempty(c_range)
    c_range=1e-9*[-0.5 3];
end
if nargin <4 || isempty(fign)
    fign=figure;
end
if nargin <3 || isempty(nr)
    nr=0; %'nr' is used for error message generation if looped over multiple molecules
end
ImpPath=SK.Path; ImpSK=SK; %output generation in case stuff fails or you decide to skip
indsize=size(SubIm.pic,1);
PlotInitialImage
nrun=0;
ClickSet=[];
but=1;
while any(but==[1 2 3])
    nrun=nrun+1;
    pointsupplied=numel(preloadpoints) >=nrun;
    if pointsupplied
        ClickSet=[ClickSet;extrapoints(preloadpoints(nrun),1:2)];
        p_a=scatter(ClickSet(nrun,1),ClickSet(nrun,2),15,'MarkerEdgeColor','red');
        switch nrun 
            case 1
                p_a.Marker='x'; %plot it, start point is cross
            case 2
                p_a.Marker='+'; %plot it end point is plus
            otherwise
                p_a.Marker='o'; %plot it
        end
    else
        try
            [ClickedX,ClickedY,but]=ginput(1);
        catch %if you delete the figure
            fign=0;
            break
        end
        if isempty(ClickedX)
            break
        else
            %do nothing, just continue
        end

        if nrun==1 && any(but==[1 3]) %start point selection
            ClickSet=[ClickedX,ClickedY];
            plot(ClickedX,ClickedY,'rx','MarkerSize',15); %plot it, start point is cross
            disp('Selected start point')
        elseif nrun==2 && any(but==[1 3]) %endpoint selection
            disp('Selected end point')
            ClickSet=[ClickSet;[ClickedX,ClickedY]];
            plot(ClickedX,ClickedY,'r+','MarkerSize',15); %plot it end point is plus
        elseif but==1 %left mouse click
            ClickSet=[ClickSet((1:end-1),:);[ClickedX,ClickedY];ClickSet(end,:)]; %append before endpoint
            plot(ClickedX,ClickedY,'ro','MarkerSize',15); %plot it
        elseif but==2 %middle click to reset to zero
            ClickSet=[];nrun=0;
            preloadpoints=[];
            PlotInitialImage
        elseif but==3 %right mouse click
            ClickSet=[ClickSet(1,:);[ClickedX,ClickedY];ClickSet(2:end,:)]; %append after startpoint
            plot(ClickedX,ClickedY,'ro','MarkerSize',15); %plot it
        else
            but=pi; % using the enter button gives an empty matrix, which gives an error, so I give it an arbitrary value of pi
        end
    end
    if nrun>1
        ImpPath=[];
        for ii=1:(length(ClickSet)-1) % we need to get 'subpaths' from all the clicked points to the next clicked point
            tempstruct.Points=ClickSet((ii:ii+1),:); %cast in right form for GetClickedPath
            try
                [ClickedPathTemp,ImpSK]=GetClickedPath(ImpSK,tempstruct,[1,2],false);
            catch
                warning('no change in nr %i because of error,',nr)
                ImpSK=SK;
                ImpPath=SK.Path;
                break
            end
            if ii>1
                if ClickedPathTemp(1)==ImpPath(end)
                    %do nothing
                elseif ClickedPathTemp(end)==ImpPath(end)
                    ClickedPathTemp=flipud(ClickedPathTemp);
                else
                    warning('non connected path being added')
                end
            end
            ImpPath=unique([ImpPath;ClickedPathTemp],'stable'); %Append to get the full path and remove duplicates
            ImpSK.Path=ImpPath;
            ImpSK.Length=numel(ImpPath);
            if isfield(ImpSK,'ClickSet'), ImpSK.ClickSet=ClickSet; end % don't create new fields here, do it after calling
        end
        [xp,yp]=ind2subFast2D(indsize,ImpPath);
        set(groot,'CurrentFigure',fign);
        plot(xp,yp,'LineWidth',2);
    end

end
if fign~=0, figure(fign);hold off; end % switch hold off unless figure was deleted

    function PlotInitialImage
        figure(fign);clf;
        [OrigPathx,OrigPathy]=ind2subFast2D(indsize,SK.Path);
        DNAcontour_plotim(SubIm.pic.*(1+SubIm.sk_im),[OrigPathx,OrigPathy]);hold all;set(gca,'Clim',c_range);
        colormap(colmap);
        EB=vertcat(SK.EndPoints,SK.BranchPoints); % In linear index, we need this to select the right values from the matrix
        [EBx,EBy]=ind2subFast2D(indsize,EB); % in image coordinates
        labelpoints=cellfun(@num2str,num2cell(1:length(EB)),'UniformOutput',false); %creates labels for all the points
        text(EBx,EBy,labelpoints);
        if~isempty(extrapoints)
            scatter(extrapoints(:,1),extrapoints(:,2),16,'blue')
        end
    end
end
