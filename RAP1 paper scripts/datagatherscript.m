savefolder='C:\Users\ajkatan\surfdrive\RAP1_analysis';
filelist=dir([savefolder '\*.mat']);
nfiles=length(filelist);
binvec=[0:5:250];
%%
searchindex={...
    'BB16_PLL_10x', 'R16G6', 274; ...
    'DNA221','R16G10', 302 ; ...
    '222a','R16G15', 337 ; ...
    'DNA223','R16G20', 372 ; ...
    'BB8', 'R16G35', 477 ; ...
    'AKTA','bare16G6', 274; ...
    };
nmols=length(searchindex);
% CL221=[194 302 106 602]';
% CL222=[199 337 106 642]';
% CL223=[204 372 106 682]';
% CL8=[103 477 218 798]';
% CL16=[103 274 189 566]'; %contour length in basepairs

%%
D=cell(nfiles,1);DS=D;SL=D;validmols=D; V=D;%initialize
if exist('allfig','var') && ishghandle(allfig)
    figure(allfig);clf
else
    allfig=figure;
end
CP=repmat(struct('ClickPoints',[]),1,nfiles);
molindex=zeros(1,nfiles);
ppf=2;fpc=2;%plots per file, files per column
nrows=ppf*fpc;ncols=ceil(nfiles/fpc);
for ii=1:nfiles
    valD=(regexp(filelist(ii).name,searchindex(:,1)));
    for jj=1:nmols
        if ~isempty(valD{jj})
            molindex(ii)=jj;
        end
    end
    disp(filelist(ii).name)
    CP(ii)=load(filelist(ii).name,'ClickPoints');
    temp=load(filelist(ii).name,'PixToBP');
    ScaleFactor(ii)=0.34*temp.PixToBP;
    % Snakes{ii}=[CP(ii).ClickPoints.Snake];
    validmols{ii}=false(length(CP(ii).ClickPoints),1);
    SL{ii}=zeros(length(CP(ii).ClickPoints),1);    DS{ii}=SL{ii}; V{ii}=SL{ii};%initialize
    ndist=size([CP(ii).ClickPoints.Distances],1);
    D{ii}=zeros(ndist,length(CP(ii).ClickPoints));
    for jj=1:length(CP(ii).ClickPoints)
        if~isempty( CP(ii).ClickPoints(jj).Snake)  %this ensures all sizes are the same
            validmols{ii}(jj)=true;
            SL{ii}(jj)=ScaleFactor(ii)*CP(ii).ClickPoints(jj).Snake.SnakeLength;
            D{ii}(:,jj)=ScaleFactor(ii)*[CP(ii).ClickPoints(jj).Distances];
            DS{ii}(jj)= ScaleFactor(ii)*CP(ii).ClickPoints(jj).Snake.E2E;
            V{ii}(jj)=mean(CP(ii).ClickPoints(jj).Snake.SnakeVals)*CP(ii).ClickPoints(jj).Snake.SnakeLength; %total volume of the binding region
        end
    end
    colnr=ceil(ii/fpc);
    rownr=1+ppf*(ii-1)-(colnr-1)*ppf*fpc;
    figure(allfig);subplot(nrows,ncols,colnr+(rownr-1)*ncols);
    histogram(SL{ii}(validmols{ii}),binvec);hold all
    histogram(D{ii}(2,validmols{ii}),binvec)
    histogram(DS{ii}(validmols{ii}),binvec)
    xlabel('length(nm)')
    if ii==1,legend({'contour length','end to end of clicks','end to end of snake'},'Location','NorthWest');end
    hold off
    title(searchindex{molindex(ii),2})
    subplot(nrows,ncols,colnr+rownr*ncols);
    try
        h=histogram(D{ii}(2,validmols{ii})./(SL{ii}(validmols{ii})'),linspace(0,1,15));%hold all
    catch
        fprintf(1,'No relative histogram?\n')
        size(D{ii}(2,validmols{ii}))
        size(SL{ii}(validmols{ii}))
        sum(validmols{ii})
    end
    h.Normalization='count';
    %plot(SL{ii},SL{ii}); hold off
    xlabel('contour/E2E');ylabel('freq')
    %xlim([0 250]);ylim([0 200])
    if ii==1,legend({'E2E vs CL','CL'},'Location','NorthWest');end
end
%% sort by molecule type
SLM=cell(nmols,1); DM=SLM;DSM=SLM;validmolsM=SLM;RelLength=SLM;Vol=SLM;
ppmol=6;molpc=1;%plots per molecule, molecules per column
nrows=ppmol*molpc;ncols=ceil(nmols/molpc);
if exist('molfig','var') && ishghandle(molfig)
    figure(molfig);clf
else
    molfig=figure;
end
cumgraph=figure(333);clf;cumgraph2=figure(444);clf;
for ii=1:nmols
    SLM{ii}=vertcat(SL{molindex==ii});
    DSM{ii}=vertcat(DS{molindex==ii});
    VM{ii}=vertcat(V{molindex==ii});
    DM{ii}=[D{molindex==ii}];
    validmolsM{ii}=vertcat(validmols{molindex==ii});
    colnr=ceil(ii/(molpc));
    figure(molfig);
    %length
    rownr=1+(ii-1)*ppmol-(colnr-1)*ppmol*molpc; plotnr=colnr+(rownr-1)*ncols;    subplot(nrows,ncols,plotnr);
    h=histogram(SLM{ii}(validmolsM{ii}),binvec); title(searchindex(ii,2));xlabel('contour length')
    line(ones(1,2)*0.34*searchindex{ii,3},[0,max(h.Values)],'LineWidth',3,'Color','red')
    %E2E
    rownr=rownr+1; plotnr=colnr+(rownr-1)*ncols;    subplot(nrows,ncols,plotnr);
    histogram(DM{ii}(2,validmolsM{ii}),binvec); xlabel('E2E (clicks)')
    %
    rownr=rownr+1; plotnr=colnr+(rownr-1)*ncols;    subplot(nrows,ncols,plotnr);
    histogram(DSM{ii}(validmolsM{ii}),binvec);xlabel('E2E (snakes)')
    %
    rownr=rownr+1; plotnr=colnr+(rownr-1)*ncols;    subplot(nrows,ncols,plotnr);
    RelLength{ii}=DSM{ii}(validmolsM{ii})./(SLM{ii}(validmolsM{ii})); relvals=linspace(0,1,20);
    RH(ii)=histogram(RelLength{ii},relvals);xlabel('relative E2E');
    RH(ii).Normalization='probability';
    %
    rownr=rownr+1; plotnr=colnr+(rownr-1)*ncols;    subplot(nrows,ncols,plotnr);
    plot(RH(ii).BinEdges(2:end),cumsum(RH(ii).BinCounts))
    xlabel('relative E2E')
%
    rownr=rownr+1; plotnr=colnr+(rownr-1)*ncols;    subplot(nrows,ncols,plotnr);
    VHM(ii)=histogram(VM{ii}(validmolsM{ii})./SLM{ii}(validmolsM{ii}),25);xlabel('mean height');
    VHM(ii).Normalization='count';
    figure(cumgraph)
    plot(searchindex{ii,3}*RH(ii).BinEdges(2:end),cumsum(RH(ii).Values),'LineWidth',2);hold("all");
    xlabel('scaled relative E2E (bp)');ylabel('cumulative probability')
    figure(cumgraph2);hold("all")
    plot(RH(ii).BinEdges(2:end),cumsum(RH(ii).Values),'LineWidth',2);hold("all");
    xlabel('relative E2E');ylabel('cumulative probability')
end
figure(cumgraph);hold("off");legend(searchindex{:,2})
figure(cumgraph2);hold("off");legend(searchindex{:,2})

%% End to end 2D histograms
distbins=1:2:200;e2ebins=distbins;stepsize=1;doplots=0;
P2P=cell(length(CP),1);DC=P2P;
emptystruct=DNAcontour_E2Edist(1,1,1,distbins,e2ebins,stepsize,false);
emptystruct.N_analyzed=0;
E2Ehist=repmat(emptystruct,nmols,1);

for jj=1:length(CP)
    kk=molindex(jj);
for ii=1:length(CP(jj).ClickPoints)
    if ~isempty(CP(jj).ClickPoints(ii).Snake)
        X=ScaleFactor(jj)*CP(jj).ClickPoints(ii).Snake.SnakeX;
        Y=ScaleFactor(jj)*CP(jj).ClickPoints(ii).Snake.SnakeY;
        D=ScaleFactor(jj)*CP(jj).ClickPoints(ii).Snake.SnakeD;
        if numel(D)>1
        [temphist,tempp2p,tempdc] = DNAcontour_E2Edist(X,Y,D,distbins,e2ebins,stepsize,false,'pdf');
        E2Ehist(kk).Counts=E2Ehist(kk).Counts+temphist.Counts;
        E2Ehist(kk).RelCounts=E2Ehist(kk).RelCounts+temphist.RelCounts;
        E2Ehist(kk).N_analyzed=E2Ehist(kk).N_analyzed+1;
        P2P{jj}=[P2P{jj};tempp2p];
        DC{jj}=[DC{jj};tempdc];
        end
    end
end
end

%%
loghistplots=figure(212);
loghistplots2=figure(211);
for jj=1:nmols
    figure(loghistplots);subplot(2,3,jj);
    s=pcolor(E2Ehist(jj).XC,E2Ehist(jj).RelYC,log10(E2Ehist(jj).RelCounts'));
    s.EdgeColor='none';
    hold all;plot(E2Ehist(jj).XC,45./E2Ehist(jj).XC,'red','LineWidth',2);ylim([0 1]);
    plot(0.34*searchindex{jj,3}*ones(size(E2Ehist(jj).XC)),E2Ehist(jj).RelYC,'green','LineWidth',2);hold off
    xlabel('contour length(nm)');ylabel('End to End length/contour length');
    set(gca,'Clim',[-4 0.5]);
    colorbar
    title(searchindex{jj,2})
    figure(loghistplots2);subplot(2,3,jj);
    s=pcolor(E2Ehist(jj).XC,E2Ehist(jj).YC,log10(E2Ehist(jj).Counts'));
    xlabel('contour length(nm)');ylabel('End to End length');
    s.EdgeColor='none';
    set(gca,'Clim',[-4 1]);
    colorbar
    title(searchindex{jj,2})
    figure(213);semilogy(E2Ehist(jj).RelYC,mean(E2Ehist(jj).RelCounts(16:26,:)));hold all;
end
figure(213);legend(searchindex{:,2});hold off;title(sprintf('probaility density of relative end to end lengths averaged between %.0f and %.0f nm contour length',distbins([20,30])))
%%
SimE2E=DNAcontour_E2Edist([],[],[],distbins,e2ebins,stepsize,false,'pdf');
figure(219);
s=pcolor(SimE2E.XC,SimE2E.RelYC,log10(SimE2E.RelCounts'));
    s.EdgeColor='none';
%% making images with the snakes
makeimages=false;
if makeimages
load('colormaps.mat')
for jj=1:nfiles
    Ldata=load(fullfile(filelist(jj).folder,filelist(jj).name),'ClickPoints','SK','HasClick','picsmooth','DNAOnly','ypix','SubIms','meta');
    disp(filelist(jj).name)
    sfig(jj)=figure(100+jj); clf;
    DNAcontour_plotim(Ldata.picsmooth);
    colormap(gold1)
    caxis([-0.5e-9 4e-9]);
    hold all;
    if Ldata.DNAOnly
        i1=1;i2=2;
    else
        i1=2;i2=3;
    end
    for ii=1:length(Ldata.SK)
        if Ldata.HasClick(ii) && ~isempty(Ldata.ClickPoints(ii).Path)
            xp=Ldata.ClickPoints(ii).Points(i1:i2,1)+Ldata.SK(ii).Xoff;
            yp=Ldata.ClickPoints(ii).Points(i1:i2,2)+Ldata.SK(ii).Yoff;
            scatter(xp,yp,'blue','LineWidth',2);
            plot(Ldata.ClickPoints(ii).Snake.SnakeX+Ldata.SK(ii).Xoff,Ldata.ClickPoints(ii).Snake.SnakeY+Ldata.SK(ii).Yoff,'-r','LineWidth',1);
        end
    end
    figure(100+jj);  hold off
end
%% annotation
for jj=1:nfiles
    ascale=load(fullfile(filelist(jj).folder,filelist(jj).name),'PixToBP','xpix','ypix');ascale.nm=0.34*ascale.PixToBP;
    %sfig(jj)=figure(100+jj);
    sfig(jj).Position=[2000,-180,1250,1300];
    title(searchindex{molindex(jj),2},filelist(jj).name,'Interpreter','none')
    legend({'array boundaries','array contour'},'Location','northeastoutside')
    ax=sfig(jj).CurrentAxes;
    xsr=1e3*round(ascale.nm*ascale.xpix/1e3);ysr=1e3*round(ascale.nm*ascale.ypix/1e3);
    ax.XTick=linspace(0,xsr/ascale.nm,5);ax.YTick=linspace(0,ysr/ascale.nm,5);
    ax.XTickLabel=compose('%.0f',linspace(0,xsr,5));
    ax.YTickLabel=compose('%.0f',linspace(0,ysr,5));
    saveas(sfig(jj),sprintf('%d.eps',jj),'epsc')
end

%%
im=figure(897);
for jj=1:nfiles
    Ldata=load(fullfile(filelist(jj).folder,filelist(jj).name),'ClickPoints','HasClick','DNAOnly','ypix','SubIms','meta');
    if Ldata.DNAOnly
            i1=1;i2=2;
    else
            i1=2;i2=3;
    end
    for ii=1:length(Ldata.SubIms)        
        if Ldata.HasClick(ii) && ~isempty(Ldata.ClickPoints(ii).Path)
            set(groot,'CurrentFigure',im);clf;
            DNAcontour_plotim(Ldata.SubIms(ii).pic);
            colormap(gold1)
            caxis([-0.5e-9 4e-9*(1-0.5*Ldata.DNAOnly)]);
            hold('all');
            xp=Ldata.ClickPoints(ii).Points(i1:i2,1);
            yp=Ldata.ClickPoints(ii).Points(i1:i2,2);
            scatter(xp,yp,'blue','LineWidth',2);
            plot(Ldata.ClickPoints(ii).Snake.SnakeX,Ldata.ClickPoints(ii).Snake.SnakeY,'-r','LineWidth',2);
            hold('off')
            title(searchindex{molindex(jj),2},[filelist(jj).name, sprintf(' nr %.0f',ii)],'Interpreter','none')
            axis('off')
            saveas(im,sprintf('%.0f_%.0f.eps',jj,ii),'epsc')
        end
    end
end
end
