% syms('t','s')
% eqn = 2*s*(1-s*(1-exp(-1/s)))-t^2 == 0
% assume(s>0);assume(t<=1)
% solu=solve(eqn,s,'Real',true,'IgnoreAnalyticConstraints',true);

S=0:0.01:2;
T=sqrt(2*S.*(1-S.*(1-exp(-1./S))));
figure(23);loglog(T,S);xlabel('P/L'); ylabel('E2E/L')
