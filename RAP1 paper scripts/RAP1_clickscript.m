% RAP1 clickscript
% Takes the output of DNAcontour run and throws individual molecules on the
% screen. You then click 4 points: startpoint, array start point, array end
% point, endpoint
% Clicked positions are stored in a struct

DNAOnly=false;
clickfig=figure;
load('colormaps.mat')
ClickPoints=repmat(struct('Points',[0 0 0 0],'Distances',[]),length(SubIms),1);
validmols=[];
for ii=1:length(SubIms)
    try
        figure(clickfig);clf;
        DNAcontour_plotim(SubIms(ii).pic);colormap(gold1);hold('all');set(gca,'Clim',[-0.1e-9 4e-9])
        but=1;ClickedX=[];ClickedY=[];
    catch % if the figure has been closed, exit the loop
     fprintf(1,'loop exited before image nr %i \n',ii)
     return
    end
     while but==1 || but==2
            try
                [cx,cy,but]=ginput(1);
            catch %if you close the figure this gives an error, but we just want it to continue
                but=pi;
            end
                
            if but==1
                plot(cx,cy,'ro');
                ClickedX=[ClickedX cx];
                ClickedY=[ClickedY cy];    
            elseif but==2
                 figure(clickfig);hold ('off');clf; %reset all
                 DNAcontour_plotim(SubIms(ii).pic);colormap(gold1);hold('all');set(gca,'Clim',[-0.1e-9 4e-9])
                 ClickedX=[];ClickedY=[];
            else
                but=pi; % using the enter button gives an empty matrix, which gives an error, so we use pi as a placeholder
            end
     end
     hold ('off')
     if ~DNAOnly && numel(ClickedX)==4
        validmols=[validmols ii]
        CP_wrapped=[ClickedX' ClickedY';[ClickedX(1) ClickedY(1)]];
        ClickPoints(ii).Points=CP_wrapped(1:4,:);
        ClickPoints(ii).Distances=sqrt(sum((diff(CP_wrapped,1)).^2,2));
     elseif DNAOnly && numel(ClickedX)==2
        validmols=[validmols ii]
        CP_wrapped=[ClickedX' ClickedY';[ClickedX(1) ClickedY(1)]];
        ClickPoints(ii).Points=CP_wrapped(1:2,:);
        ClickPoints(ii).Distances=sqrt(sum((diff(CP_wrapped,1)).^2,2));
     end             

end
close(clickfig)


%%
figure(36); clf;
load(fullfile(codefolder,'colormaps.mat'))
%DNAcontour_plotim(picsmooth,fliplr(vertcat(Blobregions([Blobregions.onDNA]).WeightedCentroid)));
DNAcontour_plotim(picsmooth);
colormap(gold1)
caxis([-0.5e-9 4e-9]);
hold all;
if DNAOnly
    i1=1;i2=2;
else
    i1=2;i2=3;
end

for ii=1:length(SK)
    HasClick(ii)=(size(ClickPoints(ii).Points,1)>1) && (~any(ClickPoints(ii).Points(2,:))==0) ;
    if HasClick(ii) &&~isempty(SK(ii).Path)
     %frpintf(1,'%i ',ii)
     xp=ClickPoints(ii).Points(i1:i2,1)+SK(ii).Xoff;
     yp=ClickPoints(ii).Points(i1:i2,2)+SK(ii).Yoff;
     scatter(xp,yp,'red');
     %ClickPoints(ii).Path=GetClickedPath(SK(ii),ClickPoints(ii),[2,3]);
     [StartPoint,EndPoint] = MatchClick2Path(ClickPoints(ii),[i1,i2],SK(ii));
     ClickPoints(ii).Path=SK(ii).Path(sort(StartPoint:EndPoint));
     [xp,yp]=DNAcontour_sub2fullmap(SK(ii).Path,SK(ii).Xoff,SK(ii).Yoff,size(SubIms(ii).sk_im,1),ypix);
     plot(xp,yp,'-g','LineWidth',1);
     ClickPoints(ii).Snake=SubPixDNA(ClickPoints(ii),SubIms(ii).pic.*SubIms(ii).bin_im,meta,1);
     plot(ClickPoints(ii).Snake.SnakeX+SK(ii).Xoff,ClickPoints(ii).Snake.SnakeY+SK(ii).Yoff,'-r','LineWidth',1);
   %  [xp,yp]=DNAcontour_sub2fullmap(SK2(ii).Path,SK(ii).Xoff,SK(ii).Yoff,size(SubIms(ii).sk_im,1),ypix);
  %   plot(xp,yp,'-r','LineWidth',1);
    end
end

%%
%%
CL221=[194 302 106 602]';
CL222=[199 337 106 642]';
CL223=[204 372 106 682]';
CL8=[103 477 218 798]';
CL16=[103 274 189 566]'; %contour length in basepairs
CL=CL8;
AllSnakes=[ClickPoints(validmols).Snake];
AllDist=[ClickPoints(validmols).Distances];
Clengths=0.34*PixToBP*[AllSnakes.SnakeLength]; %distance in nm
nbins=50;
if ~ishghandle(histfig), histfig=figure;end
figure(histfig);clf; 
title('end to end length histograms (nm)')
if ~DNAOnly
subplot(2,2,1);L1hist=histogram(AllDist(1,:),'NumBins',nbins); title('end 1')
subplot(2,2,2);L2hist=histogram(AllDist(2,:),'NumBins',nbins); title('binding region')
subplot(2,2,3);L3hist=histogram(AllDist(3,:),'NumBins',nbins); title('end 2')
%subplot(2,2,4);L4hist=histogram(AllDist(4,:),'NumBins',nbins); title('total')
subplot(2,2,4);CLhist=histogram(Clengths,'NumBins',nbins); title('contour length (measured)')
    line(gca,[0.34*CL(4),0.34*CL(4)],[0,max(CLhist.Values)],'Color','red')
else
    subplot(2,2,1);L1hist=histogram(AllDist(1,:),'NumBins',nbins); title('end to end')
    subplot(2,2,2);CLhist=histogram(Clengths,'NumBins',nbins); title('contour length (measured)')
    line(gca,[0.34*CL(4),0.34*CL(4)],[0,max(CLhist.Values)],'Color','red')
end

%%
%E2E=mean(AllDist,2)/0.34; %end to end in basepairs
S=0:0.01:60; % S=2*persistence length/contour length
T=sqrt((2/3)*S.*(1-S.*(1-exp(-1./S)))); %2/3 is ideal projection, 2 is ideal 2D
figure(25);loglog(T,S);xlabel('E2E/L'); ylabel('2P/L'); hold('on');
PersLens=0.5*CL(4).*interp1(T,S,E2E./CL(4)); % There is no explicit solution to the Rivetti equation so we interpolate
for ii=1:4
    scatter(E2E./CL(4),2*PersLens./CL(4))
end
hold off


%% gather all the data
R16G6_DNA_dist=load('2023018_BBB16_AKTA_SpeedVac_dial_s2_PLL.0_00001.gwy.mat','AllDist');R16G6_DNA_dist=R16G6_DNA_dist.AllDist;
R16G6dist=load('20230222_BB16_PLL_10x.0_00000.gwy.mat','AllDist');R16G6dist=R16G6dist.AllDist;
R16G10dist=load('DNA221_Rap1_3minPLL_90s.0_00001.spm.gwy.mat','AllDist');R16G10dist=R16G10dist.AllDist;
R16G15dist=load('DNA222a-Rap1_3minPLL_1min.0_00002.gwy.mat','AllDist');R16G15dist=R16G15dist.AllDist;
R16G20dist0=load('DNA223_Rap1_3minPLL_12min.0_00000.gwy.mat','AllDist');
R16G20dist1=load('DNA223_Rap1_3minPLL_12min.0_00001.gwy.mat','AllDist');R16G20dist=[R16G20dist0.AllDist,R16G20dist1.AllDist];
R16G20s2dist0=load("06051509.0_00001_DNA223_RAP1.spm.gwy.mat", 'AllDist');
R16G20s2dist1=load("06051549.0_00001.spm_DNA223_RAP1.gwy.mat", 'AllDist');R16G20s2dist=[R16G20s2dist0.AllDist,R16G20s2dist1.AllDist];
R16G35dist=load('20230222_BB8_PLL_10x.0_00002.gwy.mat','AllDist');R16G35dist=R16G35dist.AllDist;
distnmax=max([length(R16G6_DNA_dist),length(R16G6dist),length(R16G10dist),length(R16G15dist),length(R16G20dist),length(R16G35dist)]);
%%
figure(3);clf;
L2hist16G6=histogram(R16G6dist(2,:)./(0.34*CL16(2)),'NumBins',20); hold('on');
L2hist16G10=histogram(R16G10dist(2,:)./(0.34*CL221(2)),'NumBins',20);
L2hist16G15=histogram(R16G15dist(2,:)./(0.34*CL222(2)),'NumBins',20); 
L2hist16G20=histogram(R16G20dist(2,:)./(0.34*CL223(2)),'NumBins',40); 
L2hist16G35=histogram(R16G35dist(2,:)./(0.34*CL8(2)),'NumBins',20); 
hold ('off'); title('binding region'),xlabel('E2E/CL');legend('6space', '10space','15space','20space','35space')
figure(4);clf
L4hist16G6_DNA=histogram(R16G6_DNA_dist(1,:)./(0.34*CL16(4)),'NumBins',30); hold('on');
L4hist16G6=histogram(R16G6dist(4,:)./(0.34*CL16(4)),'NumBins',20); 
L4hist16G10=histogram(R16G10dist(4,:)./(0.34*CL221(4)),'NumBins',20);
L4hist16G15=histogram(R16G15dist(4,:)./(0.34*CL222(4)),'NumBins',20); 
L4hist16G20=histogram(R16G20dist(4,:)./(0.34*CL223(4)),'NumBins',20); 
L4hist16G35=histogram(R16G35dist(4,:)./(0.34*CL8(4)),'NumBins',20); 
title('total'),xlabel('E2E/CL');hold ('off');legend('6spaceDNAOnly','6space', '10space', '15space','20space', '35space')
figure(5);clf;
L2hist16G6n=histogram(R16G6dist(2,:),'NumBins',20); hold('on');
L2hist16G10n=histogram(R16G10dist(2,:),'NumBins',20);
L2hist16G15n=histogram(R16G15dist(2,:),'NumBins',20); 
L2hist16G20n=histogram(R16G20dist(2,:),'NumBins',20); 
L2hist16G35n=histogram(R16G35dist(2,:),'NumBins',20); 
hold ('off');title('binding region'),xlabel('E2E(nm)');legend('6space', '10space', '15space','20space','35space')
figure(6);
L4hist16G6_DNAn=histogram(R16G6_DNA_dist(1,:),'NumBins',30); hold('on');
L4hist16G6n=histogram(R16G6dist(4,:),'NumBins',20);
L4hist16G10n=histogram(R16G10dist(4,:),'NumBins',20);
L4hist16G15n=histogram(R16G15dist(4,:),'NumBins',20);
L4hist16G20n=histogram(R16G20dist(4,:),'NumBins',20);
L4hist16G35n=histogram(R16G35dist(4,:),'NumBins',20); 
hold ('off');title('total'),xlabel('E2E(nm)');legend('6spaceDNAOnly','6space', '10space', '15space','20space', '35space')
title('total'),xlabel('E2E(nm)');
%%
figure(555);clf;
subplot(4,1,1);L4hist16G15n=histogram(R16G15dist(2,:),'NumBins',20); ax(1)=gca;title('binding region');
line(0.34*[CL222(2),CL222(2)],[0,10],'Color','r')
legend('15space')
subplot(4,1,2);
L2hist16G20n=histogram(R16G20dist(2,:),'NumBins',45); 
line(0.34*[CL223(2),CL223(2)],[0,10],'Color','r')
hold('off');ax(2)=gca;legend('20space old')
subplot(4,1,3);L2hist16G20s2n=histogram(R16G20s2dist(2,:),'NumBins',15); ax(3)=gca;
line(0.34*[CL223(2),CL223(2)],[0,10],'Color','r')
legend('20space new')
subplot(4,1,4);L4hist16G35n=histogram(R16G35dist(4,:),'NumBins',20);ax(4)=gca;
legend('35space');xlabel('E2E(nm)')
line(0.34*[CL8(2),CL8(2)],[0,10],'Color','r')
linkaxes(ax,'x');

