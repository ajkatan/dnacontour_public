PixToLU=0.34*PixToBP;
dv=[2 5 10 20 30]/PixToLU;
bv=linspace(0,pi,40);
doplots=9;
for ii=1:size(SK)
    SPS(ii)=SubPixDNA(SK(ii),SubIms(ii).pic,meta,false);
end
[AngleStruct]=PersistenceLengthFromAngles(SPS,PixToLU,dv,bv,doplots);
%%
if DNAOnly
    i1=1;i2=2;
else
    i1=2;i2=3;
end
validmols2=[];
for ii=validmols
    [StartPoint,EndPoint,d_s,d_e] = MatchClick2Path(ClickPoints(ii),[i1,i2],SK(ii));
        if ~isempty(d_s) && ~isempty(d_e) && d_s<3 && d_e<3 %if not too far off the path
            ClickPoints(ii).Path=SK(ii).Path(sort(StartPoint:EndPoint));
            ClickPoints(ii).Snake=SubPixDNA(ClickPoints(ii),SubIms(ii).pic,meta,false);
            validmols2=[validmols2 ii];
        else
            %nothing, it means the path is not right with these clickpoints
        end
end
doplots=10;
[AngleStructC]=PersistenceLengthFromAngles([ClickPoints(validmols2).Snake],PixToLU,dv,bv,doplots);
