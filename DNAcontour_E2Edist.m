function [E2Ehist,P2P,DC] = DNAcontour_E2Edist(X,Y,D,distbins,e2ebins,stepsize,doplots,normalization)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
if nargin<8||isempty(normalization)
    normalization='count';
end

if nargin<6|| isempty(stepsize),  stepsize=ceil(numel(X)/1e4); %max 10 K length into the subfunction
end

if nargin<4|| isempty(distbins),  distbins=5*[1:100]'; % bin edges in nm
elseif isrow(distbins), distbins=distbins';
end
if nargin<5|| isempty(e2ebins),  e2ebins=linspace(0,max(distbins),100)'; % bin edges in nm
elseif isrow(e2ebins), e2ebins=e2ebins';
end

if  nargin==0|| isempty(X) %test mode
    if nargin<7||isempty(doplots) 
        doplots=597; %random plot window
    end
    lpbp=140/sqrt(3); %persistence length in basepairs.
    DNAbp=5e5;
    [X,Y,D]=GenerateSimDNA(DNAbp,lpbp);
    %sim data is long so we cut it up in pieces
    maxlen=1e4;stepsize=5; nlens=floor(DNAbp/maxlen);
    distbins=(0.34*stepsize)*round(distbins/(0.34*stepsize)); %prevent missing slots because it is not an integer nr of basepairs
    [P2P0,DC0]=DNAcontour_E2E(X(1:maxlen),Y(1:maxlen),D(1:maxlen),stepsize); nperlen=numel(P2P0); %test run to see how long it is
    P2P=0*(1:nlens*nperlen);DC=P2P;
    P2P(1:(nperlen))=P2P0;  DC(1:(nperlen))=DC0;
    for ii=1:(nlens-1)
        irange=(nperlen*ii+1):(nperlen*(ii+1));
        jrange=(ii*maxlen+1):((ii+1)*maxlen);
        [P2P(irange),DC0(irange)]=DNAcontour_E2E(X(jrange),Y(jrange),D(jrange),stepsize);
    end

    if doplots
        figure(doplots);subplot(1,2,1);plot(X,Y,'LineWidth',2);
        plotrange=1:25:length(DC);
        subplot(1,2,2);scatter(DC(plotrange),P2P(plotrange)./DC(plotrange));grid on;%hold all;plot(DC/l_p,DC/l_p);hold off;
    end
    elseif numel(X)==1%if called with a nonsense input to generate empty outputs of the right form
    P2P=[];DC=[];
else%if called with data
    [P2P,DC]=DNAcontour_E2E(X,Y,D,stepsize);
end
if nargin<7||isempty(doplots) 
    doplots=false;
end
rely=linspace(0,1,numel(e2ebins));
P2P=P2P(DC~=0);DC=DC(DC~=0); %weed out the zeros
hc=histcounts2(DC,P2P,distbins,e2ebins,'Normalization',normalization);
hcrel=histcounts2(DC,P2P./DC,distbins,rely,'Normalization',normalization);
E2Ehist.Counts=hc;E2Ehist.XE=distbins;E2Ehist.YE=e2ebins;
E2Ehist.RelCounts=hcrel;E2Ehist.RelYE=rely;
E2Ehist.XC=distbins(2:end)-0.5*diff(distbins);
E2Ehist.YC=e2ebins(2:end)-diff(e2ebins);
E2Ehist.RelYC=rely(2:end)-0.5*diff(rely);
if doplots
    figure(doplots+1);
    surf(E2Ehist.XC,E2Ehist.YC,E2Ehist.Counts');
    xlabel('contour length(nm)');ylabel('End to End length');
    figure(doplots+2);
    surf(E2Ehist.XC,E2Ehist.RelYC,E2Ehist.RelCounts');
    xlabel('contour length(nm)');ylabel('End to End length/Contour length');
end
end
function [X,Y,D]=GenerateSimDNA(DNAbp,lpbp)
    bpl=0.34; %basepair length in nm        
    l_p=lpbp*bpl;fprintf('persistence length %.1d nm\n',l_p);
    DNAstartposX=randn;DNAstartposX=repmat(DNAstartposX,DNAbp,1);
    DNAstartposY=randn;DNAstartposY=repmat(DNAstartposY,DNAbp,1);
    DNAstartangle=2*pi*rand(1,1);
    DNAstartangle=repmat(DNAstartangle,DNAbp,1); %random startangles
    DNAangle_d=randn(DNAbp,1)/sqrt(lpbp); %worm like chain for each segment
    DNAangle=DNAstartangle+cumsum(DNAangle_d,1); %sum up to get the angle
    DNA_dx=bpl*cos(DNAangle);DNA_dy=bpl*sin(DNAangle); %differential xy, in nm
    X=cumsum(DNA_dx,1)+DNAstartposX;Y=cumsum(DNA_dy,1)+DNAstartposY; %xy positions by integrating differentials
    D=bpl*(1:DNAbp);
end