function masked=DNAcontour_SetHeightMask_particles(im,htop,hbottom,htoo,gracepixels);
%thresholding function
%sets a height mask on an AFM image between two values
%masked pixels will be higher than the lower threshold (hbottom),
%and belong to a region which has a maximum value higher than the high
%threshold (htop).
%So if a region has areas nowhere higher than htop, it will be excluded.
%This allows including perimeters of high objects without including low objects
%The htoo threshold prevents perimeters of too large objects being counted.
%A mechanism is built in to allow areas with only few pixels (gracepixels)
%higher than the high threshold to not be excluded.

%
if nargin<4
    gracepixels=0; %max size of too high parts we will still count
end

Test_Mode=0;

if nargin==0
    Test_Mode=1;
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    tim1=getfield(load(fullfile(thisfolder,'testims.mat')),'tim1');
    im=tim1.data;
    hbottom=3e-10;
    htop=1.5e-9;
    htoo=3e-9;
end

notlow=im>hbottom;
notlowstruct=bwconncomp(notlow);
maxes=regionprops(notlowstruct,im,'MaxIntensity'); %N_notlowregions x 1 struct array of max values
toohighinds = find([maxes.MaxIntensity] > htoo); %label indices of regions that would be discarded on the basis of too high pixels
toolowinds =  find([maxes.MaxIntensity] < htop);
graceflag=false(size(toohighinds)); %preallocate in case toohighinds is empty

if gracepixels>0 %find regions with only a few pixels too high
    toohigh=im>htoo;
    toohighstruct=bwconncomp(toohigh);
    areas=regionprops(toohighstruct,'Area'); %N_highregions x 1 struct array of areas
    graceinds=find([areas.Area]<gracepixels); %label indices of regions that are too small to discard
    grace=ismember(labelmatrix(toohighstruct),graceinds); %boolean matrix size of image, 1 for small regions in 'too high' boolean
    gracelin=find(grace); %linear indices of points to include
    for ii=1:length(toohighinds)
        inds=notlowstruct.PixelIdxList{toohighinds(ii)}; %all the linear indices of pixels in the regions to be tested
        graceflag(ii)=any(ismember(gracelin,inds)); %sets
    end
end

belongstoohigh = ismember(labelmatrix(notlowstruct), toohighinds(~graceflag));
belongstoolow =  ismember(labelmatrix(notlowstruct), toolowinds);
masked=notlow & ~belongstoohigh & ~belongstoolow;

if Test_Mode
    figure(333)
    subplot(2,2,1);pcolor(im+0);shading flat;title('original');caxis([hbottom htoo]);
    subplot(2,2,2);pcolor(notlow+0);shading flat;title('not too low');
    subplot(2,2,3);pcolor(belongstoohigh+0);shading flat;title('too high');
    subplot(2,2,4);pcolor(masked+0);shading flat;title('final mask');
    disp('end of test')
end