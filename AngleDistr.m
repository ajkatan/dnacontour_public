function [f,bincen] = AngleDistr(Angs,Dists,distvec,binvec,doplots)
%Takes angle as a function of distance,
%interpolates to get fixed distances and takes correlation at these
%distances
if nargin<5
    doplots=0;
elseif isempty(doplots)
    doplots=1;
end
if nargin<3|| isempty(distvec),  distvec=bpl*[2 5 10 20 50]; end
if nargin<4|| isempty(binvec),  binvec=linspace(0,2*pi,180);end

if  nargin==0|| isempty(Angs) %test mode
    bpl=0.34; %basepair length in nm
    DNAbp=1e6; DNAnm=bpl*DNAbp; %length in basepairs and nm
    lpbp=150; %persistence length in basepairs.
    l_p=lpbp*bpl;fprintf('persistence length %.1d nm\n',l_p);
    DNAstartposX=randn;DNAstartposX=repmat(DNAstartposX,DNAbp,1);
    DNAstartposY=randn;DNAstartposY=repmat(DNAstartposY,DNAbp,1);
    DNAstartangle=2*pi*rand(1,1);
    DNAstartangle=repmat(DNAstartangle,DNAbp,1); %random startangles
    DNAangle_d=randn(DNAbp,1)/sqrt(lpbp); %worm like chain for each segment
    %DNAangle_d(1:2000:end)=pi/2;
    DNAangle=DNAstartangle+cumsum(DNAangle_d,1); %sum up to get the angle
    DNA_dx=bpl*cos(DNAangle);DNA_dy=bpl*sin(DNAangle); %differential xy, in nm
    DNAx=cumsum(DNA_dx,1)+DNAstartposX;DNAy=cumsum(DNA_dy,1)+DNAstartposY; %xy positions by integrating differentials
    [Angs,~,Dists]=AngAndCurv(DNAx,DNAy);
    if doplots

        figure(212);subplot(1,2,1);plot(DNAx,DNAy,'LineWidth',2);
        subplot(1,2,2);plot(Dists(2:end),Angs,'LineWidth',2);grid on
    end

end
if ~isrow(binvec),binvec=binvec';end %force to row vector
bincen=0.5*(binvec(2:end)+binvec(1:(end-1))); % bin centers
maxN=length(distvec);
Lc=max(Dists);np0=length(Dists);
ivecd=(Lc/np0).*(1:(np0-1))';% interpolating points at equal distance
IntAngs=interp1(Dists(2:end),Angs,ivecd,'nearest'); %angles at these points
f=zeros(maxN,length(bincen));
for ii=1:maxN
    first2do=find(Dists>(Dists(1)+distvec(ii)),1,'first');
    dIntAngs=DiffAngle(IntAngs(first2do:end),IntAngs(1:(end-first2do+1)));%angle difference, mapped to -pi-pi
    %f(ii,:)=histcounts(abs(dIntAngs(dIntAngs~=0)),binvec);
    f(ii,:)=histcounts(abs(dIntAngs),binvec);
end
if doplots  %test mode
    figure(213);
    for ii=1:maxN
        plot(180*bincen/pi,-log10(f(ii,:)/np0));hold all
    end
    hold off
end