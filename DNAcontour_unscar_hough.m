function [descarred,scarpic]=DNAcontour_unscar_hough(pic,meta,doplot)
%Removes 'scars' from an AFM image. Scars are single full or partial lines that are much
%higher than their surrounding. Does not work for multi-line artifacts.
%Method: Applies a unidirectional single line enhancing filter to the image, 
%Then applies a threshold to obtain a binary image
%In this binary image, lines are selected by taking peaks in the Hough transform
%Gaps in the lines are filled up and a 'scar' image is formed from the
%lines
%The corrected image is computed by subtracting the line enhanced image
%from the original in places where a scar is detected.
%
%Jacob Kerssemakers & Allard Katan 2014

nscars=50; %max number of scars identified. Should this be an input variable?
HPmax=0.01; %Limit value (relative to maximum) in the Hough transform to consider it a line. Should this be an input variable?
Ngapfill=8; %Nr of pixels between lines that are filled up. Should this be an input variable?

if nargin<3|| isempty(doplot)
 doplot=0; %do the plots or not
end
if nargin<1 || isempty (pic) %test or debug
    doplot=1;
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    tim1=getfield(load(fullfile(thisfolder,'testims.mat')),'tim1');
    pic=tim1.data;
end

if nargin<2|| ~isfield(meta,'cleaner')
    meta.cleaner.filtsize=7;
    meta.cleaner.linethreshold=1;
    meta.cleaner.minlinelength=20;
    meta.cleaner.direction='x';
end
%shorten the names for readability
filtsize=meta.cleaner.filtsize; %nr of lines over which the line enhancing filter averages. More lines makes it more sensitive but also more prone to errors due to multiple lines interfering
Sigmas=meta.cleaner.linethreshold; %nr of standard deviations above the median of the line enhanced image to consider it a line
minsize=meta.cleaner.minlinelength; %minimum line length
direc=meta.cleaner.direction; %direction (horizontal or vertical)

filtshift=floor(filtsize/2); %shift induced by filter
filtsize=1+2*filtshift; %forces odd size

h=-ones(1,filtsize)/(filtsize-1);
h(ceil(filtsize/2))=1; % creates a normalized filter with coefficient 1 in the center and equal negative coefficient elsewhere.

switch direc
    case 'x' %horizontal scars
        pic2filt=zeros(size(pic,1),size(pic,2)+filtshift);
        pic2filt(:,1:(end-filtshift))=pic;
        scarpic=(filter(h,1,pic2filt'))'; %filter enhances single pixel steps up and down
        scarpic=scarpic(:,(1+filtshift):end);
        thetas=-1:0.1:1;
    case 'y'  %vertical scars
        pic2filt=zeros(size(pic,1)+filtshift,size(pic,2));
        pic2filt(1:(end-filtshift),:)=pic;
        scarpic=filter(h,1,pic2filt); %filter enhances single pixel steps up and down
        scarpic=scarpic((1+filtshift):end,:);
        thetas=[-89.9:0.1:-89 89:0.1:90];
end

scarbool=scarpic>median(scarpic(:))+Sigmas*std(scarpic(:)); % threshold the scar image
[H,theta,rho] = hough(scarbool,'Theta',thetas); %
Hthresh=ceil(HPmax*max(H(:))); 
P = houghpeaks(H,nscars,'threshold',Hthresh);
lines = houghlines(scarbool,theta,rho,P,'FillGap',Ngapfill,'MinLength',minsize);
scarbool_final=false(size(scarbool));
for ii = 1:length(lines)
   %Hough works 90 degrees rotated!
   %lines(n).point1=[starty,startx],lines(n).point2=[endy,endx]
   yx = [lines(ii).point1; lines(ii).point2]; 
   %select the area in the image that corresponds to this line
   scarbool_final(yx(1,2):yx(2,2),yx(1,1):yx(2,1))=true;
   %in principle the lines can be slightly tilted. 
end
hold off

descarred=pic-scarbool_final.*scarpic;

if doplot
figure(101);
ax1=subplot(1,2,1);DNAcontour_plotim(scarbool); title('threshold');
ax2=subplot(1,2,2);DNAcontour_plotim(scarbool_final);title('hough');
linkaxes([ax1 ax2],'xy');

figure(102);
ax1=subplot(2,1,1);DNAcontour_plotim(pic);
DNAcontour_ImageAutoScale(pic);
ax2=subplot(2,1,2);DNAcontour_plotim(descarred);
DNAcontour_ImageAutoScale(pic);
linkaxes([ax1 ax2],'xy')
end