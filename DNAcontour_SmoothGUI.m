function [picsmooth,sigma,winsize,nreps]=DNAcontour_SmoothGUI(pic);
%DNAcontour_SmoothGUI Smooths an image with visual feedback and user input
%   Detailed explanation goes here
if nargin<1
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    tim2=getfield(load(fullfile(thisfolder,'testims.mat')),'tim2');
    pic=tim2.data;
end
sigma=0.5;
winsize=5;
nreps=7;

Smfig=figure;
stop_it=0;
Woptions.WindowStyle='normal';
CurrentZoomX=[0 size(pic,1)];CurrentZoomY=[0 size(pic,2)];
CurrentHeight=[min(pic(:)),max(pic(:))];
while ~stop_it
    picsmooth=DNAcontour_GaussSmoothImage(pic,sigma,winsize,nreps);
    figure(Smfig);
    subplot(1,2,2);DNAcontour_plotim(pic); ImAx1=gca;title('Original');caxis(CurrentHeight);
    subplot(1,2,1);DNAcontour_plotim(picsmooth); ImAx2=gca;title('Smoothed');caxis(CurrentHeight);
    linkaxes([ImAx1,ImAx2],'xy');set(ImAx1,'xlim',CurrentZoomX);set(ImAx1,'ylim',CurrentZoomY);
    answer = inputdlg_tt({'Sigma (pixels)','Window size (pixels)','Nr of repetitions','Color min','Color max'},'Set parameters',1,{num2str(sigma),num2str(winsize),num2str(nreps),num2str(CurrentHeight(1)),num2str(CurrentHeight(2))},Woptions);
    if isempty(answer) % The 'done' button, aka 'cancel', gives an empty answer
        stop_it=1;
    else
        sigma=sscanf(answer{1},'%g');winsize=ceil(sscanf(answer{2},'%i'));nreps=ceil(sscanf(answer{3},'%i'));CurrentHeight=[sscanf(answer{4},'%g'),sscanf(answer{5},'%g')];
    end
    if ishghandle(Smfig)
        CurrentZoomX=get(ImAx1,'xlim');CurrentZoomY=get(ImAx1,'ylim');
    end
end
picsmooth=DNAcontour_GaussSmoothImage(pic,sigma,winsize,nreps); %repeat to be sure we use the latest values
if ishghandle(Smfig)
    close(Smfig);
end
end

