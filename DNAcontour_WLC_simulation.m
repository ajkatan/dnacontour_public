function [DNAx,DNAy,DNAangle]=DNAcontour_WLC_simulation(l_p,DNAbp,NDNA,p_kinks,kinksharp,bpl)
%simulates a single WLC chain. It is possible to add kinks
%p_kinks=Mean length in bp between sharp kinks. Kinks are random angles
%between kinksharp*(-pi and pi);
if nargin==0||isempty(l_p)
   l_p=50; %persistence length in nm. This is the three-dimensional persistence length!!
end
if nargin<2||isempty(DNAbp)
   DNAbp=3e3; %total chain length in bp
end
if nargin<3
    NDNA=100;
end
if nargin<4 
    p_kinks=0;%3e3;
end
if nargin<5
    kinksharp=0.6;
end
if nargin<6
bpl=0.34; %basepair length in nm, the segment length
end
lpbp=l_p./bpl; %persistence length basepairs.
DNAstartangle=pi*(2*rand(1,NDNA)-1);
DNAstartangle=repmat(DNAstartangle,DNAbp,1); %random startangles
DNAangle_d=randn(DNAbp,NDNA)./sqrt(lpbp); %worm like chain for each segment
if p_kinks~=0
nbp=numel(DNAangle_d); %total nr of angles
%the following ensures there are (can be) kinks even in short molecules
n_kinks1=ceil(nbp/p_kinks); %always >1 even if all molecules together shorter than kink period
n_kinks_helper=rand(n_kinks1,1)>exp(-nbp/p_kinks); %randomizes the existence of kinks. 
n_kinks=sum(n_kinks_helper) %total nr of kinks is stochastic
pos_kinks=ceil(nbp*rand(n_kinks,1)); %kink positions, for linear indexing
kink_angs=kinksharp*pi*(2*rand(n_kinks,1)-1); %random -pi to pi
DNAangle_d(pos_kinks)=kink_angs;
end
DNAangle=DNAstartangle+cumsum(DNAangle_d,1); %sum up to get the angle
DNA_dx=bpl*cos(DNAangle);DNA_dy=bpl*sin(DNAangle); %differential xy
DNAx=cumsum(DNA_dx,1);DNAy=cumsum(DNA_dy,1); %xy positions by integrating differentials
if nargin==0
figure(732);plot(DNAx,DNAy);axis equal;
end