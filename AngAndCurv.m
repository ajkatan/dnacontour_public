function [Tang,TD,D,L]=AngAndCurv(x,y)
%calculates the tangent and angle differential of sequential points (x-y
%pairs)
%'Tang' contains the tangent, which is the direction of the difference
%between the two points
%'TD is the angle between the two tangents of three succesive xy
%pairs, divided by the distance of the outer two
%D is the total distance from the start

%inputs must be columns!
%lengths: TD shorter than Tang shorter than D equal to xy

dx=x(2:end)-x(1:(end-1)); %differential. Is equal to vector 1-> 2 at point 1.
dy=y(2:end)-y(1:(end-1));
R=sqrt(dx.^2+dy.^2);
D=[0;cumsum(R)]; %distance
L=D(end);

Tang=atan2(dx,dy); %the tangent
c1=DiffAngle(Tang(2:end),Tang(1:(end-1)));  %angle differential, mapped to -pi-pi

dx2=x(3:end)-x(1:(end-2)); %distance of 2 points with one skipped
dy2=y(3:end)-y(1:(end-2));
R2=sqrt(dx2.^2+dy2.^2);
TD=zeros(size(c1));
if ~isempty(R2)
    TD(R2>0)=c1(R2>0)./R2(R2>0);
end
end