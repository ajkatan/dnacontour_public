function [hbottom,htop,htoo]=DNAcontour_ThresholdTester(pic,hbottom,htop,htoo)
%gui method of deciding a threshold on an AFM image
%uses DNAcontour_SetHeightMask, see that file for description of output
%variables

if nargin==0
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    tim2=getfield(load(fullfile(thisfolder,'testims.mat')),'tim2');
    pic=tim2.data;
end
thtfig=figure;
if nargin<2 ||isempty(hbottom), hbottom=median(pic(:))+2*std(pic(:)); end %first tries
if nargin<3||isempty(htop), htop=hbottom; end %set it equal to hbottom to disable it for first try
if nargin<4 ||isempty(htoo),htoo=Inf;end

stop_it=0;
Woptions.WindowStyle='normal';
CurrentZoomX=[0 size(pic,1)];CurrentZoomY=[0 size(pic,2)];
while ~stop_it
    htop=max(hbottom,htop); %protect against typos
    masker=DNAcontour_SetHeightMask(pic,hbottom,htop,htoo);
    figure(thtfig)
    subplot(1,2,1);DNAcontour_plotim(pic.*masker); IncAx=gca;
    colormap(hot);title('included');caxis([hbottom-htop htop+hbottom]);
    subplot(1,2,2);DNAcontour_plotim(pic.*~masker);ExcAx=gca;
    title('excluded');caxis([hbottom-htop htop+hbottom]);
    linkaxes([IncAx,ExcAx],'xy');set(IncAx,'xlim',CurrentZoomX);set(IncAx,'ylim',CurrentZoomY);
    answer = inputdlg_tt({'lower','upper','max'},'Set thresholds',1,{num2str(hbottom),num2str(htop),num2str(htoo)},Woptions);
    if isempty(answer) % The 'done' button, aka 'cancel', gives an empty answer
        stop_it=1;
    else
        hbottom=sscanf(answer{1},'%g');htop=sscanf(answer{2},'%g');htoo=sscanf(answer{3},'%g');
        if ishghandle(thtfig)
            CurrentZoomX=get(IncAx,'xlim');CurrentZoomY=get(IncAx,'ylim');
        end
    end
end
if ishandle (thtfig)
    close(thtfig)
end
end

