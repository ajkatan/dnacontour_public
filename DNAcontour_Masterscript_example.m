%Masterscript for DNA contour package

%definition of axes:
%1,1 is the bottom left corner
%N,1 is the bottom right corner
%N,N is the top right corner
%x is the first index
%y is the second index
%this means that pcolor(pic') gives the correct view (note transpose!), and to plot a point
%at x0,y0 from the bottom left you simply use plot(x0,y0).


%% Setting some variables
clear

%some of the functions have, or will have, a gui for optimizing parameters. 
%All have a direct input of parameters as well.
%Typically you use the guis for optimizing, then afterwards you fill in
%those values in the appropriate places and switch off the guis
useguis.file=1;useguis.savefolder=1; %select folders for input and ouptut
%in this example it is assumed the folder 'Test Data' is also copied along
%and the testimage in there is present.
useguis.cleaner=1; %cleaning lines
useguis.smooth=1; %smoothing parameters
useguis.threshold1=1;% DNA/filament threshold
useguis.threshold2=1;% Blob/particle threshold
useguis.area=1; %area filter
%below you can switch off parts of the processing. 1 =on, 0= off
do_bg=1; %do a background subtraction or not
cleanit=0; %perform cleaning of lines
smoothit=1; %perform smoothing
doplots=1; %enable 'debug' output plots of subfunctions
getblobs=0; %find blobs/particles (

figbase=0;thisfig=figbase; %the figures will be plotted in new windows starting at figbase+1. 
%If you run this script twice and want to compare figures, increase the value of figbase so there is no overlap
%% File i/o
%set default folder to this folder
thisfile=mfilename('fullpath');
if isempty(thisfile) %the line above fails if you ctrl-enter this script instead of running it
    thisfile=which('DNAcontour_Masterscript_example');% works only if you did not change the file name
end
codefolder=fileparts(thisfile);
addpath(genpath(codefolder));

%% read in file here
% read meta from earlier run, for some cases during testing
%meta=getfield(load('D:\ajkatan\My Documents\data\SungHyunTweezer\20141111_dna_10mmmg_0p6ng_2min.001.mat','meta'),'meta');

if useguis.file
    [fnn,datafolder]=uigetfile('*.*','MultiSelect','on');
    fnl=fullfile(datafolder,fnn); %file name list
else %fill in file name and path below
    fnl=fullfile(codefolder,'Test data','DI_testfile1.000');
    [datafolder,fnn,fne]=fileparts(fnl);
end

if ~iscell(fnl),fnl={fnl};fnn={fnn}; end %if single file make into cell
if useguis.savefolder
    savefolder = uigetdir(datafolder,'Folder to save processed data');
else
    savefolder=fullfile(datafolder,'processed MAT');if ~exist(savefolder,'dir'),mkdir(savefolder);end
end
%%
for ff=1:length(fnl)
    %% file input
    cd(datafolder)
    fn=fnl{ff};
    [~,~,fne]=fileparts(fn);
    meta.datafilename=fn;
    if strcmpi(fne,'.gwy'),ftype='gwy';
    elseif strcmpi(fne,'.mat'),ftype='mat';
    else ftype='DI';
    end %no catch, just assume it's DI
    meta.ch_nr=1; %read ch1 by default
    switch ftype
        case 'DI'
            [images,~,global_info] = OpenNanoscopeAK(fn);
            picraw=1e-9*images.Height; clear('images'); %this fails if you record height trace and retrace, adjust if necessary.
            if exist('global_info','var');
                meta.ImsizeX=global_info.ScanSize(1); %assuming square pixels. Unit is nm
                meta.ImsizeY=global_info.ScanSize(2);
            else
                meta.Imsize=1;
            end
        case 'gwy'
            ReadChannel = readgwychannel(fn,meta.ch_nr-1); %0-based ch nrs
            picraw=fliplr(ReadChannel.data');
            meta.ImsizeX=ReadChannel.xreal;
            meta.ImsizeY=ReadChannel.yreal;
        case 'mat' %assume simulated image with height in nm
            sims=load(fn);
            picraw=1e-9*sims.simim;
            meta.ImsizeX=1e-9*sims.im_nm;meta.ImsizeY=1e-9*sims.im_nm;
    end
    %% do background subtraction
    if do_bg
        pic=DNAcontour_bgsub(picraw,{'LBLO','4x2','LBLO',},[4,4,1],2,1);
    else
        pic=picraw;
    end
    %we make the pixels square, that makes life easier. y pixel size is
    %made into x pixel size
    [xpix,ypix]=size(pic);
    if ~(xpix/ypix==meta.ImsizeX/meta.ImsizeY) %if not image with square pixels
        pic=imresize(pic,round([xpix xpix*(meta.ImsizeY/meta.ImsizeX)]),'Lanczos2'); %Square the pixels. Lanczos gives better results
    end
    %% clean up the image
    if cleanit
        if useguis.cleaner
            [picclean,meta.cleaner]=DNAcontour_CleanGUI(pic);
        else %manual input
        meta.cleaner.filtsize=7;
        meta.cleaner.linethreshold=6;
        meta.cleaner.minlinelength=20;
        meta.cleaner.direction='x';
        picclean=DNAcontour_unscar_hough(pic,meta,0);
        end
        picclean=DNAcontour_bgsub(picclean,{'LBLO',},1.1,0,1); % stripes affect background
    else
        picclean=pic;
    end
    if smoothit
        if useguis.smooth
            [picsmooth,meta.smooth.sigma,meta.smooth.winsize,meta.smooth.nreps]=DNAcontour_SmoothGUI(picclean);
        else %manual input
            meta.smooth.sigma=0.25;
            meta.smooth.winsize=6;
            meta.smooth.nreps=8;
            picsmooth=DNAcontour_GaussSmoothImage(picclean,meta.smooth.sigma,meta.smooth.winsize,meta.smooth.nreps);
        end
    else
        picsmooth=picclean;
    end
    PixToBP=meta.ImsizeX/(0.34e-9*size(picsmooth,1)); %pixels to basepairs
    %% set thresholds for heigth of DNA
    if useguis.threshold1
        [meta.heightmask.hbot1,meta.heightmask.htop1,meta.heightmask.hmax1]=DNAcontour_ThresholdTester(picsmooth); %for DNA & protein
    else %manual setting
        meta.heightmask.hbot1=1.1e-10;
        meta.heightmask.htop1=2.0e-10;
        meta.heightmask.hmax1=Inf;
    end
    
    %%Make binary image by height treshold
    masked1=DNAcontour_SetHeightMask(picsmooth,meta.heightmask.hbot1,meta.heightmask.htop1,meta.heightmask.hmax1,0); %finds regions higher than hmin, and not containing points higher than hmax
    if doplots
        thisfig=thisfig+1;
        filterfig=thisfig;
        figure(filterfig);
        subplot(1,3,1);DNAcontour_plotim(pic);title('original image');DNAcontour_ImageAutoScale(pic);
        subplot(1,3,2); DNAcontour_plotim(picsmooth);title('smoothed');DNAcontour_ImageAutoScale(pic);
        subplot(1,3,3); DNAcontour_plotim(masked1);title('height mask');
    end
    %% filter DNA binary image by area
    if useguis.area
        [meta.minarea,DNA_bin]=DNAcontour_AreaFilterGUI(masked1);
    else
        meta.minarea_DNA=1000;
        DNA_bin=bwareaopen(masked1,meta.minarea_DNA,8); %filter out the small dots
    end
    
    %% Map the branches and their length
    meta.PadSize=2;
    meta.skel.method='thin';
    meta.skel.clean=0;
    meta.snake.smooth=5;
    meta.snake.Hweight=150;
    meta.snake.stepsize=0.2;
    meta.snake.n_its=500;
    
    if doplots,thisfig=thisfig+1;figure(thisfig);end
    [DNAregions,SK,SubIms,SPS,sk_im,loop_im,path_im]=DNAcontour_Analysis_wrapper(picsmooth,DNA_bin,meta,doplots);
    
    if doplots,thisfig=thisfig+1;figure(thisfig);
        DNAcontour_plotim(loop_im+DNA_bin+sk_im);title('Loops');
        thisfig=thisfig+1; figure(thisfig);
        DNAcontour_plotim(DNA_bin+path_im+sk_im);title('DNAregions,Branches and Paths')
    end;
    
    %% some plots of curvatures etc.
    
    ac=vertcat(SPS(:).SnakeCurv); %all curvature
    [cf,cbin]=hist(log10(PixToBP./abs(ac(ac~=0))),500); %histogram of log(radius in bp)
    if doplots
thisfig=thisfig+1;figure(thisfig);subplot(2,1,1);
    plot(10.^cbin,cf);xlim([-1,1000]);
    title('Curvature distribution');xlabel('radius (bp)');
    aa=vertcat(SPS(:).SnakeAng); %all angles
    aa2=vertcat(SPS(:).Angle);
    [af,abin]=hist(aa,500); %histogram of angles
    [af2,abin2]=hist(aa2,500); %histogram of angles
    subplot(2,1,2);plot(abin,af,abin2,af2);
    title('Angle distribution');xlabel('angle (rad)');
    end
    if PixToBP>1e8,PixToBP=PixToBP/1e9;end %meter/nm correction
    AC=cell(length(SPS),1);
    dv=([15 30 60 120 240]); %distance vector in bp
    bv=pi*logspace(-1.5,0,150);
    AA=zeros(length(dv),length(bv));
    for ss=1:length(SPS);
        AC{ss} = AngleDistr(SPS(ss).SnakeAng,SPS(ss).SnakeD,dv/PixToBP,bv,0);
        AA=AA+AC{ss};
    end
    if doplots
    thisfig=thisfig+1;figure(thisfig);
    binlength=[diff(bv)];binlength=[binlength binlength(end)];scalemat=repmat(1./binlength,length(dv),1);
    semilogy(bv/pi,scalemat.*AA./repmat(sum(scalemat.*AA,2),[1,length(bv)]),'.-','MarkerSize',16);
    xlabel('theta/pi(rad)')
    ylabel('G(theta,L)')
    lstring={};
   for ii=1:length(dv)
    lstring=[lstring,['this file',sprintf(', L=%i bp',dv(ii))]];
   end
legend(lstring,'Location','southwest')
    end
    %% find blobs
    
    if getblobs
        %set threshold
        if useguis.threshold2
            [meta.heightmask.hbot2,meta.heightmask.htop2,meta.heightmask.hmax2]=DNAcontour_ThresholdTester(picsmooth); %for protein only
        else
            meta.heightmask.hbot2=7e-10;
            meta.heightmask.htop2=10e-10;
            meta.heightmask.hmax2=5e-9;
        end
        
        masked2=DNAcontour_SetHeightMask(picsmooth,meta.heightmask.hbot2,meta.heightmask.htop2,meta.heightmask.hmax2,0);
        
        if useguis.area
            [meta.minarea_Blob,Blob_bin]=DNAcontour_AreaFilterGUI(masked2);
        else
            meta.minarea_Blob=9; %minimal area of blobs in pixels
            Blob_bin=bwareaopen(masked2,meta.minarea_Blob,8); %filter out the small dots
        end
        Blob_binstruct=bwconncomp(Blob_bin,8); %makes a structure of connected regions. Points in these regions are found in field 'PixelIdxList'%minimal area in pixels
        labelmat_Blob=labelmatrix(Blob_binstruct); %label all the points in regions with the region nr
        Blobregions=regionprops(Blob_binstruct,picsmooth,'WeightedCentroid');
        WC2=reshape([Blobregions.WeightedCentroid],2,Blob_binstruct.NumObjects)'; %from N struct array of 2x1 to Nx2
        WC2=[WC2(:,2) WC2(:,1)]; % the order is reversed in the builtin function
        
        if doplots
            thisfig=thisfig+1;
            figure(thisfig);
            subplot(1,2,1);DNAcontour_plotim(masked2);title('Blob Mask')
            DNAcontour_plotim(labelmat_Blob);title('Blob labels after area filter');hold all;
            for ii=1:size(WC2,1)
                text(WC2(ii,1),WC2(ii,2),num2str(ii),'Color','w');
            end
        end
        
        %% Match the blob xy positions to positions along the DNA
        %doplots=1;
        totalarea=sum(DNA_bin(:));
        sk_area=sum(sk_im(:));
        meanwidth=totalarea/sk_area;
        on_DNA_thresh=meanwidth;
        if doplots %plot the blob matching or not
            thisfig=thisfig+1;
            figure(thisfig);
        end
        [Blobregions] = DNAcontour_MatchBlobs2DNA(Blobregions,SK,path_im,sk_im,on_DNA_thresh,doplots);
        %% from here on it is about extracting the information
        
        
        PathL=[SK.Length]*PixToBP; %true length
        AreaL=PixToBP*[DNAregions.Area]./meanwidth;
        [N,c]=hist(PathL,30);
        N2=hist(AreaL,c);
        thisfig=thisfig+1;
        figure(thisfig);
        bar(c,[N',N2']);legend('path length','area-based')
        title('DNA length (basepairs)')
        %%
        
        boundperc=100*sum([Blobregions.onDNA])./length(Blobregions);
        perconpath=100*sum([Blobregions.onPath])./sum([Blobregions.onDNA]);
        blobspermol=sum([Blobregions.onDNA])./length(SK);
        fprintf(1,['Nr of DNA molecules: %i \n'  ...
            'Nr of Blobs: %i \n'  ...
            'Blobs on DNA from total (perc): %.1f \n'  ...
            'Blobs on path vs rest of DNA (perc): %.1f \n' ...
            'Nr of blobs per DNA : %.2f \n'],[length(DNAregions),length(Blobregions),boundperc,perconpath,blobspermol]);
        bdi=[Blobregions.onPath]; %indices where the blob is on the path
        bL=[SK([Blobregions(bdi).blob_path]).Length];%length of the paths of the molecules with blobs
        bd=PixToBP*abs([Blobregions(bdi).blob_dist]-bL/2);
        bd=bd./(PixToBP*bL);
        DNAbp=2080;
        %bdwrap=DNAbp-max(bd,DNAbp-bd); %this gives 0 at the ends, DNAbp/2 in the middle and negative nrs for anything >DNAbp
        
        thisfig=thisfig+1;
        figure(thisfig);
        hist(bd,20);title('Blob distance from center (bp)')
    end
    
    %% wrap up
    %% saving code
    
    cd(savefolder);
    savefilename=[fnn{ff} '.mat']
    save(savefilename)
    fclose('all');
end