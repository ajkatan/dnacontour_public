function [AngleStruct]=PersistenceLengthFromAngles(varargin)
%Syntax: [AngleStruct]=PersistenceLengthFromAngles(SPS,PixToLU,dv,bv,doplots)

%from a DNAcontour stucture with snake contour (SPS), the differential
%tangent angle is calculated at all contour length differences defined in dv
%The distribution of these angles is fit to a worm-like chain model for
%each distance
%Input: SPS:        SnakePathStruct from DNAcontour
%       PixToLU:    number of length units in a pixel. The distance vector
%                   is expressed in pixels, the persistence length in these
%                   units
%       dv:         Distances at which to evaluate angles, in pixels
%       bv:         vector with bin edges  (as defined in the 'histcounts'
%                   function) for the angle histogram. There are 1 less
%                   bins than edges. Units are radias
%      doplots:     0 if you don't want a plot, the figure nr if you do.
%Output:
test_mode=false;
if nargin<5 || isempty (varargin{5}), doplots=0; else, doplots=varargin{5}; end

if nargin<4 || isempty (varargin{4})
    bv=[0,pi*logspace(-2,0,31)]; %bin edges. Last edge should be at pi
else, bv=varargin{4};
end

if nargin<2||isempty (varargin{2}), PixToLU=1;
else, PixToLU=varargin{2};
end

if nargin<3 || isempty (varargin{3})
    dv=(linspace(3,80,12))/(PixToLU); %distance vector in pixels
else,dv=varargin{3};
end

if nargin<1 || isempty(varargin{1})
    test_mode=true;
    [AA,bc]=AngleDistr([],[],dv,bv,0); %The default in AngleDistr gives you a WLC simulation with points spaced one basepair apart, and length units of nm.
    if PixToLU~=1
        warning('Using simulation values, but PixToLU ~=1, Lp results are not in nm')
    end
else, SPS=varargin{1};
end

if ~test_mode
    AC=cell(length(SPS),1);
    AA=zeros(length(dv),length(bv)-1);
    for ss=1:length(SPS)
        if isempty(SPS(ss).SnakeAng)||numel(SPS(ss).SnakeD)<2
            disp('Empty input, skipping'), disp(ss)
            continue
        end
        [AC{ss},bc] = AngleDistr(SPS(ss).SnakeAng,SPS(ss).SnakeD,dv,bv,0);
        AA=AA+AC{ss};
    end
end
binwidth=diff(bv);binscale=repmat(1./binwidth,length(dv),1); %to get a count density, we need to scale numbers with the bin widths
scalemat= binscale./repmat(sum(AA,2),[1,length(bc)]); %and to get a probabiltiy density we need to scale with the total counts
DiffAngPD=scalemat.*AA; %probability density of differential angles
t1=find(bv>1,1,'first'); %approximately theta =1, for start point estimation of the fit
PLfunc=fittype('A*exp(-0.5*(Lp./L)*theta.^2)','independent','theta','problem','L');
PLFits=cell(size(dv));
Lpfit=dv*0;
for ii=1:length(dv)
    Astart=max(DiffAngPD(ii,:));Lpstart=2*dv(ii)*log(Astart/DiffAngPD(t1));
    PLFits{ii}=fit(bc',DiffAngPD(ii,:)',PLfunc,'problem',PixToLU*dv(ii),'StartPoint',[Astart,Lpstart]);
    Lpfit(ii)=PLFits{ii}.Lp;
end
if doplots
    if islogical(doplots) 
        doplots=figure(doplots);
    else
        figure(doplots);
    end
    
    for ii=1:length(dv)
        lstring=sprintf('L=%.1i nm',dv(ii)*PixToLU);
        scatter(180*bc/pi,DiffAngPD(ii,:),16,'o','DisplayName',lstring); hold on
        if Lpfit(ii)>0
           lstring=sprintf('fit: L=%.1i nm,Lp=%.1i nm',dv(ii)*PixToLU,Lpfit(ii));
           plot(180*bc/pi,PLFits{ii}(bc),'DisplayName',lstring);           
        end        
    end
    hold off
    xlabel('theta(deg)')
    ylabel('p(theta,L)')
    legend(gca,'Location','northeast')
end
AngleStruct.AllAngles=AA;
AngleStruct.Distances=dv;
AngleStruct.BinCenters=bc;
AngleStruct.fits=PLFits;
AngleStruct.Lp=Lpfit;
AngleStruct.AngleProbL=DiffAngPD;
AngleStruct.PixToLU=PixToLU;
end
