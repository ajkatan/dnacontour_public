function [NN,NNindsL,NNindsS,edgeOK] = GetNearestNeighbours(A,ind1,ind2,indtype)
%GetNearestNeighbours Gets nearest neighbours of matrix element
% Neighbours= (in the matrix)
% N
% E 
% S
% W
% NE
% SE
% SW
% NW
% or
% |8 1 5|
% |4 x 2|
% |7 3 6|
% Note that in DNAcontour, images have the origin (x=1,y=1)in the bottom left,
%so in an image it will look like this
% |5 2 6|
% |1 x 3|
% |8 4 7|

%the first 4 are the nearest, the last four the next nearest
%  
if nargin <3 
    indtype='linear';
end
if nargin==3
    indtype='subscript';
end

neighboursr=[-1 0 1 0  -1 1  1 -1]';
neighboursc=[ 0 1 0 -1  1 1 -1 -1]';


[nr,nc]=size(A);
switch indtype
    case 'linear'
        [indrow,indcol]=ind2subFast2D(nr,ind1);
    case 'subscript'
        indrow=ind1;indcol=ind2;
end

NNindsRow=neighboursr+indrow;
OOBr=NNindsRow<1|NNindsRow>nr;%out of bounds
NNindsCol=neighboursc+indcol;
OOBc=NNindsCol<1|NNindsCol>nc;%out of bounds

edgeOK=~(OOBr|OOBc); %not out of bounds
%prefill with NaN's
NN=[NaN;NaN;NaN;NaN;NaN;NaN;NaN;NaN];
NNindsL=NN;
if nargout>2
NNindsS=[NN,NN];
NNindsS(edgeOK,:)=[NNindsRow(edgeOK),NNindsCol(edgeOK)];
end
NNindsL(edgeOK)=sub2indFast2D(nr,NNindsRow(edgeOK),NNindsCol(edgeOK));
NN(edgeOK)=A(NNindsL(edgeOK));

end





