function SPS=SubPixDNA(SK,imdata,pars,DoEcho)
%gets subpixel info from DNA that has been skeletonized.
%First uses nearest-neighbour weighted centroid algorithm with
%optional smoothing with window size csm.

%The data from the first method is the startpoint for an active contour
%iteration
%
if nargin==0 %test mode
    d=0.001;
    namp=0.5*d^1.5;
    t=(d:d:3)';
    x=-(t.^0.5).*cos(2*pi*t)+namp*randn(size(t));
    y=(t.^2).*sin(2*pi*t)+namp*randn(size(t));
    x=x+namp*randn(size(x));
    [Ang,Curv]=AngAndCurv(x,y);
    csm=5;
    [As,Cs]=AngAndCurv(quicksmooth(x,csm),quicksmooth(y,csm));
    figure(212);plot(x,y,t(2:end),Ang/pi,'.r',t(2:(end-1)),Curv,'.g',t(2:end),As/pi,'-r',t(2:(end-1)),Cs,'-g','LineWidth',2);grid on
    SPS=[];
    DoEcho=1;
else
    if nargin<4
        DoEcho=0;
    end
    if nargin<3
        csm=1;
        a=2;
        b=0.5;
        n=10;
    else
        csm=pars.snake.smooth;
        a=pars.snake.Hweight;
        b=pars.snake.stepsize;
        n=pars.snake.n_its;
    end
    if floor(csm/2)==csm/2 %if even
        csm=csm+1;
    end
    [x,y]=SubPixSkel(SK.Path,imdata);
    if numel(x)>=3
        if csm>1
            x=quicksmooth(x,csm);
            y=quicksmooth(y,csm);
        end
        SPS.Path=[x,y];
        [SPS.Angle,SPS.Curv,SPS.D]=AngAndCurv(x,y);
        [SPS.SnakeX,SPS.SnakeY,SPS.SnakeVals]=SnakeContour(x,y,imdata,a,b,n,1,DoEcho);
        [SPS.SnakeAng,SPS.SnakeCurv,SPS.SnakeD,SPS.SnakeLength]=AngAndCurv(SPS.SnakeX,SPS.SnakeY);
    else %two point paths are a bit useless but to avoid errors with stuff not being assigned I include them like this
        SPS.Path=[x,y];
        SPS.Angle=AngAndCurv(x,y);SPS.Curv=0;SPS.D=0;
        SPS.SnakeX=x;SPS.SnakeY=y;SPS.SnakeVals=0;
        SPS.SnakeAng=SPS.Angle;SPS.SnakeCurv=SPS.Curv;SPS.SnakeD=SPS.D;SPS.SnakeLength=0;
    end
end
end



