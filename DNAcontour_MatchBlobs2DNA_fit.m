function [foundpeaks] = DNAcontour_MatchBlobs2DNA_fit(foundpeaks,SK,pathim,sk_im,on_DNA_thresh,doplots)
%Matches blobs to their respective DNA positons
%version that matches blobs found by AFMtrack segmenting procedure

[pathdist,pathID]=bwdist(pathim);
[skdist]=bwdist(sk_im);
%make a sort of 'labelmatrix' function for SK structure. Can't uses labelmatrix
%because it is not guaranteed the order will be the same
Lmat=zeros(size(sk_im),'uint16');
for ss=1:length(SK)
    xsub=SK(ss).PixelList(:,1);ysub=SK(ss).PixelList(:,2);
    x=xsub+SK(ss).Xoff;y=ysub+SK(ss).Yoff;
    lind=sub2indFast2D(size(Lmat,1),x,y);
    Lmat(lind)=ss;
end

if doplots
    DNAcontour_plotim(pathdist.*(pathdist<on_DNA_thresh));hold all
    title('path distances and colocalization')
end
blobx=[foundpeaks.xfit];
bloby=[foundpeaks.yfit];
[blobxp,blobyp]=round_to_index(size(pathim),blobx,bloby);
for ii=1:length(foundpeaks)   
    foundpeaks(ii).onPath=pathdist(blobxp(ii),blobyp(ii))<on_DNA_thresh;
    foundpeaks(ii).onDNA=skdist(blobxp(ii),blobyp(ii))<on_DNA_thresh;
    if foundpeaks(ii).onDNA
        if doplots
            plot(blobx(ii),bloby(ii),'og','MarkerSize',12)
        end
        if foundpeaks(ii).onPath
            p_idx=pathID(blobxp(ii),blobyp(ii));[p_x,p_y]=ind2sub(size(pathID),p_idx);
            ss=Lmat(p_idx);foundpeaks(ii).blob_path=ss;
            p_lind=sub2ind(SK(ss).ImSize,p_x-SK(ss).Xoff,p_y-SK(ss).Yoff); %linear index in the subimage
            pp=find(SK(ss).Path==p_lind);
            foundpeaks(ii).blob_pos=pp;
            foundpeaks(ii).blob_dist=SK(ss).Dist(pp);
        else
            foundpeaks(ii).blob_path=0;
            foundpeaks(ii).blob_pos=0;
            foundpeaks(ii).blob_dist=-1;
        end
    end
end
hold off

end

