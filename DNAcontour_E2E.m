function [P2P,DC,D]=DNAcontour_E2E(X,Y,D,stepsize)
%produces vectors P2P and DC, that contain straight point to point length (=end to end length of sections) 
%and distance along the contour for pairs of points. The set from which these are calculated are the coordinate vectors X and Y
%D, the distance from the start along the contour for each XY pair, can be
%calculated here or given as input.
%To limit the nr of combinations, a stepsize can be given
%
%
if nargin==2 || isempty(D)
   Xdif=[0; diff(X)];Ydif=[0;diff(Y)] ;
   D=cumsum(sqrt(Xdif.^2+Ydif.^2));
end
if nargin<4 || isempty(stepsize)
   stepsize=1;
end
if all([size(X)==size(Y),size(X)==size(D)]) && iscolumn(X)
   %nothing
elseif all([numel(X)==numel(Y),numel(X)==numel(D)])
  X=X(:)';Y=Y(:)';D=D(:)';
else
    sprintf(' X: %.0f by %.0f,Y: %.0f by %.0f,D: %.0f by %.0f',[size(X),size(Y),size(D)])
    error('Wrong sizes')    
    return
end

n=numel(X);
N0=1:stepsize:(n-1);
N=sum(N0(1:(end-1))); %total nr of elements you need
P2P=zeros(N,1);
DC=zeros(N,1);
kk=1;istart=1;
   for ii=istart:stepsize:(n-1)
       arange=((ii+1):stepsize:n)';
       n_added=numel(arange);
       P2P(kk:(kk+n_added-1))=sqrt((X(arange)-X(ii)).^2+(Y(arange)-Y(ii)).^2);
       DC(kk:(kk+n_added-1))=D(arange)-D(ii);
       kk=kk+n_added;
   end
   
