function [xp,yp]=round_to_index(Size,x,y);
%rounds real values to indices of a matrix of size Size
%coord should have same dimensions as Size
xp=round(x);
xp=max(xp,1);
xp=min(xp,Size(1));
yp=round(y);
yp=max(yp,1);
yp=min(yp,Size(2));