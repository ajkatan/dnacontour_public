function DNAcontour_plotim(pic,overlayvector);
imagesc(pic')
axis xy
axis equal
if nargin>1 && ~isempty(overlayvector)
    MSiz=ceil(mean(size(pic))/7);
    hold all;
    scatter(overlayvector(:,1),overlayvector(:,2),MSiz,'+m');
end
