function lind=sub2indFast2D(n_row,i1,i2) %fast 2d version of sub2ind
lind=(i2-1)*n_row+i1;
end