function [xoff,yoff]=plot_subim(im,BB,varargin)
xoff=floor(BB(2));xext=BB(4);
yoff=floor(BB(1));yext=BB(3);
DNAcontour_plotim(im(xoff+(1:xext),yoff+(1:yext)));
xoff=xoff-0.5;yoff=yoff-0.5;
if nargin>2 %if xy pairs specified
   hold on
   n=2;
   while nargin>=2+n
       plot(varargin{n-1}-xoff,varargin{n}-yoff);hold all
       n=n+2;
   end
       hold off
end
end 