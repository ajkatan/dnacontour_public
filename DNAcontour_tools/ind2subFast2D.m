function[i1,i2]=ind2subFast2D(n_row,lind)
%faster replacement of ind2sub for 2d matrices
i2=ceil(lind/n_row);
i1=lind-(n_row)*(i2-1);
end