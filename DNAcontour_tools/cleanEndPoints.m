function [EndPointsOut,zapit,EPimOut]=cleanEndPoints(EndPointsIN,SK_im,EPim)
%90 degree bends in skeletons are seen by the 'endpoint' algorithm as
%single pixel sidearms from the shortest (diagonal) path. If you want to
%keep the 90 degree bend because it reflects the underlying structure more,
%you get an endpoint that is actually sandwiched between points. 
%I clean up those points here
%Note that without cleaning the branchpoints as well, this situation still
%exists for the branchpoints, leaving single pixel branches that have no
%endpoint.

%AK, july 2018: more efficient seems to be to use 'skel' instead of 'thin',
%which eliminates the 90 degree bends in favour of the shorter path. Using
%'thin' may get more accurate representation but the subpixeling probably
%clears that up (to be checked)

if nargout>2 &&(nargin<3 ||isempty(EPim))
    EPim=false(size(SK_im));
    EPim(EndPointsIN)=true;
end

threeinarow=[1 5 8;4 7 8;3 6 7;2 5 6];
zapit=false(size(EndPointsIN));
for cc=1:length(EndPointsIN)
    [enn,ennind]=GetNearestNeighbours(SK_im,EndPointsIN(cc));
    zapit(cc)= numel(ennind(enn==1))>3 || ... %more than 3 nearest neighbours
        (numel(ennind(enn==1))==3 && ~ismember(find(enn)',threeinarow,'rows')) ; %or three that are not in a row
end
EndPointsOut=EndPointsIN(~zapit);

if nargout>2
    EPimOut(zapit)=false;
end