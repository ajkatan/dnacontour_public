function [StartPoint,EndPoint,d_s,d_e] = MatchClick2Path(ClickPoints,WhichPoints,SK)
%Matches a set of clicked points to a Path in a skeleton structure
p1=repmat(ClickPoints.Points(WhichPoints(1),:),length(SK.Path),1); %p1 is a double column of the form [x,y;x,y; etc]
p2=repmat(ClickPoints.Points(WhichPoints(2),:),length(SK.Path),1);
[xpath,ypath]=ind2sub(SK.ImSize,SK.Path);
dp1=sum(([xpath,ypath]-p1).^2,2); %squared distance
dp2=sum(([xpath,ypath]-p2).^2,2);
[d_s,S]=min(dp1);
[d_e,E]=min(dp2);
StartPoint=min(S,E);EndPoint=max(S,E); %sort them