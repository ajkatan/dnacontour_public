function c=quicksmooth(y,width) 
%moving average part cut out of the toolbox smooth function
%this function is quicker but there are no checks, use at your own risk        
    n = length(y);        
            c = filter(ones(width,1)/width,1,y);
            cbegin = cumsum(y(1:width-2));
            cbegin = cbegin(1:2:end)./(1:2:(width-2))';
            cend = cumsum(y(n:-1:n-width+3));
            cend = cend(end:-2:1)./(width-2:-2:1)';
            c = [cbegin;c(width:end);cend];
    end