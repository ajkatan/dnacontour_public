function [convim,tipim]=tipconvolution(orim,tipsize,psize) %tip radius and image heigth in same units, psize gives size of a pixel in these units
max_im=max(orim(:));
convsize_nm=sqrt(max_im*tipsize); %radius in nm of the convolution matrix
convsize=round(convsize_nm/psize); %radius in pixels of the convolution matrix
convsize=max(2*convsize+1,3);% no less than 1
convgrid=convsize*psize*radialgrid(convsize);%diameter must be odd
tipim=(convgrid.^2)/tipsize; %parabola tip
linc=find(tipim==0);%center index of tip parabola
convim=nlfilter_stripped(orim,[convsize,convsize],@convmaxfun);

    function cm=convmaxfun(imforconv)
        cval=imforconv(linc); %value under tip
        centersub=imforconv-cval; %diff between rest of area and tip height
        cm0=(centersub-tipim).*(centersub>=tipim); %if higher than tip parabola, this is difference
        cm=max(cm0(:))+cval; %add the highest difference
    end
end