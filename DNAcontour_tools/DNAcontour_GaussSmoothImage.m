function [smooth_pic,winsize]=DNAcontour_GaussSmoothImage(pic,sigma,winsize,nreps)
%performs Gaussian smoothing of an image
%Jacob Kerssemakers & Allard Katan 2014
doplot=0;
if nargin<1 || isempty (pic) %test or debug
    debug=1;
    doplot=1;
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    one_up=split(thisfolder,filesep);
    one_up=fullfile(one_up{1:end-1});
    tdfolder=fullfile(one_up,[filesep,'Test data']);
    tim1=getfield(load(fullfile(tdfolder,'testims.mat')),'tim1');
    pic=tim1.data;
end

if nargin<2 || isempty(sigma)
    sigma=1;
end
if nargin<3 || isempty(winsize)
    winsize=[5 5];
elseif length(winsize)==1
    winsize=[winsize,winsize];
end
if nargin<3 || isempty(nreps)
    nreps=2;
end

winsize_new=1+2*(floor(winsize/2)); %make sure it is ODD for symmetry (otherwise you get a shift)
if ~all(winsize==winsize_new)
    winsize=winsize_new;
    fprintf('Window size adapted to %i by %i \n',winsize)
end
h = fspecial('gaussian', winsize,sigma);
smooth_pic=imfilter(pic,h,'replicate','same');
if nreps>1
    for ii=2:nreps
        smooth_pic=imfilter(smooth_pic,h,'replicate','same');
    end
end
if doplot
    figure(101);
    ax1=subplot(1,2,1);DNAcontour_plotim(pic); title('original');DNAcontour_ImageAutoScale(pic);
    ax2=subplot(1,2,2);DNAcontour_plotim(smooth_pic);title('smooth');DNAcontour_ImageAutoScale(pic);
    linkaxes([ax1 ax2],'xy');
    if debug
        figure(102);
        DNAcontour_plotim(smooth_pic+pic);title('added');DNAcontour_ImageAutoScale(pic);
    end
end