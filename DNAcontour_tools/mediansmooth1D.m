function m=mediansmooth1D(x,ws);
%1D median smoother. Allows for row vectors but columns are faster
%ws is window size
if ~isvector(x)
    error('x is not a vector')
end
if size(x,1)==1
   x=x';
end
%we build a ws x length(x) matrix, with some extra colums to account for
%the window
mm=NaN*ones(ws+length(x)-1,ws);%initialize NaN's
for ii=1:ws
mm(ii:(ii+length(x)-1),ii)=x; %fill with X, shifting 1 each time
end
%then we take the median along the columns
m=nanmedian(mm,2);
m0=ceil(ws/2);
me=floor(ws/2);
if ws>2
    m=m(m0:(end-me));
else
    m=m(1:length(x));
end