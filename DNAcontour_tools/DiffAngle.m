function aout=DiffAngle(a1,a2)
%input:angles defined -pi-+pi
%output: difference angle, between -pi and pi
aout=a1-a2;
%we need to correct this to take out the 2 pi modulation
a_pos=aout>0; %all the positive diffs
a_long=abs(aout)>pi; %true if it is shorter to go through -pi
aout(a_long)=2*pi-abs(aout(a_long)); %those get the shorter angle. But 2pi-|a| is always positive
%if the difference was positive before correction, it should become negative
aout(a_pos&a_long)=-aout(a_pos&a_long);%which is what we do here