function grid=radialgrid(varargin)
%syntax(grid=radialgrid(nx,{ny}). ny is optional, ny=nx if not given)
    xsize=varargin{1};
    if rem(xsize,2)==0 %why is there no 'even' function??
        disp('Adding 1 to the X size. Size must be odd!')
        xsize=xsize+1;
    end
    if nargin>1
        ysize=varargin{2};
        if rem(ysize,2)==0
        disp('Adding 1 to the Y size. Size must be odd!')
        ysize=ysize+1;
    end
    else
        ysize=xsize;
    end
    [gridX,gridY]=meshgrid(1:xsize,1:ysize);
    gridX=gridX-(xsize-1)/2-1; %Center around zero The last minus 1 because the meshgrid starts at 1 
    gridY=gridY-(ysize-1)/2-1;
   grid=sqrt(gridX.^2+gridY.^2)./(min(xsize,ysize)-1); % normalize to have radius of inner circle as 1
end