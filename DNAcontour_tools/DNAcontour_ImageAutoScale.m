function hscale=DNAcontour_ImageAutoScale(pic,fign)
%Autoscales an image to median +5/-1 standard deviation of an input.
%Input is not necessarily the same as the figure data.
%Jacob Kerssemakers & Allard Katan 2014

if nargin<2 || isempty(fign)
    fign=gcf;
end
hmean=nanmedian(pic(:));hstd=nanstd(pic(:));
hscale=[hmean-hstd,hmean+5*hstd];
figure(fign);caxis(hscale);