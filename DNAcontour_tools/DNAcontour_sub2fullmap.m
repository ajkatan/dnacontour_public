function [i1,i2]=DNAcontour_sub2fullmap(idx,xoff,yoff,subim_nrows,fullim_nrows)
%maps positon of subimage back to full image
%input idx: if nx2 interpreted as [x,y], if nx1 interpreted as linear index
%output: i1 =x if nargout=2, linear if nargout=1;
%padding of subimages is assumed to be taken into account in the offset
%values

if isempty(idx)
   i1=[];i2=[];
   return
elseif size(idx,2)==2
   x=idx(:,1);y=idx(:,2);
elseif size(idx,2)==1
   [x,y]=ind2subFast2D(subim_nrows,idx);
else
    error('wrong size input')
end
xout=x+xoff;yout=y+yoff;

if nargout==1 %if linear index
    i1=sub2indFast2D(fullim_nrows,xout,yout);
else
    i1=xout;
    i2=yout;
end
    
    