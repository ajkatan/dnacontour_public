function ptq(ss)
%plot trajectory quickly
%a tool to visually inspect trajectories from command line with a very short command
if nargin==0
    ss=1;
end
SubIms=evalin('caller','SubIms');
SPS=evalin('caller','SPS');
SK=evalin('caller','SK');
figure(gcf);clf;
DNAcontour_plotim(SubIms(ss).pic);hold all;
plot(SK(ss).PixelList(:,1),SK(ss).PixelList(:,2),'ok','MarkerFaceColor','k');
[px,py]=ind2subFast2D(SK(ss).ImSize(1),SK(ss).Path);
plot(px,py,'-y');
plot(SPS(ss).SnakeX,SPS(ss).SnakeY,'-r');
hold off
figure(gcf);legend('Skeleton','Path','Optimized');