function [ImData,ch_info,global_info] = OpenNanoscopeAK(file_name,ch2read)
% Reads Nanoscope image data.
% input is a file name (dialog if empty), and optionally a numeric array with the channels you want to read
%Output ImData is a struct with each field named according to the
%channel name, and each field contains an image
%ch_info and global_info contain information on individual channels and the
%scan parameters
% Written by Allard Katan, TU Delft, 2015. a.j.katan@tudelft.nl
%
% Inspired by openNano version 6.0 originally writen by Jaco de Groot
%

if nargin==0 || isempty(file_name)
    [filename0,path0]=uigetfile('*.*','Pick input file');
    file_name=fullfile(path0,filename0);
end

%In the data header, information is presented in lines like this example:
%
% \@2:Z scale: V [Sens. Zsens] (0.001488662 V/LSB) 2.777844 V
%
%The subfunction read_header_values finds these lines and extracts the numbers necessary.
%If you need to read in additional paramters, below you can tell what to
%search for and which variables that goes into. To do so, add new lines to
%the ValSearchString.
%format: Each row is a value. Per line:
%column 1: Identifier string of the line (what to look for in the data header), use regular expression syntax. So
%e.g. \\ for a single backslash.
%column 2: index, so whether you want the first, second etc number listed
%on the line. Allows you to skip V/LSB values or @2 etc. Also allows for
%reading multiple numbers per line.
%column 3: name of the field  you want to assign the number(s) to.

ValSearchString={...
    '\\Version:',2, 'SoftwVersion',...   % software version
    ;'\\Scan size:',[1 2], 'ScanSize'... %scan size (image data),
    ;'Aspect ratio:',[1 2], 'Aspect'... %Image size aspect ratio'
    ;'\\Samps/line:',1, 'SampsLine'... nr of points per scanline
    ;'Number of lines:',1,'Nlines',... nr of lines in the recorded image
    ;'\Lines:',1,'NlinesOri',... nr of lines original scan setting (equal to Nlines unless you used the 'save now')
    ;'Data offset',1, 'DataStart'...   %tells you where to start reading
    ;'\@2:Z scale',3, 'Scaling'... %Scaling values for all the channels. They are all called Z scale, to make it easy
    };
%Strings can be read with another cell, syntax is similar.
%This looks first for double quotes. If the line doesn't contain quotes,
%the whole line except the search string is used.
StringSearchString={...
    ;'Image Data:',1, 'ChannelName'... %name of the channel
    ;'\\Line Direction: ',1,'LinDir'... %trace or retrace. note the space!
    };

[param,ReadPos]=read_header_values(file_name,ValSearchString,StringSearchString);
L = length(param.DataStart); %number of times a data start is found, so number of channels
if nargin<2 || isempty(ch2read)
    ch2read=1:L; %default: read all channels
elseif any(ch2read>L)
    warning('Only %i channels are present in the file, ignoring higher channel nrs',L);
    ch2read=ch2read(ch2read<=L);
end

if param.SoftwVersion/1e6 >=9.2 % after 9.2, data is stored in 32 bit numbers
    DataType='int32';
else
    DataType='int16';
end
%this loop reads the channels.
fid = fopen(file_name,'r');
im=0;
for pp=ch2read
    im=im+1;
    chname=regexp(param.ChannelName{ch2read(im)},'[\s\.]*','split'); %take out dots and spaces
    ch_info(im).Name=[chname{:}]; %Name of channel
    ch_info(im).Scaling=param.Scaling(ch2read(im));
    ch_info(im).Direction=param.LinDir{ch2read(im)};
    %To get the 'Unit Sens' scaling values we first need to know what they are called.
    %So we read that name, and build a search string for it to run another
    %read later.
    fseek(fid,ReadPos.Scaling(ch2read(im),2),'bof');
    SensLine=fgets(fid);
    SensString=regexp(SensLine,'\[.*\]','match'); %get everything in square brackets
    SensString=SensString{1};SensString=SensString(2:end-1); %de-cell and strip brackets
    SensName{im}=sprintf('Ch%iSens',ch2read(im));
    SensSearch(im,:)={['\@' SensString ':'],1,SensName{im}}; %the final search string
    
    %Now read the actual data
    fseek(fid,param.DataStart(im),'bof');
    if im==1 || ~isfield(ImData,ch_info(im).Name) %if no trace-retrace duplications
        ImData.(ch_info(im).Name) = fread(fid, [param.SampsLine(1) param.Nlines(1)],DataType); %read one data channel
    elseif  strcmpi(ch_info(im).Direction,'Trace')
        ImData.([ch_info(im).Name '_T']) = fread(fid, [param.SampsLine(1) param.Nlines(1)],DataType); %read one data channel, add a T to indicate trace
    elseif  strcmpi(ch_info(im).Direction,'Retrace')
        ImData.([ch_info(im).Name '_R']) = fread(fid, [param.SampsLine(1) param.Nlines(1)],DataType); %read one data channel, add a R to indicate retrace
    end
    
end %channel read loop

[ChSenses,~]=read_header_values(file_name,SensSearch,{}); %Do a new read for the sensitivities associated with each channel

for nn=1:length(ch2read)
    ch_info(nn).UnitSens=ChSenses.(SensName{nn});
    ImData.(ch_info(nn).Name)=ch_info(nn).UnitSens*ch_info(nn).Scaling*ImData.(ch_info(nn).Name)/2^16;
end

%special treatment for scan size to find out if it is in nm or um
fid = fopen(file_name,'r');
fseek(fid,ReadPos.ScanSize(1,2),'bof'); %byte position of the first scansize line
SSline=fgets(fid);
is_um = regexp(SSline,'~m', 'once');is_nm=regexp(SSline,'nm', 'once');
if ~isempty(is_um), SSscaler=1e-6;elseif ~isempty (is_nm), SSscaler=1e-9;else SSscaler=1; end
param.ScanSize=SSscaler*param.ScanSize;

fclose(fid);
%strip away unnecessary repeats of parameters and store in global info
global_info.ScanSize=param.ScanSize(1,:);
global_info.Aspect=param.Aspect(1,:);
global_info.SampsLine=param.SampsLine(1,:);
global_info.Nlines=param.Nlines(1,:);
if param.NlinesOri ~=global_info.Nlines %correct for 'save now' images. in this 'if' to make it explicit
    global_info.ScanSize(2)=(global_info.ScanSize(2)/param.NlinesOri)*global_info.Nlines;
    global_info.Aspect(2)=(global_info.Aspect(2)/param.NlinesOri)*global_info.Nlines;
end
helptext={'Description of variable units: ';...
    'Data in ImData is scaled into units similar to display during scanning';...
    'ScanSize is in meters. Height values are always in nanometers. ';...
    'Other variables can differ based on Nanoscope software version and settings. ';...
    'Phase is usually in degrees. Amplitude can be volts or nanometers, etc. ';...
    'The "Scaling" in ch_info refers to a full scale';...
    };
global_info.Help=[helptext{:}];
end %main function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [parameters,readpos] = read_header_values(file_name,VSS,SSS,parameters);
%reads the parameters from the file by scanning each lines for predefined
%strings. If it finds such a line, it either extracts values (for strings
%in cell VSS) or names (in cell SSS).
%
fid = fopen(file_name,'r');
[message,errnum] = ferror(fid);
if(errnum)
    fprintf(1,'I/O Error %d \t %s',[errnum,message]);
    %   break
end
header_end=0; eof = 0;
nvs=size(VSS,1);nss=size(SSS,1);
nstrings=nvs+nss;
n=zeros(nstrings,1); %counts each time a single string is found
for ij=1:nvs    %initialize parameters to empty
    parameters.(VSS{ij,3})=[];
    readpos.(VSS{ij,3})=[];
end;
for ij=1:nss    %initialize parameters to empty
    parameters.(SSS{ij,3})=[];
    readpos.(SSS{ij,3})=[];
end;
lcount=0;
while( and( ~eof, ~header_end ) )
    currpos=ftell(fid);
    line = fgetl(fid); %read one line at a time
    lcount=lcount+1;
    for ii=1:nvs %run over all the value searchstrings
        thisfield=VSS{ii,3};
        if any(regexpi(line,VSS{ii,1})) %check if this seachstring is in this line. %AK changed to case-sensitive! %AK 2017-04: Changed back because of new software changes. Forgot why I changed it in the first place
            vals=getnumbers(line); %takes numbers out of the line
            if length(vals)>=max(VSS{ii,2})&&length(vals)>=length(VSS{ii,2}) %if there are enough numbers in this line
                n(ii)=n(ii)+1; %counts the number of times this value was assigned
                parameters.(thisfield)(n(ii),:)=[vals{VSS{ii,2}}]; %put the right number into the field
                readpos.(thisfield)(n(ii),:)=[lcount currpos]; %store where you found it, line and byte position
            end
        end
    end
    for jj=1:size(SSS,1); %run over all the string searchstrings
        tt=jj+nvs;
        thisfield=SSS{jj,3};
        if any(regexpi(line,SSS{jj,1})) %check if this seachstring is in this line
            quoted=regexpi(line,'(?<=").*(?=")','match'); %matches anything between double quotes
            if length(quoted)>=SSS{jj,2}
                n(tt)=n(tt)+1; %counts the number of times this string was assigned
                parameters.(thisfield){n(tt)}=quoted{SSS{jj,2}};
            else
                n(tt)=n(tt)+1;
                StringLine=regexpi(line,SSS{jj,1},'split');
                parameters.(thisfield){n(tt)}=[StringLine{:}]; %need to glue them together because a cell comes out
            end
            readpos.(thisfield)(n(tt),:)=[lcount ftell(fid)];
            
        end
    end
    if( (-1)==line )
        eof  = 1;
    end
    if ~isempty( strfind( line, '\*File list end' ) )
        header_end = 1;
    end
    
end
fclose(fid);
end %read header values

function [vals,nvals]=getnumbers(instring) 
nrs='([-+])?[0-9]+(\.[0-9]+)?([eE]([-+])?([0-9]+))?'; %optional +/-, number, optional decimal digits, optional(e or E, optional +/-, number)
thenrs=regexpi(instring,nrs,'match');
vals=[];nvals=0;
if ~ isempty(thenrs)
    nvals=length(thenrs);
    vals=cell(nvals,1);
    for ii=1:nvals
        vals{ii}=sscanf(thenrs{ii},'%g');
    end
end
end