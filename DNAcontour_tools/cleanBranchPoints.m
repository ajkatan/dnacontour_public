function [BranchPointsOut,zapit,BPimOut]=cleanBranchPoints(BranchPointsIN,SK_im,BPim)
%Eliminates 90 degree corner points from branchpoint lists
%In the branchpoint algorithm, 90 degree corners in a skeleton are seen as
%a branchpoint, with one branch the 45 degree shortcut, and the other one a
%single pixel

%AK, july 2018: not necessary anymore if you use 'skel' instead of 'thin'
%for the skeletonization

if nargout>2 &&(nargin<3 ||isempty(BPim))
    BPim=false(size(SK_im));
    BPim(BranchPointsIN)=true;
end

%Getnearestneigbours has the following index numbering
% |8 1 5|
% |4 x 2|
% |7 3 6|
% Note that in DNAcontour, images have the origin (x=1,y=1)in the bottom left,
%so in an image it will look like this
% |5 2 6|
% |1 x 3|
% |8 4 7|

%The branchpoints to be eliminated are not the corner pixels but the ones next to them.
%90 degree corner possibilities therefore are:
elbow=[1 3 5;... %corner at 1
    1 3 8;...
    2 4 5;... %corner at 2
    2 4 6;...
    1 3 6;... %corner at 3
    1 3 7; ...
    2 4 7;... %corner at 4
    2 4 8;...
    ];
elbowlogic=false(size(elbow,1),8);
for ii=1:size(elbow,1)
    elbowlogic(ii,elbow(ii,:))=true;
end
%that means we are looking for points that have exactly 3 nearest
%neighbours, and whose neighbours are in those positons
%which 2 are in
zapit=false(size(BranchPointsIN));
for cc=1:length(BranchPointsIN)
    [enn,ennind]=GetNearestNeighbours(SK_im,BranchPointsIN(cc));
    ennL=enn==1;%make enn logical , so we can use it for indexing to check the number
    WhichElbow=ismember(enn',elbowlogic,'rows');
     if any(WhichElbow)
    cornerindex=ennind(ceil(find(WhichElbow)/2)); %Find which corner to check.This gets you the linear index into the skeleton image of that point
    cornerneighbours=GetNearestNeighbours(SK_im,cornerindex);
    zapit(cc)= sum(enn(ennL))==3 && ... 
               sum(cornerneighbours)<=2; % only if there is not a t junction
     end
    
end
BranchPointsOut=BranchPointsIN(~zapit);
if nargout>2
    BPimOut=BPim;
    BPimOut(zapit)=false;
end