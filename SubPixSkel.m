function [xc,yc]=SubPixSkel(Sk_IDX,imdata)
%Finds the sub-pixel correction for skeleton or path by center of mass
%method
%fast but sensitive to errors >1 pixel in the skeleton
%input: linear indices of the skeleton/path and image data 
%output: weighted centroid in x and y in a 1 pixel radius around the skeleton point
if any(imdata(:)<0) %negative values disturb the weighting
    imdata=imdata-min(imdata(:));
end
neighboursr=[-1 0 1 0  -1 1  1 -1]';
neighboursc=[ 0 1 0 -1  1 1 -1 -1]';
skelimdata=imdata(Sk_IDX);
[xc,yc]=ind2subFast2D(size(imdata,1),Sk_IDX); %first the integer pixel data

for ii=1:length(Sk_IDX)
    NN=GetNearestNeighbours(imdata,xc(ii),yc(ii),'subscript');
    NN(isnan(NN))=0;
    xc(ii)=xc(ii)+sum(neighboursr.*NN)/sum([NN; skelimdata(ii)]); %here come the corrected values
    yc(ii)=yc(ii)+sum(neighboursc.*NN)/sum([NN; skelimdata(ii)]);
end    
% % %it can happen that this leads to non-unique points, which gives trouble
% % %later on. So we uniquify.
% % xy=unique([xc,yc],'rows','stable'); %only unique sets of points
% % xy2=interp1(xy,[1:length(xc)]'); %reinterpolate to the original nr of points
% % if any(isnan(xy2))
% %     warning('NaNs found in interpolation')
% %     xy
% % end
% xc=xy2(:,1);yc=xy2(:,2);