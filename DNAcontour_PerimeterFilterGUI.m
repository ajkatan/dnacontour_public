function [BW,P_t,A_t]=DNAcontour_PerimeterFilterGUI(BWin,P_t,A_t,skipgui);
%UNTITLED Filters a thresholded image for 'surface to volume ratio' with visual feedback
%   Detailed explanation goes here
if nargin<1
    thisfile=mfilename('fullpath');
    thisfolder=fileparts(thisfile);
    tim2=getfield(load(fullfile(thisfolder,'testims.mat')),'tim2');
    pic=tim2.data;
    BWin=pic>6e-10;
end
BWstruct= bwconncomp(BWin,8);
RP=regionprops(BWstruct,'Area','Perimeter','BoundingBox');
S2Vr=[RP.Perimeter]./[RP.Area];
BBA=zeros(size(S2Vr));
for ii=1:length(RP)
    BBA(ii)=RP(ii).BoundingBox(3)*RP(ii).BoundingBox(4);
end
A2Ar=[RP.Area]./BBA;
AFfig=figure;
subplot(5,1,1);nbins=max(1,round(length(S2Vr)/3));hist(S2Vr,nbins);
subplot(5,1,2);nbins=max(1,round(length(A2Ar)/3));hist(A2Ar,nbins);
if nargin<2 || isempty(P_t)
    P_t=0.2; %first try
end
if nargin<3 || isempty(A_t)
    A_t=0.2; %first try
end

if nargin<4||isempty(skipgui)
    skipgui=false;
end
stop_it=0;
Woptions.WindowStyle='normal';
CurrentZoomX=[0 size(BWin,1)];CurrentZoomY=[0 size(BWin,2)];
while ~stop_it
    idxToKeep = find((S2Vr>P_t)&A2Ar<A_t);
    BW =ismember(labelmatrix(BWstruct),idxToKeep);
    if skipgui
        break
    end
    figure(AFfig);subplot(5,1,3:5);    
    DNAcontour_plotim(BWin+BW);ImAx=gca;
    colormap(hot);set(ImAx,'Clim',[0 2]);title('included=white, excluded=orange');
    set(ImAx,'xlim',CurrentZoomX);set(ImAx,'ylim',CurrentZoomY);
    answer = inputdlg_tt({'Minimal perimeter to area ratio','Max area to BB area ratio'},'Set S2Vr Threshold',1,{num2str(P_t),num2str(A_t)},Woptions);
    if isempty(answer) % The 'done' button, aka 'cancel', gives an empty answer
        stop_it=1;
    else
        P_t=sscanf(answer{1},'%g');
        A_t=sscanf(answer{2},'%g');
    end
    CurrentZoomX=get(ImAx,'xlim');CurrentZoomY=get(ImAx,'ylim');
end
if ishghandle(AFfig)
close(AFfig);
end
end

