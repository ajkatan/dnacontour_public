function [SK,loop_im]=DNAcontour_FindLoops(SK,SK_subim)
%finds the loops in a skeletonized DNA image
LoopN=1-bweuler(SK_subim); % the number of holes in the image
LoopPixels=cell(1,LoopN); %preallocations
LoopBranches=cell(1,LoopN);
LoopEPBP=cell(1,LoopN);
EPBP=[SK.EndPoints;SK.BranchPoints]; %the end- and branchpoints
if LoopN;
    ExtLoopImage=bwmorph(SK_subim,'spur',Inf);
    %there are still connecting pieces between two loops in the loop image, as
    %they are not removed by spur. We solve that here.
    LoopInternals=imfill(ExtLoopImage,'holes')~=ExtLoopImage;  %fills up the holes in the loops and selects only the holes
    LoopIntLabels=labelmatrix(bwconncomp(LoopInternals,4)); %4 connected so you don't get connections across the skeleton
    %now we find the loops and match them to branches and
    %end/branchpoints
    for LL=1:max(LoopIntLabels(:)) %for each individual loop
        ThisInternal=false(size(ExtLoopImage)); ThisInternal(LoopIntLabels==LL)=true; %select one internal region
        DilateThis=bwmorph(ThisInternal,'dilate'); %dilate it to find the pixels adjacent to the border
        LoopPixels{LL}=find(DilateThis&ExtLoopImage);
        %map the branches to the loops
        LoopBranches{LL}=[];
        LoopEPBP{LL}=EPBP(ismember(EPBP,LoopPixels{LL}));
        %         if any(ismember(SK.EndPoints,LoopPixels{LL})) %there should not be endpoints on loops, right?
        %             warning('Region %i, loop %i contains an endpoint?',ii,LL)
        %         end
        skipmap=1;
        if~skipmap
        for kk=1:length(SK.BranchIdx) %afor each branch
            if all(ismember(SK.BranchIdx{kk},LoopPixels{LL})) %if this branch is completely contained in this loop
                LoopBranches{LL}=[LoopBranches{LL};kk]; %append
            end
        end
        end
    end
    
else
    ExtLoopImage=false(size(SK_subim));
end
%dump into structure
SK.LoopN=LoopN;
SK.LoopPixels=LoopPixels;
SK.LoopBranches=LoopBranches;
SK.LoopEPBP=LoopEPBP;
loop_im=ExtLoopImage;
end

