function subtracted=DNAcontour_bgsub(AFMim,varargin)
%AFM background subtraction program
%can subtract background from images
%5 methods implemented at the moment: line by line median offset, line by line linear,
%2D plane, 2D parabola 2D cubic and 4x2 polynomial
%user has the choice of using a threshold (expressed in standard
%deviations from the image mean).Data above and below the threshold are
%ignored for the background subtraction
%Multiple subtractions can be done in series, either different ones or the
%same type. Use 'alternate' to switch types during iterations.
%syntax: subtracted=AFMbg(AFMim,method,threshold,iterate,alternate)
%example code to simply fit and subtract a plane :
% subtracted=AFMbg(AFMim,'plane',0,0,0);
%example code to subtract a plane,then LBL offset, and then a parabola, with a 1.8 sd threshold:
% subtracted=AFMbg(AFMim,{'plane','LBLO','parab'},1.8,2,1);

%Allard Katan, TU Delft, april 2013


%inputparser
vinputnames={'method','threshold','iterate','alternate'}; %names of the inputs

vinputdefs={... %default values
    'LBLO',...  %method
    0, ...      %threshold sigma, 0 means no treshold
    0,...       %iterate, nr of iterations after first
    1,...       %alternate, between methods in iterations
    };
for ii=1:length(vinputnames)
    if ii<=length(varargin)&& ~isempty(varargin{ii}) %if it's given, assign values
        eval(sprintf('%s=%s;',vinputnames{ii},sprintf('varargin{%i}',ii)));
    else %assign defaults
        eval(sprintf('%s=%s;',vinputnames{ii},sprintf('vinputdefs{%i}',ii)));
    end
end
%conditional default for iterate
if (nargin<4 || isempty(varargin{3})) && length(method)>1
   iterate=length(method)-1;
end
    
%a block of code to allow single or multiple methods as inputs
%flags dominate over nr of methods in input
if iscell(method)&& (~alternate || ~iterate);
    method=method{1};
elseif iscell(method)&& alternate && iterate
    methods=method;
elseif ~iscell(method) && alternate && iterate
    methods={'plane','LBLO','parab'} ;%if the inputs are contradictory we fall back on this default
end % an else should not be necessary because that case is good


%end inputparser

subtracted=AFMim; %preallocation
for jj=1:(1+iterate)
    %a block to allow treshold on off switching
    if length(threshold)<jj
        threshold(jj)=threshold(end);
    end
    imtemp=subtracted; %in principle this is unnecessary, we could use 'subtracted' at the moment but it doesn't really hurt
    if iterate && alternate
        nmeth=length(methods);
        itind=mod(jj,nmeth);
        if itind==0
            itind=nmeth; %now we have a 1 2 3 1 2 3 etc iteration index
        end
        method=methods{itind}; %note the circular definition here regarding methods and method!
    end
    
    switch method
        case {'LBLO','LBLL'}
            if threshold(jj) %threshold creates a matrix which is true for the points that should be ignored
                linemedian=median(imtemp,1);
                linesd=std(imtemp,1);
                upper=linemedian+0.1*threshold(jj)*linesd;%for clarity
                lower=linemedian-2*threshold(jj)*linesd;
                
                %upper=linemedian+threshold(jj)*linesd;%for clarity
                %lower=linemedian-threshold(jj)*linesd;
                validpoints=bsxfun(@le, imtemp, upper)& bsxfun(@ge,imtemp, lower); % this does a line by line comparison and creates a 2D logical matrix that is 1 for points inside the interval
                %we do the threshold line by line because there can be problems with
                %all point on a line being out of bounds
            else
                validpoints=ones(size(imtemp))==1;%logical ones
            end
                      
            for ii=1:size(AFMim,2)
                vp=validpoints(:,ii);
                switch method
                    case 'LBLO' % LBL median offset
                        m1=median(imtemp(vp,ii));
                        subtracted(:,ii)=imtemp(:,ii)-m1;
                        
                    case 'LBLL' % LBL line
                        lx=(1:size(AFMim,1))';                                              
                        lfit=fit(lx(vp),imtemp(vp,ii),'poly1','Normalize','on');
                        subtracted(:,ii)=imtemp(:,ii)-lfit(lx);
                end
            end
        case {'plane','parab','cubic','4x2'} %subtract plane
            if threshold(jj) %threshold creates a matrix which is true for the points that count
                planemedian=median(imtemp(:));
                planesd=std(imtemp(:));
                upper=planemedian+threshold(jj)*planesd;%for clarity
                lower=planemedian-threshold(jj)*planesd;
                validpoints=imtemp>=lower & imtemp<=upper; % this does a comparison and creates a 2D logical matrix that is 1 for points inside the interval
            else
                validpoints=ones(size(imtemp))==1;%logical ones
            end                     
            x=repmat(1:size(imtemp,2),size(imtemp,1),1);
            y=repmat((1:size(imtemp,1))',1,size(imtemp,2));
            switch method
                case 'plane'
                    surffit=fittype('poly11');
                case 'parab'
                    surffit=fittype('poly22');
                case 'cubic'
                    surffit=fittype('poly33');
                case '4x2'
                    surffit=fittype('poly24');%rotated!
            pfit=fit([x(validpoints),y(validpoints)],imtemp(validpoints),surffit,'Normalize','on');%
            subtracted=imtemp-pfit(x,y);
            %testmedian=median(subtracted(:));
            
        otherwise
            disp('unknown method, no subtraction!')
    end %method switch
    if any(~isfinite(subtracted(:))) %there were some bugs with NaN, so just in case this removes them
        subtracted(~isfinite(subtracted))=nanmean(subtracted(:));
    end
end %iterate
end %function






