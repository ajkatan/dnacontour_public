function [SK]=DNacontour_MapBoundaryBranches(SK,sk_im,debug_mode,pausetime)

if nargin==0 ||isempty(SK) %test mode
    if nargin<3,debug_mode=true;end
    [SK,sk_im,bdy]=MakeTestSkel;
end
if nargin<2||isempty(sk_im)
   sk_im=false(SK.ImSize);
   sk_im(SK.PixelIdxList)=true;
end
if isfield(SK,'TrB')&& ~isempty(SK.TrB{1})
   bdy=SK.TrB{1}; % outer boundary is the first in the list found by Trace Boundaries function
else
    error('no boundary found in structure')
end
epim=bwmorph(sk_im,'endpoints'); %image with endpoints =1, rest=0
bpim=bwmorph(sk_im,'branchpoints'); %image with junctions =1, rest=0
EndPoints=find(epim>0); %list of endpoints
BranchPoints=find(bpim>0); %same for branchpoints
bdy_lin=sub2indFast2D(size(sk_im,1),bdy(:,1),bdy(:,2)); %linear indices makes this a lot easier to work with
bdyim=false(size(sk_im));bdyim(bdy_lin)=true;

OffBdyBP=find(bpim &~bdyim)'; %linear indices of branchpoints off the boundary
bdy_lin=AddIsolatedBP(bdy_lin,bdyim,OffBdyBP);

[TrOr0,~,bdiE,N_ep,~,bdiB,~]=EndBranchBoundaryMap(EndPoints,BranchPoints,bdy_lin);
% EPinTrOr=sortidx<=length(bdiE); %true where there is an endpoint in the reshuffled boundary

if  isempty(bdiB) %No branchpoints, so single circle or line? That is easy
    if N_ep>1% line
        bdy_lin=bdy_lin(TrOr0(1):TrOr0(2));
        TrOr0=TrOr0(1:2); %skip the back travel
    end
    %assign everything
    SK.BranchIdx{1}=bdy_lin(:);
    SK.TrOr=TrOr0;
    SK.bdy_lin=bdy_lin;
    SK.TrN=length(bdy_lin);
    SK.bdy=bdy;
    SK.EndPoints=EndPoints; SK.BranchPoints=[];
    SK.AdMap=GetAdjacencyMap(SK.BranchIdx,SK.EndPoints,SK.BranchPoints,bdyim);
    return
end

%If it is not that simple, we eliminate the longest possible double stretch.
%To do this, we rotate the boundary vector and see up to where its end is
%the same as its beginning. The rotation that leads to the largest double
%stretch is remembered, and that double stretch is eliminated to give the
%new boundary

DKeep=length(bdy_lin);bdy_rot=bdy_lin;DStart=DKeep;
for ii=1:N_ep
    ThisEP=bdiE(ii); %this endpoint as an index in the boundary
    if ThisEP==1 || ThisEP==length(bdy_lin)&& bdy_lin(end)==bdy_lin(1) %if it is already starting here (or ending, should be the same)
        StartHere=bdy_lin;
    else
        StartHere=[bdy_lin(ThisEP:end);bdy_lin(1:ThisEP)];
    end
    EndHere=flipud(StartHere); %if we go the other way
    NotDouble=StartHere~=EndHere; %false where both directions are equal
    DStart=find(NotDouble,1,'last'); %the last point where we are not just going back to start
    if ~isempty(DStart) && DStart<DKeep
        DKeep=DStart;
        bdy_rot=StartHere(1:DKeep);
    end
end

[TrOr1]=EndBranchBoundaryMap(EndPoints,BranchPoints,bdy_rot); %get the travel order
[Branches,TrN,bdy_new]=CreateBranches(bdy_rot,TrOr1); %reshuffle the boundary
[TrOr,EPB,~,~,BPB,~,~]=EndBranchBoundaryMap(EndPoints,BranchPoints,bdy_new); %Get the reshuffled travel order and the indices of end-and branchpoints in this boundary
[bdyX,bdyY]=ind2subFast2D(size(sk_im,1),bdy_new);%xy indices of the boundary
bdyim=false(size(sk_im));bdyim(bdy_new)=true; %binary image
SK.BranchIdx=Branches;
SK.TrOr=TrOr;
SK.bdy_lin=bdy_new;
SK.TrN=TrN; %length of each branch
SK.bdy=[bdyX,bdyY];
SK.EndPoints=unique(bdy_new(EPB),'stable'); SK.BranchPoints=unique(bdy_new(BPB),'stable');
SK.AdMap=GetAdjacencyMap(SK.BranchIdx,SK.EndPoints,SK.BranchPoints,bdyim);


if nargin==0|| (nargin>2&&debug_mode)
    if nargin<4||isempty(pausetime), pausetime=0.2; end
    im2plot=zeros(size(sk_im));
    ibx=-1*ones(length(Branches),1);iby=ibx;
    for ii=1:length(Branches)
        if ~isempty(Branches{ii})
            tic
            im2plot(Branches{ii})=im2plot(Branches{ii})+1;
            figure(2);DNAcontour_plotim(im2plot);axis ij;
            [ibx(ii),iby(ii)]=ind2subFast2D(size(sk_im,1),Branches{ii}(round(TrN(ii)/2)));
            for jj=1:ii
                text(ibx(jj),iby(jj),num2str(jj))
            end
            pause(pausetime-toc);
        end
    end
end



    function [TrOr,EPB,bdiE,N_ep,BPB,bdiB,sortidx]=EndBranchBoundaryMap(EndPoints,BranchPoints,bdy)
        EPB=ismember_A(bdy,EndPoints);%returns a list of boundary points (as linear indices into the skeleton image) that are endpoints.
        bdiE=find(EPB);%bdiE has the indices in the boundary that correspond to these points
        N_ep=length(bdiE);
        BPB=ismember_A(bdy,BranchPoints); %same for branchpoints
        bdiB=find(BPB);
        [TrOr,sortidx]=sort([bdiE;bdiB]); %Travel Order, the 'waypoints' in the boundary, sorted in order of how they are traveled. These are indices in the boundary
    end

    function [AdMap,IsEpB]=GetAdjacencyMap(Branches,EndPoints,BranchPoints,branch_im)
        %makes a map of which branches connect to which endpoints and
        %branchpoints
        %Output: logical matrix NxM, where N is the number of end-and
        %branchpoints, M is the number of branches
        EPBP=[EndPoints;BranchPoints];
        [IsNN,IndNN]=GetNearestNeighbours(branch_im,EPBP');
        NNonSkel=IndNN.*IsNN; %All the neighbours of the points that are also on the skeleton
        EPBPandNN=[EPBP;NNonSkel];
        AdMap=false(length(EPBPandNN),length(Branches));
        
        for kk=1:length(Branches)
            AdMap(:,kk)=ismember_A(EPBPandNN,Branches{kk});
        end
        IsEpB=sum(AdMap(1:length(EndPoints),:),1)>0; % is true where a branch connects to an endpoint
    end

    function bdy_out=AddIsolatedBP(bdy_lin,bdyim,OffBdyBP)
        %This function adds a branchpoint at 90 degree crossings. Due to the
        %8-connectedness, such a point can be ignored in the boundary
        if ~isempty(OffBdyBP)
            [BPN,BPNind]=GetNearestNeighbours(bdyim,OffBdyBP); %gets all the neighbours in the boundary image of the branchpoints that are not on the boundary
            %BPN and BPNind are  column vectors of length 8*numel(OffBdyBP)
            NOB=find(BPN); %Neighbours On Boundary. These are indices in the BPN vector that are on the boundary and next to a branchpoint which is not on the boundary
            shortcutinds=BPNind(NOB);% the Neighbours as linear indices in the image
            BP2shortcut=OffBdyBP(ceil(NOB/8)); %gets the branchpoint that goes with each shortcut. This is again a linear index in the image
            if length(shortcutinds)>1
                insertind=[];insertval=[];
                for kk=1:length(shortcutinds)
                    t=find(bdy_lin==shortcutinds(kk)); %all indices of points in the boundary that are equal to this point (neighbour of branchpoint)
                    t0=t-1; %indices of all points before passing through this point
                    [b0,s0]=ismember_A(bdy_lin(t0),shortcutinds); %b0 is true where there is another shortcut point that comes before this shortcutpoint in the boundary order
                    %s0 contains the indices of the first-passed shortcutpoints
                    insertind=[insertind; t0(b0)]; %indices in the current boundary after which the branchpoint should be inserted
                    insertval=[insertval; BP2shortcut(s0(b0))];                    
                end                
                [~,sortervec]=sort(vertcat((1:length(bdy_lin))',insertind));
                allbdy=vertcat(bdy_lin,insertval);
                bdy_out=allbdy(sortervec);
            else %this happens in case of enclosed structures!
                bdy_out=bdy_lin;
            end
        else
            bdy_out=bdy_lin;
        end
    end

    function [Branches,BN,bdry_new]=CreateBranches(bdry,TravelOrder)
        %creates a set of branches from the boundary and the travel order
        %output:
        %Branches: cell array of vectors with values taken from bdry
        %BN: vector with number of elements in each branch
        %bdry_new: the boundary re-shuffled with this travel order
        MinSet=unique(bdry,'sorted'); %the minimal set of points that should be in the boundary, excludes all doubles
        trl=length(TravelOrder);
        Branches=cell(trl-1,1);
        BN=zeros(trl-1,1);
        skipbranch=false(trl-1,1);
        for kk=1:(trl-1)
            if TravelOrder(kk)<TravelOrder(kk+1) % regular following boundary order
                Branches{kk}=bdry(TravelOrder(kk):TravelOrder(kk+1));
            elseif bdry(TravelOrder(kk))==bdry(TravelOrder(kk+1)) &&...
                    TravelOrder(kk)==numel(bdry) && TravelOrder(kk+1)==1 %same point, nothing in between, start and end are equal
                skipbranch(kk)=true;
            elseif bdry(TravelOrder(kk))==bdry(TravelOrder(kk+1)) &&...
                    TravelOrder(kk)<=numel(bdry) || TravelOrder(kk+1)>=1 %same point, but there is a loop in between
                Branches{kk}=[bdry(TravelOrder(kk):end);bdry(1:TravelOrder(kk+1))];
            else
                error('Problem in rerouting, order seems to be wrong');
            end
            BN(kk)=numel(Branches{kk});
        end
        Branches=Branches(~skipbranch);BN=BN(~skipbranch);
        
        if nargout>2
            bdry_new=vertcat(Branches{:});
            d_elim=bdry_new(1:(end-1))==bdry_new(2:end); %doubles to eliminate because branches include branchpoints
            bdry_new=bdry_new([~d_elim;false]);
            MinSet2=unique(bdry_new,'sorted');
            if ~numel(MinSet2)==numel(MinSet)
                warning('not all points included during rerouting of boundary!')
            end
            
        end
    end

    function [SK,sk_im,bdy]=MakeTestSkel
        SK=struct;
        sk_im=false(99); %make a fake image
        sk_im(50,2:50)=true;
        sk_im(10:50,50)=true;
        sk_im(10,50:70)=true;
        sk_im(10:50,70)=true;
        sk_im(50,70:80)=true;
        sk_im(40:50,80)=true;
        sk_im(40,80:90)=true;
        sk_im(40:50,90)=true;
        sk_im(30:50,25)=true;
        sk_im=sk_im|flipud(sk_im);
        sk_im(2:50,2)=true;
        sk_im(40:50,40)=true;
        sk_im(sub2ind(size(sk_im),20:30,15:25))=true;
        sk_im(sub2ind(size(sk_im),45:55,35:45))=true;
        sk_im(24,20)=true;
        sk_im(10,70:75)=true;
        sk_im(5,70:75)=true;
        sk_im(5:10,75)=true;
        sk_im(5:10,70)=true;
        sk_im(sub2ind(size(sk_im),45:55,10:20))=true;
        sk_im=rot90(bwmorph(sk_im,'skel','Inf'),3);
        SI.sk_im=sk_im;
        SK = DNAcontour_trace_boundary(SI,SK,0);
        bdy=SK.TrB{1};
        if debug_mode
            figure(1);DNAcontour_plotim(sk_im)
        end
    end
end

%Old attempts at eliminating double parts:

%[Branches,TrN0]=CreateBranches(bdy_lin,TrOr0,true);
%trl=length(TrOr0);
% sameN=tril(TrN0==TrN0',-1); % lower diagonal matrix with 'true' where length(branch n)==length(branch m)
% [iN_eq1,iN_eq2]=ind2subFast2D(size(sameN,1),find(sameN)); %pairs of equal length branches
% BranchIdent=zeros(trl,1);
% for ii=1:length(iN_eq1)
%     thisbranch=Branches{iN_eq1(ii)};
%     thatbranch=Branches{iN_eq2(ii)};
%     if all(thisbranch==flipud(thatbranch)) %if this is the same in reverse
%         BranchIdent(iN_eq1(ii))=iN_eq2(ii);
%     end
% end

%mostdouble=0; keepJ=1:trl;


% for jj=0:trl-1
%     if EPinTrOr(jj+1) %only for the endpoints as first ones
%        Bshift=circshift(BranchIdent,-jj);
%        Nshift=circshift(TrN0,-jj);
%        BOutAndBack=[Bshift(1:(end-1))==Bshift(2:end);0]; %Two identical branches in a row means an out and back and should not be skipped. Last one can be skipped even if it is out and back.
%         %0=0 , so this vector is also true for branches that have no identical partner!
%         x1=Bshift&~BOutAndBack; %true where the branches have an identical partner, but aren't out and back
%         %the goal is to find the longest stretch of these at the tail end
%         DStart=find(~x1,1,'last'); %the last waypoint of the part that doens't travel the same road back to the start in opposite direction
%         if DStart==trl
%             Dlength=Nshift(end);
%         else
%             Dlength=sum(Nshift((DStart+1):end)); %length that is only traveled only to get back to the start
%         end
%         if  Dlength>mostdouble
%             mostdouble=Dlength;
%             keepJ=circshift(1:trl,-jj);
%             keepJ=keepJ(1:DStart-1);
%         end
%     end
% end

