function [SK,pathim]=DNAcontour_GetPaths(SK,ii);
%Gets the 'longest shortest path' from a structure generated with DNAcontour_MapBranchDistance(BW);
%uses the function 'dijkstra', which I downloaded from the Matlab Central
%site. It was written by Joseph Kirk.

%to do: shapes that can only be covered if a segment is travelled more than
%once. That will be quite hard to decide. User directed??

%ii is region nr, used for error message
if nargin<2
    ii=0;
end

pathim=false(SK.ImSize);
EPBP=[SK.EndPoints;SK.BranchPoints]; %endpoints and branchpoints
np=length(EPBP);
AdjMat=zeros(np); %Adjacency Matrix
ParAdjMat=[]; %for parallel brances
ParCostMat=[];
CostMat=inf*ones(np); %Cost Matrix
if isempty(EPBP)||numel(SK.PixelIdxList)==1 %'perfect circle' no end or branchpoints, or a single point
    MolPath=SK.BranchIdx{1};
    maxlength=SK.BranchDist{1};
    AdjMat=[];
    CostMat=maxlength;
    costs=maxlength;
    paths=[];
elseif numel(EPBP)==1 %"infinity sign", one branchpoint, no endpoints
    if length(SK.BranchIdx)~=2
        error('one branchpoint and not 2 branches? I dont understand');
    end
    MolPath=[SK.BranchIdx{1};SK.BranchIdx{2}];
    AdjMat=eye(2);
    paths={[1 1 ],[];[],[1 1 ]};
    CostMat=[SK.BranchDist{1}(end),Inf;Inf,SK.BranchDist{2}(end)];
    costs=[sum(CostMat(:)) 0 ;0 sum(CostMat(:))];
else
    %we take each branch and find the points it connects
    for jj=1:length(SK.BranchIdx) %BranchIdx is a cell with each element a vector with points on the branch
        thisbranch=SK.BranchIdx{jj}; %the list of points for this branch
        thisdist=SK.BranchDist{jj}(end); %the length of the branch
        onbranch=ismember_A(EPBP,thisbranch); %find the ends and branchpoints on this branch
        conbranch=find(onbranch); %the points connected by this branch
        if numel(conbranch)==2 %this should be true for all branches except circles. If not probably something went wrong in the branch finding.
            c1=conbranch(1);c2=conbranch(2); %shorthand
            if ~isfinite(CostMat(c1,c2)) %if it is still at initial value of 'Inf'
                AdjMat(c1,c2)=jj; %Adjacency ~=0 indicates the two points are connected. Number tells you which branch connects them.
                AdjMat(c2,c1)=jj; %'dijkstra' only works like I expect it to if this is symmetric, depite claims otherwise
                CostMat(c1,c2)=thisdist; %Length of the connecting branch.
                CostMat(c2,c1)=thisdist;
            else %if a parallel branch exists
                if ~iscell(ParAdjMat) %we only create a matrix if needed
                    ParAdjMat=cell(np); %for parallel branches
                    ParCostMat=cell(np);
                end
                alldist=[ParCostMat{c1,c2},CostMat(c1,c2),thisdist]; %all the distances between these points that we already covered
                allbranch=[ParAdjMat{c1,c2},AdjMat(c1,c2),jj];
                [mdist,mind]=max(alldist);%the longest distance
                CostMat(c1,c2)=mdist; %longest goes into the Cost and Adjacency matrices
                CostMat(c2,c1)=mdist;
                AdjMat(c1,c2)=allbranch(mind);
                AdjMat(c2,c1)=allbranch(mind);
                TheOthers=(1:length(alldist)~=mind);
                ParAdjMat{c1,c2}=allbranch(TheOthers);
                ParAdjMat{c2,c1}=allbranch(TheOthers);
                ParCostMat{c1,c2}=alldist(TheOthers); %the others in the parallel matrices
                ParCostMat{c2,c1}=alldist(TheOthers);
            end
        elseif numel(conbranch)==1 && any(vertcat(SK.LoopEPBP{:})==EPBP(c1)) % a loop that has only a single branchpoint in it(curl, lollipop)
            %warning('Still single branchpoints, region %i, branch %i!',ii,jj)
            AdjMat(c1,c1)=jj;
            CostMat(c1,c1)=thisdist;
        else
            warning('Not 2 points found in a branch but %i and not detected as loop. Region %i, Branch %i.',numel(conbranch),ii,jj);
        end
    end
    [costs,paths] = dijkstra(AdjMat,CostMat);
    costs(isnan(costs))=0; %we don't want NaN's
    costs(~isfinite(costs))=0;%we don't want Inf's either
    
    %here we break in and start modifying the paths to include loops
    LoopCandidat=find(diag(AdjMat)); %points on the diagonal of the Adjacancy matrix indicate that a 'clean' loop (no branchpoints in the loop) is connected to this point
    CMdia=diag(CostMat);
    for pp=1:numel(paths)
        thispath=paths{pp}; %pick out one path to enable indexing
        thiscost=costs(pp);
        thisloopcands=find(ismember_A(thispath,LoopCandidat)); %returns the list of indices of branchpoints within the path list which are the start/endpoints of a (clean) side loop
        if ~isempty(thisloopcands) %if there are loops we add them to the path as a duplicated set of branchpoints
            sortnums=sort([1:length(thispath),thisloopcands]); %a list of indices. through sorting we put the dupicated ones next to each other
            thispath=thispath(sortnums); %now we make the path from the indices
            thiscost=thiscost+sum(CMdia(thispath(thisloopcands))); %add up the loop lengths to the path length. The path nrs are the indices you need in the diagonal of the cost matrix
        end
        paths{pp}=thispath;
        costs(pp)=thiscost;
    end
    MolPath=[];
end

[maxlength,maxind]=max(costs(:)); %longest of the shortest paths should give an end-to end distance
if iscell(paths)
    path_nrs=paths{maxind}; %'path_nrs' is a list of waypoints, indices into the EPBP vector
else
    error('paths is not a cell in region %i',ii);
end


for pp=1:(length(path_nrs)-1)
    p1=path_nrs(pp);p2=path_nrs(pp+1);
    BranchId=AdjMat(p1,p2); %We get the ID of the branch travelled from the Adjacency Matrix
    thisbranch=SK.BranchIdx{BranchId}; %the indices on the branch
    if pp>1
        if thisbranch(1)==MolPath(end) %if the start of this branch is the end of the path
            %Do nothing
        elseif thisbranch(end)==MolPath(end) %if the end of this branch is the end of the path
            thisbranch=flipud(thisbranch); %reverse the branch before appending
        else %if the end of the previous branch is neither
            warning('are these branches connected? nr %i, branch %i and %i',ii,path_nrs(pp),path_nrs(pp+1))
        end
    end
    MolPath=[MolPath;thisbranch]; %Append to get the full path
end

MolPath=unique(MolPath,'stable');
SK.LengthNoLoop=maxlength; %put them all in the structure
SK.AdjMat=AdjMat;
SK.ParAdjMat=ParAdjMat;
SK.ParCostMat=ParCostMat;
SK.CostMat=CostMat;
SK.costs=costs;
SK.paths=paths;
SK.Path=MolPath;
[x,y]=ind2subFast2D(SK.ImSize(1),SK.Path);
[SK.Angle,SK.Curv,SK.Dist]=AngAndCurv(x,y);
SK.Length=SK.Dist(end);
pathim(SK.Path)=true;
end


