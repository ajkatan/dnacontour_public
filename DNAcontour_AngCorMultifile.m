function [AAg,totalN]=DNAcontour_AngCorMultifile(filelist,dv,bv,mult1,mult2)
%gathers histograms from a list of files (cell array, add brackets if
%single file)
%Does angle correlation distribution for each separation in dv. dv is in basepairs
%(0.34 nm). Values are histogrammed with bin centers bv;
%mult1,mult2: value between 0 and 1;
%Mult1: minimum fraction that largest correlation distance must be of the total path length. 1 = everything of
%max(dv) and longer. 0=none. >1 is possible but leads to artefacts! Default
%0.5;
%%mult2: minimum fraction of (points in path)/(points in full skeleton) for molecule to be taken into account. 
%0=us all all, 1=none; Default 0.9;
if nargin==0 || isempty(filelist)
    [fileNlist,filedir]=uigetfile('Multiselect','on');
    filelist=fileNlist;
    for ii=1:length(fileNlist)
        filelist{ii}=fullfile(filedir,fileNlist{ii});
    end
end
if nargin<2 || isempty(dv)
   dv=[10 25 50 100 200];
   warning('used default dv: [10 25 50 100 200]')
end
if nargin<3 || isempty(bv)
   bv=linspace(0,pi,90);
   warning('used default bv: 0-pi in 2 degree steps')
end
if nargin<4 || isempty(mult1)
   mult1=0.5; %use all > 2x maxlength
end
if nargin<5 || isempty(mult2)
   mult2=0.9; %use all > 90% path
end
    
AAg=zeros(length(dv),length(bv)); %All Angles gathered
for ii=1:length(filelist)
    load(filelist{ii},'PixToBP','SPS','SK');
    if PixToBP>1e8,PixToBP=PixToBP/1e9;warning('nm -> m correction applied');end %in case for some reason there is a meter/nanometer mistake
    fprintf('%s, pixels size %d basepairs\n',filelist{ii},PixToBP)
    for ss=1:length(SPS);
        if max(dv)/PixToBP <mult1*SPS(ss).SnakeD(end) && numel(SK(ss).Path)>mult2*numel(SK(ss).PixelIdxList)
            AA = AngleDistr(SPS(ss).SnakeAng,SPS(ss).SnakeD,dv/PixToBP,bv,0);
            AAg=AAg+AA;
        end
    end
end
totalN=sum(AAg(1,:));fprintf('Total points investigated: %i\n',totalN);